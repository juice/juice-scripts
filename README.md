# JUICE scripts

This repository contains a collection of diverse scripts and tools relevant to the JUICE mission, for science, operations. 
Scripts are provided in at least one language (IDL, Python, Matlab etc.). 

# Necessary/useful prerequisites
- IDL, or Python installatins, for running the respective scripts
- [SPICE Installation (IDL, Matlab, C, Fortran)](https://naif.jpl.nasa.gov/naif/toolkit.html)
- [SPICE Installation (Python)](https://spiceypy.readthedocs.io/en/main/)
- [JUICE SPICE Kernels](https://www.cosmos.esa.int/web/spice/spice-for-juice)
- [Europa Clipper SPICE bsp trajectory file](https://juigitlab.esac.esa.int/notebooks/juice-clipper/-/tree/main/data)
- [Jupiter magnetic field models (Python, IDL, Matlab, C++)](https://lasp.colorado.edu/home/mop/missions/juno/community-code/)
- [Coyote graphics library (IDL)](https://github.com/idl-coyote/coyote)
- [JUICE project planning tools and material](https://www.cosmos.esa.int/web/juice/shared-data) & [FTP](ftp://ftp.sciops.esa.int/pub/juicesoc/DATA) (also available here in the Data folder)
- [Moon coverare tool](https://pypi.org/project/moon-coverage/) & [(Gitlab location)](https://juigitlab.esac.esa.int/datalab/moon-coverage)

# Folders

- **Data**: Contains data files from the JUICE/SOC, the Magnetospheres of Outer Planets (MOP) community and other sources, from where the various scripts rread information. It also contains output from several scripts (e.g. L-shell tracing results)
- **IDL**: Scripts written in IDL language
- **Python**: Scripts written in Python language
- **Results**: Collection of sample plots and data generated through the provided scripts

## Installation & Usage
Most provided tools require some manual set-up (e.g. definition of SPICE installation directory, external libraries etc.).
Comments witihn each script should guide on how each script should set-up, executed etc and/or adapted to a different programming language.

#### IDL Setup:
<details><summary>Click to expand</summary>

1. **Clone the repository:**

Through the command line:

`cd path_to` (change _path_to_ to a desired folded)

`git clone https://gitlab.gwdg.de/juice/juice-scripts.git`

Make sure to include at leadt the IDL folder in your IDL PATH. E.g. if the repository is at _/path_to/juice_scripts_ then in an IDL startup file you can add:

`scripts_path=EXPAND_PATH('+/path_to/juice_scripts/IDL/',/all_dirs)` (replace _/path/to_ with the actual base folder)

`!path=scripts_path+!path`

2. **Install SPICE and the JUICE/Europa Clipper SPICE kernels**

- Retrieve the SPICE ICY module for your operating system from here: https://naif.jpl.nasa.gov/naif/toolkit_IDL.html and follow installation instructions. In most of the provided scripts, it is assumed that SPICE is pre-loaded. In IDL this is done as:
`dlm_register, ICY_DIR+'/lib/icy.dlm'`, where `ICY_DIR` is the path of the _icy_ folder containing the SPICE source code. It is recommended to load SPICE using a start-up file, or at least load it manually at startup. Most scripts wont work without SPICE icy module loaded.

- Retrieve the JUICE SPICE Kernels: `git clone https://s2e2.cosmos.esa.int/bitbucket/scm/spice_kernels/juice.git` or download from https://s2e2.cosmos.esa.int/bitbucket/projects/SPICE_KERNELS/repos/juice/browse

In case you want to also use certain scripts pointing to the Europa Clipper tranjectory, obtain the respective file from: [Europa Clipper SPICE bsp trajectory file](https://juigitlab.esac.esa.int/notebooks/juice-clipper/-/tree/main/data)

3. **Install Jovian magnetic field model libraries**

`cd path_to` (change path_to to a desired folded)

`git clone https://github.com/gabbyprovan/con2020.git`

`git clone https://github.com/rjwilson-LASP/PSH.git`

Within IDL or an IDL startup script:

`con2020_path=EXPAND_PATH('+/path_to/con2020/',/all_dirs)` (replace _/path/to_ with the actual base folder)

`!path=con2020_path+!path`

`psh_path=EXPAND_PATH('+/path_to/psh/',/all_dirs)` (replace _/path/to_ with the actual base folder)

`!path=psh_path+!path`

4. **Additional tools**
Few scripts use also the [Coyote graphics library (IDL)](https://github.com/idl-coyote/coyote), which could be useful to obtain and add to the IDL path, if not yet included:

`cd path_to` (change path_to to a desired folded)

`git clone https://github.com/idl-coyote/coyote.git`

Within IDL or an IDL startup script:

`coyote_path=EXPAND_PATH('+/path_to/idl_coyote/',/all_dirs)` (replace _/path/to_ with the actual base folder)

`!path=coyote_path+!path`

5. **Test some IDL scripts**

_a) Generate the heliocentric JUICE trajectory data for the Cruise period in ECLIP_J2000 coordinates_

`interval=['2023 APR 14 13:14:00.000','2031-09-01T00:00:00.000']` <span style="color:green">;time range between JUICE launch and arrivan at Jupiter</span>

`spice_dir = '/Users/roussos/ownCloud/Work/JUICE/SPICE/kernels/'` <span style="color:green">;define directory of spice kernels (change accordingly)</span>

`icy = '/Users/roussos/data/SPICE/icy/lib/icy.dlm'` <span style="color:green">;define directory of SPICE icy module (change accordingly)</span>

`au=149597870.700d` <span style="color:green">;define the 1 AU in km</span>

`tstep=3600d` <span style="color:green">;define the time step in seconds</span>

<span style="color:green">;Call get_ephem_juice_generic, use /init to initialize SPICE, if not initialized at IDL startup</span>

`out=get_ephem_juice_generic(date=interval, target='JUICE', center='Sun', sys='ECLIPJ2000', unit=au, step=tstep, /init, dir=spice_dir)`
</details>

## Support
Contact Elias Roussos (roussos@mps.mpg.de)

## Contributing
- Contributions/comments are welcome. 
- It is highly desired that original scripts are sufficienty commented, at least indicating any necessary manual set-up and examples of execution. 

## Project status
- 15 Dec. 2022: Initial commit of several IDL SPICE tools 
