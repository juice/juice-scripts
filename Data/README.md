# Data file folders

- **Aurora**: Jupiter aurora oval approximate locations from https://lasp.colorado.edu/home/mop/missions/juno/trajectory-information/
- **JUICE**: Data files for the JUICE mission obtained from the SOC or output by the application of the various scripts provided here (e.g. field line traces)
