PRO get_lshell_mission_orbit

;the set of commands below demonstrates how to calculate magnetic parameters (e.g. L-shell) with get_lshell_spice, for one of the JUICE orbits. In this example it will be orbit 12

;Step 1: Find the times of a given mission phase you are interested to obtain L-shell calculations

;The call below will list all mission phases for a selected metakernel. 
;Orbit data are stored with the JUICE SPICE kernels, at ../misc/orbnum/'

orb = juice_orbit_read(orbit='12', dir='/Users/roussos/ownCloud/Work/JUICE/SPICE/misc/orbnum/')

;When run, this will prompt the user to select:
;Orbit Information Files
;-----------------------
;0. juice_mat_crema_5_0_20220826_20351005_v01.orb
;1. juice_mat_crema_5_0b23_1_20230405_20351005_v01.orb
;2. juice_mat_crema_5_1_150lb_v01.orb
;Select (0-2):2

;Return values (STRING): Orbit Number, UTC Start, UTC Stop, UTC Periapis
;If I select 2, then I obtain the orbit 12 of crema_5_1_150lb_23_1
;The result will be:

print, orb
;12 2032 SEP 16 13:11:22 2032 OCT 03 04:10:09 2032 SEP 24 21:33:36


;Step 2: obtain the orbit start and stop times

start_time=orb[1] ;start time
stop_time=orb[2] ;stop time

;Step 3: Call get_lshell_spice. Get one field line tracing every 30 min (or 1800 sec), not at distances >70 Rj. Target is JUICE. Use the latest internal field model 'jrm33_18' and euler integrator
get_lshell_spice, trange=[start_time, stop_time], step=900d, rlimit=70d, target='JUICE', order='jrm33_18', integrator='ruk45', ds=0.1

;Make sure to select the correct SPICE kernel when promted. Calculation can take very long

;The "get_lshell_spice" call can be provided with more parameters if needed, to regulate execution time, precision etc.

END