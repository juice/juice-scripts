PRO get_lshell_mission_phase

;the set of commands below demonstrates how to calculate magnetic parameters (e.g. L-shell) with get_lshell_spice, for one of the JUICE mission phases

;Step 1: Find the times of a given mission phase you are interested to obtain L-shell calculations

;The call below will list all mission phases for the selected metakernel. 
;The data were obtained from https://www.cosmos.esa.int/web/juice/shared-data and I have stored them at 
;my local copy of the GitHub repository /Users/roussos/Documents/GitHub/juice-scripts/Data/JUICE/SOC_events_segments_phases/

out=juice_mission_phase_read(/list_events, dir='/Users/roussos/Documents/GitHub/juice-scripts/Data/JUICE/SOC_events_segments_phases/')

;When run, this will prompt the user to select:
;Phase Information Files
;-----------------------
;0. crema_3_0/Mission_Phases.csv
;1. crema_5_0/Mission_Phases.csv
;2. crema_5_0b23_1/Mission_Phases.csv
;3. crema_5_1_150lb_23_1/Mission_Phases.csv
;Select (0-3):

;If I select 3, then I obtain the mission phases of crema_5_1_150lb_23_1
;The result will be:

;Jupiter_Phase_1 Approach and first ellipse 2031-01-19T19:14:21 2032-02-08T23:05:31
;Jupiter_Phase_2 Energy reduction 2032-02-08T23:05:32 2032-06-25T12:23:23
;Jupiter_Phase_3 Europa flybys 2032-06-25T12:23:23 2032-07-24T07:02:19
;Jupiter_Phase_4 High-latitude 2032-07-24T07:02:19 2033-08-18T09:43:28
;Jupiter_Phase_5 Low energy 2033-08-18T09:43:28 2034-12-19T00:00:00
;Jupiter_Phase_all All Jupiter phases 2031-01-19T19:14:21 2034-12-19T00:00:00
;Ganymede_Phase_6_1 GEOa 2034-12-19T00:00:00 2035-01-17T03:00:00
;Ganymede_Phase_6_2 GCO5000 2035-01-17T03:00:00 2035-04-16T16:00:00
;Ganymede_Phase_6_3 GEOb 2035-04-16T16:00:00 2035-05-13T02:00:00
;Ganymede_Phase_6_4 TGCO 2035-05-13T02:00:00 2035-05-21T15:00:00
;Ganymede_Phase_6_5 GCO500 2035-05-21T15:00:00 2035-09-29T00:00:00
;Ganymede_Phase_all All Ganymede phases 2034-12-19T00:00:00 2035-09-29T00:00:00
;Mission_phase_all All mission phases 2031-01-19T19:14:21 2035-09-29T00:00:00

;First column shows the mission phase names. Lets assume we want to calculate results for Mission_phase_all. If a different Phase is chosen, change the entries below accordingly
;For a certain phase, select e.g. 'Jupiter_Phase_5'

;Step 2: obtain the times for Mission_phase_all
phases=out[0,*]
find_phase=WHERE(STRUPCASE(phases) EQ STRUPCASE('Mission_phase_all'))

start_time=out[2,find_phase[0]] ;start time stored in column 2 of matrix out
stop_time=out[3,find_phase[0]] ;stop time  stored in column 3 of matrix out

;Step 3: Call get_lshell_spice. Get one field line tracing every 30 min (or 1800 sec), not at distances >70 Rj. Target is Europa. Use the latest internal field model 'jrm33_18' and euler integrator
get_lshell_spice, trange=[start_time, stop_time], step=900d, rlimit=70d, target='JUICE', order='jrm33_18', integrator='rk45', ds=0.1

;Make sure to select the correct SPICE kernel when promted. Calculation can take very long

;The "get_lshell_spice" call can be provided with more parameters if needed, to regulate execution time, precision etc.

END