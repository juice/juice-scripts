PRO juice_pitch_angle_generic,$
    step=step,$ 
    trange=trange,$ 
    vector=vector,$ 
    bvector=bvector,$ 
    model=model,$ 
    output=output,$ 
    dir=dir,$
    order=order,$
    out_dir=out_dir
    
;+
  ; NAME:
  ; juice_pitch_angle_generic
  ;
  ; PURPOSE:
  ; Calculate pitch angle between any arbitrary magnetic field direction and a pointing vector available in the JUICE kernels
  ;
  ; CATEGORY:
  ; JUICE pointing scripts
  ;
  ; CALLING SEQUENCE:
  ; juice_pitch_angle_generic, [step=step], [trange=trange], [vector=vector], [bvector=bvector], [dir=dir], [out_dir=out_dir],$
  ;                            [model=model], [order=order], [output=output]
  ;
  ; INPUTS:
  ; step: time step in seconds, default is 120 sec
  ; trange: time range (format example trange=['2032-01-11T05:44:04.000','2032-01-12T05:44:04.000'], time format accepted any UTC format that works for SPICE)
  ; vector: define with respect to which pointing vector pitch angle should estimated. Could be any JUICE frame with a boresight definition 
  ;         (e.g. vector='JUICE_PEP_JEI') or any arbitrary direction, but with a spice ref. frame indicated e.g. as vector=LIST([0.0, 1.0, 0.0],'JUICE_SPACECRAFT')
  ;         In this second case the format should always be vector=LIST([vec_x, vec_y, vec_z], spice_frame_in_which_vector_is_defined]
  ;         [vec_x, vec_y, vec_z] does not have to be a unit vector
  ; bvector: this is a user-defined, constant magnetic field vector relative to which pitch angle should be estimated. Should be in IAU_JUPITER cartesian.
  ;          Does not need to be unit vector. If not set or if number of elements is not 3, the script reverts to the use of a field model.
  ; output: output filename
  ; order: Jovian field expansion order, options: '', 'vit4', 'vip4_4', 'vipal_5', 'o6_3', 'isaac_10', 'jrm09_10', 'jrm33_13','jrm33_18'. Default: jrm09_10. Error input reverts to default
  ; dir: location of SPICE kernels directory. Default can be changed directly in the script
  ; out_dir: directory to store the output file. . Default can be changed directly in the script.
  ; 
  ;
  ; OUTPUTS:
  ; Ascii file with time series of pitch angle estimates
  ; 
  ; COMMON BLOCKS:
  ; None.
  ;
  ; SIDE EFFECTS:
  ; None.
  ;
  ; RESTRICTIONS:
  ; Requires JUICE SPICE kernels, Jupiter field models
  ;
  ; PROCEDURE:
  ; Adjust input folders in the beginning of the script: out_dir, dir
  ; 
  ; Examples:
  ; 
  ;(A) Get the pitch angle with respect to direction (0,1,0) of the JUICE_SPACECRAFT frame,
  ;    between '2032-07-02T04:22:11.000' & '2032-07-03T04:22:11.000', assuming B=[0,0,-1] in IAU_Cartesian coordinates.
  ;    juice_pitch_angle_generic, bvector=[0,0,-1.0], vector=LIST([0.0,1.0,0.0],'JUICE_SPACECRAFT'), trange=['2032-07-02T04:22:11.000','2032-07-03T04:22:11.000']

  ;(B) Get the pitch angle with respect to the boresight of JUICE_PEP_JEI,
  ;    between '2032-07-02T04:22:11.000' & '2032-07-03T04:22:11.000', assuming B is that given by a field model.
  ;    juice_pitch_angle_generic, /model, vector='JUICE_PEP_JEI', trange=['2032-07-02T04:22:11.000','2032-07-03T04:22:11.000']
  ;
  ; MODIFICATION HISTORY:
  ; Written, ER, October, 2022.
  
IF (N_ELEMENTS(dir) EQ 0) THEN dir='/Users/roussos/ownCloud/Work/JUICE/SPICE/kernels/'
IF (N_ELEMENTS(out_dir) EQ 0) THEN out_dir='/Users/roussos/'

;some inputs. Adjust accordingly
dirmk = dir + 'mk/' ;metakernels directory
sysval='IAU_JUPITER' ; use this coordinate system for follow-up SPICE calculations since magnetic field models return mag vector in that

;Define some default values
IF (N_ELEMENTS(step) EQ 0) THEN step=120d ;time step for ephemeris in seconds
IF (N_ELEMENTS(trange) EQ 0) THEN trange=['2032-07-02T04:22:11.000','2032-07-03T04:22:11.000'] ; Europa flyby 1, CREMA 5.0xx
IF (N_ELEMENTS(output) EQ 0) THEN output='pitch_angle'
IF (N_ELEMENTS(model) EQ 0 and N_ELEMENTS(bvector) NE 3) THEN model=1 ; If bvector not defined (correctly), and model not defined, use input from field model
IF (N_ELEMENTS(order) EQ 0) THEN order='jrm09_10'
order=STRLOWCASE(order); make sure input of order is in lowercase


rj=71492.0 ;jupiter radius in km

PRINT, 'Pitch Angles will be calculated for vector '
PRINT, ''

cd, dirmk ;change to metakernels directory
metakernel=file_search('*.tm', count=mkcount) ;find all metakernels

PRINT, 'SPICE metakernels' ;print metakernel names on screen
PRINT, '-----------------------'
FOR i=0l, mkcount-1l DO PRINT, STRTRIM(STRING(i),2)+'. '+metakernel[i]

READ, input_key, PROMPT='Select (0-'+STRTRIM(STRING(mkcount-1l),2)+'):' ;select metakernel
cspice_furnsh, dirmk + metakernel[input_key] ;load selected metakernel

;;;;;GET THE POSITION;;;;;;
;this is for calculations in the pitch angle panel
;below get the JUICE position in the desired coordinate system (sysval) in unit=1 Rj, stored
;in the xyzt array
;xyzt[0:2]--> x, y, z position
;xyzt[3:5]--> x, y, z velocity
;xyzt[6]--> ET time (seconds)

xyzt=get_ephem_juice_generic(date=trange, $
  step=step, sys=sysval, center='JUPITER', target='JUICE', unit=rj) ;unit=Rj so output in Rj or Rj/sec, Rj unit chosen as its appropriate for field model input;

x=xyzt[*,0] ;x,y,z variables
y=xyzt[*,1]
z=xyzt[*,2]

et_time=xyzt[*,6] ;assign a variable to the ET-time for simplicity (same for both panels since they use the same trange & step)
cspice_et2utc, et_time, 'ISOD', 1, isod; generate time labels in ISOD format for file export

r=SQRT(x*x + y*y + z*z) ;radial distance
colat=ACOS(z/r) ;this is in the 0-->pi range, as required by SPICE
s3lone=ATAN(y,x) ;s3 longitude, east
test=WHERE(s3lone LT 0)
IF (test[0] NE -1) THEN s3lone[test]=2*!PI +s3lone[test] ;convert to 0-->2pi

vtype=typename(vector);type of vector input
IF (vtype EQ 'LIST') THEN BEGIN ;if its a list, separate vector and input frame information
  
  bsight=vector[0]
  frame_in_which_fov_was_defined=vector[1]
  
ENDIF ELSE BEGIN ;if a spice input, find its id and get the boresight and input frame with CSPICE_GETFOV
  
  CSPICE_BODN2C, vector, id, found 
  CSPICE_GETFOV, id, 6, shape, frame_in_which_fov_was_defined, bsight, bounds ;GET FoV params
  
ENDELSE

CASE order OF
  'vit4': imodel = 'vit4_order04'
  'vip4_4': imodel  = 'vip4_order04'
  'vipal_5': imodel = 'vipal_order05'
  'o6_3': imodel = 'o6_order03'
  'isaac_10': imodel = 'isaac_order10'
  'jrm09_10': imodel = 'jrm09_order10'
  'jrm33_13': imodel = 'jrm33_order13'
  'jrm33_18': imodel = 'jrm33_order18'
  ELSE: BEGIN
    order =  'jrm09_10'
    imodel = 'jrm09_order10'
  END
ENDCASE

pitch_angle=FLTARR(N_ELEMENTS(et_time)) ;Make array to store pitch angle


FOR i=0l, N_ELEMENTS(et_time)-1l DO BEGIN ;start a loop for all times

  CSPICE_PXFORM, frame_in_which_fov_was_defined, sysval, et_time[i], rotate ;rotation matrix into the desired frame (sysval)
  CSPICE_MXV, rotate, bsight, bsight_final ;make the rotation in the desired frame-->bsight final

  ;get an estimate for the magnetic field. If model=1 THEN input is from (JRM09 internal field model + CAN current sheet), else use the user-defined vector, if provided
  IF keyword_set(model) THEN dummy= execute('Bmag=jovian_'+imodel+'_internal_xyz(x[i], y[i], z[i]) +con2020_model_xyz(''analytic'', x[i], y[i], z[i])') ELSE Bmag=TRANSPOSE(bvector)

  u_mag = TRANSPOSE(bmag/SQRT(TOTAL(bmag^2.0))); get the magnetic field unit vector
  pitch_angle[i] = 180.0-CSPICE_VSEP(u_mag,bsight_final) * !RADEG ;get the pitch angle. The subtraction from 180 is for converting from instrument to particle pitch angle

ENDFOR

;convert pitch angle output to strings for printing
pitch_angle=STRTRIM(STRING(pitch_angle),2)

;Some parameters for the output file header
look_direction=STRTRIM(STRING(bsight),2) 
look_frame=frame_in_which_fov_was_defined
IF NOT keyword_set(model) THEN input_b='User-provided B-vector (IAU_JUPITER)' ELSE input_b='Internal model '+imodel+' + CAN model (IAU_JUPITER)'


GET_LUN, lun

fmt='(A22,1X,A12)'

IF (N_ELEMENTS(bvector) NE 3) THEN filename=out_dir+output+'_'+metakernel[input_key]+'_'+order+'.dat' ELSE filename=out_dir+output+'_'+metakernel[input_key]+'.dat' ;define filename
fsearch=FILE_SEARCH(filename, count=count) ;search if the same file exists from before. 
IF (count EQ 1) THEN file_delete, filename ;make sure not to overwrite on previous file with the same filename & directory

OPENW, lun, filename ;open file to write
PRINTF, lun, ';File generated (UTC): '+systime(/UTC) ;put some header material
PRINTF, lun, ';Input Look Direction vector:'+look_direction[0], '    ', look_direction[1],'    ', look_direction[2]
PRINTF, lun, ';Input Look Direction vector frame: '+look_frame
PRINTF, lun, ';Input Magnetic Field vector: '+input_b
PRINTF, lun, ';'
PRINTF, lun, ';Date                  Pitch Angle [deg]'
PRINTF, lun, ';-----------------------------------------------------------------'
PRINTF, lun, ';'

FOR i=0l, N_ELEMENTS(et_time)-1l DO PRINTF, lun, TRANSPOSE([[isod[i]],[pitch_angle[i]]]),FORMAT = fmt ;write the output
FREE_LUN, lun

cd, dirmk
cspice_kclear ;unload spice kernels

END