FUNCTION spice_get_rectangular_corners, $
         bsight=bsight,$
         refang=refang,$
         crsang=crsang,$
         refvec=refvec
         
;+
; NAME: 
; spice_get_rectangular_corners
;
; PURPOSE:
; Obtain the four boundary corners of an instrument FoV, if its FoV is rectangle-shaped and the corners cannot be obtained by cspice_getfov (e.g. if 
; e.g. if boresights, reference vectors, reference & cross angles are provided separately.
;
; CATEGORY:
; JUICE spice scripts
;
; CALLING SEQUENCE:
; spice_get_rectangular_corners, bsight=bsight, refang=refang, crsang=crsang, refvec=refvec
;
; INPUTS:
; bsight: 3-element array of the sector boresight. not necessary to be unit vector
; refang: reference angle in radians (see getfov spice routine for definition)
; crsang: cross angle in radians (see getfov spice routine for definition)
; refvec: 3-element array of reference vector (see getfov spice routine for definition)
;
; The procedure below is taken from icy/src/cspice/getfov.c, written in C++ and translated in IDL
; 
;       /*           (1) Normalize BSIGHT, label it B. */
;
;       /*           (2) Compute the unit vector in the plane defined by REFVEC */
;       /*               and B that is normal to B and pointing towards */
;       /*               REFVEC, label this B1. */
;
;       /*           (3) Cross B and B1 to obtain B2. These three vectors */
;       /*               form a basis that is 'aligned' with the FOV cone. */
;
;       /*           (4) Compute the inward normals to the sides of the */
;       /*               rectangular cone in a counter-clockwise order */
;       /*               about the boresight: */
;
;       /*                 NORMAL(1) = -COS(REFANG)*B1 + SIN(REFANG)*B */
;       /*                 NORMAL(2) = -COS(CRSANG)*B2 + SIN(CRSANG)*B */
;       /*                 NORMAL(3) =  COS(REFANG)*B1 + SIN(REFANG)*B */
;       /*                 NORMAL(4) =  COS(CRSANG)*B2 + SIN(CRSANG)*B */
;
;       /*           (5) Compute the appropriate cross products to obtain */
;       /*               a set of boundary corner vectors: */
;
;       /*                 BOUNDS(1) = NORMAL(1) x NORMAL(2) */
;       /*                 BOUNDS(2) = NORMAL(2) x NORMAL(3) */
;       /*                 BOUNDS(3) = NORMAL(3) x NORMAL(4) */
;       /*                 BOUNDS(4) = NORMAL(4) x NORMAL(1) */

  cspice_unorm, bsight, b, bmag
  
  cspice_vperp, refvec, bsight, b1
  
  cspice_vhat, b1, tmpvec
  cspice_vequ, tmpvec, b1
  
  cspice_vcrss, b, b1, b2
  
  cosran = cos(refang);
  coscan = cos(crsang);
  
  sinran = sin(refang);
  sincan = sin(crsang);
  d__1 = -cosran;
  cspice_vlcom, d__1, b1, sinran, b, normal0
  d__1 = -coscan;
  
  cspice_vlcom, d__1, b2, sincan, b, normal3
  cspice_vlcom, cosran, b1, sinran, b, normal6
  cspice_vlcom, coscan, b2, sincan, b, normal9
  
  cspice_vcrss, normal0, normal3, bounds0
  cspice_vcrss, normal3, normal6, bounds3
  cspice_vcrss, normal6, normal9, bounds6
  cspice_vcrss, normal9, normal0, bounds9
  
  cspice_unorm, bounds0, tmpvec, vmag
  cspice_vscl, bmag, tmpvec, bounds0
  
  cspice_unorm, bounds3, tmpvec, vmag
  cspice_vscl, bmag, tmpvec, bounds3
    
  cspice_unorm, bounds6, tmpvec, vmag
  cspice_vscl, bmag, tmpvec, bounds6
  
  cspice_unorm, bounds9, tmpvec, vmag
  cspice_vscl, bmag, tmpvec, bounds9
    
  out=[[bounds0],[bounds3],[bounds6],[bounds9]]
  
  RETURN, out
  
END