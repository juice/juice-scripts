PRO juice_wake,$
    step=step,$ ;time step in seconds
    trange=trange,$ ;time range (format: trange=['2032-01-11T05:44:04.000','2032-01-12T05:44:04.000'])
    target=target, $
    ps=ps,$ ; export to postscript
    output=output,$ ;postscript file output
    lrestore=lrestore,$ ;restore pre-calculated
    condition=condition,$ ;condition for wake crossing. Two element array [longitude separation, L-shell separation] (units of deg, Rj), e.g. condition=[15.0,0.03], see notes for explanation
    wtype=wtype, $; method for calculating intervals of wake crossing. Possible values 1 or 2 (default is 1)
    plotmake=plotmake ;show some plots, top panel L-shell vs time from JUICE (and moon, if defined), second panel dL & dlongitude vs time, if moon is define
    
;The routine estimates the l-shell and position of up to two targets. First target is always JUICE (even if defined otherwise). Second targetcan be a moon of Jupiter, for now this works only 
;Europa, Callisto, Ganymede. If two targets are defined, the routine estimates also the L-shell and longitude difference between JUICE and the moon and checks if a condition 
;for a wake crossing is satisfied. There are two ways to calculate the wake events:
;
;Method 1 (wtype=1) is the instanteneous L-shell/longitude difference. 
;The script checks if at a given step the longitude and L-shell difference of JUICE and the moon is within a given range of values, defined through the condition keyword.
;E.g. if condition=[15.0, 0.03], then a wake crossing occurs if JUICE is within 15.0 deg downstream of the moon and has an absolute L-shell difference less than 0.03 +rmoon[km]/rj[km],
;where rmoon is the moon radius, and rj is the jupiter radius (71492 km). The addition of the moon radius is done since a moon's finite size covers range in L-shell, essentially
;indicating through condition[1] the L-shell distance of JUICE from the wake flanks. The value of condition[1] is indicative of the expected wake width with respect to a moon's size
;and the expected magnitude of radial flows 

;Method 2 (wtype=2) relies on the residence of JUICE within the L-shell range of a moon. Because of the rotation/magentic axis misalignement, 
;all moons oscillate in L-shell as Jupiter rotates.
;The script checks if at a given time the longitude difference of JUICE and the moon is within a given range, while JUICE is within the moon L-shell range+margin described above.
;The longitude difference range and the L-shell margin are given through the condtion keyword
;E.g. if condition=[15.0, 0.03], then a wake crossing occurs if JUICE is within 15.0 deg downstream of the moon and resides within the moon L-shell range, amplified by 0.03 Rj
;The moon L-shell ranges have been estimated through field line tracing and are defined internally in the script. 
;The moon's radius is not added here because the L-shell oscillation of a moon is far larger. 

;Method 1 (default) is best for close flybys or small longitude separations (few deg). 
;The user is advised to define the allowed L-shell difference with some margin since a moon's wake can be wider
;than its actual diameter

;Method 2 is best for distant wake crossings (more than few deg, not close flybys). It is more relevant because the wake effects can take a considerable time to travel from the moon to 
;JUICE, depending on particle energy and type (ion or electron). When the wake effects arrive at a given L-shell of JUICE, the actual moon L-shell may have been different
;at the time of the wake formation, as the moon moves along its orbit. Because the L-shell range of Callisto is large (26.14-->47.67) wtype=2 is not recommended for Callisto as it gives
;numerous events of long duration. Method 2 typically gives longer wake durations and more events than method 1.

;The wake events, if a moon is defined as target along with JUICE, are stored in an ascii file saved in output_folder (defined below)
;Wake events shorter than 1 time step (step) are not stored in the ascii file
;If a wake event runs until the end of the analysed time range, the end of the time range is marked as its stop time (even if the wake condition is satisfied after that)

;The user can select a short time period event and plot it using plotmake=1 and export it with /ps or ps=1. For long time periods, such plots are not useful.

;Since calculations of L-shells and magnetic parameters are time consuming, the user can use precalculated values through lrestore=1 (or /lrestore) keyword.

;The routine is in part based on juice_lshell.pro

    
;Examples
;juice_wake, step=300d, target=['juice','europa'], ca='2032-07-02T16:22:22.000', condition=[15d, 0.03], /ps, /plotmake ;not using pre-calculated L-shells, makes a plot, method 1
;juice_wake, target=['juice','callisto'], /lrestore, trange=['2031-01-31T23:00:00.000','2034-12-19T00:00:00.000'], condition=[15d, 1.5d],wtype=2;  ;using pre-calculated L-shells, method 2
  
P0=!p ;Save default idl plot settings

LOADCT, 39
!p.thick = 2.5 ;make some modification of initial plot settings for this routine
!x.thick = 2.5
!y.thick = 2.5
!z.thick = 2.5

;some inputs
sav_folder='/Users/roussos/ownCloud/mylib/spice_juice/' ;savefile folder
output_folder='/Users/roussos/' ;where to store plot output (if ps=1 selected)
spice_dir='/Users/roussos/ownCloud/Work/JUICE/SPICE/kernels/' ;SPICE kernels directory\
dirmk = spice_dir + 'mk/' ;metakernels directory
sysval='IAU_JUPITER' ;use this coordinate system it is relevant for magnetic field models

IF (keyword_set(ps) and keyword_set(plotmake)) THEN BEGIN ;If postscript output is required, prepare the environment

  Set_Plot, 'ps'
  !p.font=0
  Device, Color=1, Bits_Per_pixel=8, /Landscape
  IF (N_ELEMENTS(output) EQ 0) THEN output='juice_lshell' ;default name for plot file, if not given

  Device, Filename=output_folder+output+'.ps' ;change here the folder path to save plots

ENDIF

;Define some default values
IF (N_ELEMENTS(wtype) EQ 0) THEN wtype=1 ;default method to define wake conditions
IF (wtype NE 1 AND wtype NE 2) THEN wtype=1 ;make sure wtype=1 or 2
IF (N_ELEMENTS(step) EQ 0) THEN step=120d ;time step for ephemeris in seconds
IF (N_ELEMENTS(trange) EQ 0) THEN trange=['2032-07-01T04:22:11.000','2032-07-04T04:22:11.000'] ; period enclosing Europa flyby 1, CREMA 5.0
IF (N_ELEMENTS(target) GE 1) THEN target=STRTRIM(STRUPCASE(target),2) ;make sure everything is capitalized, spaces removed
IF (N_ELEMENTS(target) EQ 0) THEN target='JUICE' ;default target, if none given
IF (N_ELEMENTS(condition) EQ 0) THEN condition=[15d, 0.05]
testj=WHERE(target NE 'JUICE') ;test if none of the requested targets is equal to JUICE
IF (testj[0] NE -1) THEN target=['JUICE', target[testj]];if no JUICE target defined, make sure JUICE is & it is the first entry and not a duplicate
IF (N_ELEMENTS(target) GT 2) THEN BEGIN; if more than two argets defined, give the following message (code not fully functional for more than 2 targets)
  
  target=target[0:1]
  PRINT, 'Script works only for up to two targets (JUICE and max one moon)'
  PRINT, 'Calculations will be done for JUICE and '+target[1]
  
ENDIF

IF (N_ELEMENTS(clr) EQ 0) THEN BEGIN ;automatically define colors for plotting different targets and plot parameters
  
  IF (N_ELEMENTS(target) NE 1) THEN clr=FINDGEN(N_ELEMENTS(target))*254/(N_ELEMENTS(target)-1l) ELSE clr=0
  IF NOT keyword_set(ps) THEN clr[0]=255
  
ENDIF 

rj=71492.0 ;jupiter radius in km

cd, dirmk ;change to metakernels directory
metakernel=file_search('*.tm', count=mkcount) ;find all metakernels

PRINT, 'SPICE metakernels' ;print metakernel names on screen
PRINT, '-----------------------'
FOR i=0l, mkcount-1l DO BEGIN

  PRINT, STRTRIM(STRING(i),2)+'. '+metakernel[i]

ENDFOR

READ, input_key, PROMPT='Select (0-'+STRTRIM(STRING(mkcount-1l),2)+'):' ;select metakernel
cspice_furnsh, dirmk + metakernel[input_key] ;load selected metakernel

xyzt=get_ephem_juice_generic(date=trange, $
  step=step, sys=sysval, center='JUPITER', target=target[0], unit=rj) ;unit=1 Rj

et_time=xyzt[*,6]

nl=N_ELEMENTS(et_time) ;number of time tags
lval=FLTARR(nl,N_ELEMENTS(target)) ;array to store l-shell values
x=FLTARR(nl,N_ELEMENTS(target)) ;array to store x values
y=FLTARR(nl,N_ELEMENTS(target)) ;array to store y values
z=FLTARR(nl,N_ELEMENTS(target)) ;array to store z values
mageq=FLTARR(nl,N_ELEMENTS(target)) ;array to store predicted magnetic field value at magnetic equator of the estimated L-shell
magloc=FLTARR(nl,N_ELEMENTS(target)) ;array to store predicted magnetic field value at the target location
longitude=FLTARR(nl,N_ELEMENTS(target)) ;array to store target longitude (S3 RH)
londiff=FLTARR(nl,N_ELEMENTS(target)-1) ;array to store longitude difference between JUICE (target 0) and other targets (target 1)
lshelldiff=FLTARR(nl,N_ELEMENTS(target)-1) ;array to store L-shell difference between JUICE (target 0) and other targets (target 1)

FOR k=0l, N_ELEMENTS(target)-1l DO BEGIN ;for every target, start this loop

  PRINT, 'Tracing field line from the position of '+STRUPCASE(target[k])
  xyzt=get_ephem_juice_generic(date=trange, $
    step=step, sys=sysval, center='JUPITER', target=target[k], unit=rj) ;get target position unit=1 Rj
      
  xmag=xyzt[*,0] ;x,y,z components
  ymag=xyzt[*,1]
  zmag=xyzt[*,2]
  
  rval=SQRT(xmag*xmag + ymag*ymag + zmag*zmag) ;radial distance
  plat=ASIN(zmag/rval) ;planetary latitude
  s3lone=ATAN(ymag,xmag) ;s3 longitude RH
  test=WHERE(s3lone LT 0)
  IF (test[0] NE -1) THEN s3lone[test]=2*!PI +s3lone[test] ;convert to 0-->2pi
  
  longitude[*,k]=s3lone*!radeg ; convert s3lone to deg and store to longitude
  x[*,k]=xmag ;store x,y, z
  y[*,k]=ymag
  z[*,k]=zmag
  
  IF keyword_set(lrestore) THEN BEGIN ;retrieve magnetic field traced model traced values from precalculated matrices
  
    IF (k EQ 0) THEN BEGIN
  
      restore, sav_folder+'juicel.sav' ;JUICE is always target[0]
  
    ENDIF ELSE BEGIN
  
      IF (target[k] EQ 'CALLISTO') THEN restore, sav_folder+'juicel_callisto.sav' ;Callisto
      IF (target[k] EQ 'GANYMEDE') THEN restore, sav_folder+'juicel_ganymede.sav' ;Ganymede
      IF (target[k] EQ 'EUROPA') THEN restore, sav_folder+'juicel_europa.sav' ;Europa
  
    ENDELSE
    
  ENDIF
  
  IF NOT keyword_set(lrestore) THEN BEGIN ;if you want to recalculate L-shells with different field model params
    
    PRINT, 'Calculating L-shells...'
    
    FOR i=0l, nl-1l DO BEGIN
    
        tr=trace_jupiter_l(location=[rval[i], plat[i]*!radeg, s3lone[i]*!radeg], ds=0.2, rlimit=70.0) ;always input in Rj, deg, deg. Rlimit prevents tracings beyond 70 Rj
        lval[i,k]=tr[0] ;estimated L-shell
        ;mageq[i,k]=tr[1] ;estimated magnetic field at mag. equator in nT
        ;magloc[i,k]=tr[2] ;estimated local magnetic field in nT
        
        Print, format='(a,a,f6.2,a,$)','      ', string("15B), (i+1)/float(nl)*100, '% complete' 
    
    ENDFOR
    
  ENDIF ELSE BEGIN ;if you read from the precalculated sav files, interpolate to the desired time resolution
    
    lval[*,k]=INTERPOL(ms, et_time_ms, et_time) ;L-shell
    ;mageq[*,k]=INTERPOL(beq, et_time_ms, et_time) ;equatorial magnetic field
    ;magloc[*,k]=INTERPOL(bloc, et_time_ms, et_time) ;local magnetic field
    
  ENDELSE

ENDFOR

IF keyword_set(plotmake) THEN BEGIN ;only when requested to plot the output

  cspice_et2utc, et_time, 'J', 6, julian_time_string; generate time in JD, useful for IDL plot labeling with IDL time tools
  julian_time=DBLARR(N_ELEMENTS(julian_time_string))
  FOR i=0l,N_ELEMENTS(julian_time_string)-1l DO julian_time[i]=DOUBLE(STRSPLIT(julian_time_string[i],'JD ',/EXTRACT))  ;remove the "JD " part of the julian date string and convert to double
  
  tlabels=LABEL_DATE(DATE_FORMAT=['%H:%I:%S','%Y-%M-%D']);format of plot labels for the time axis
  
  PLOT, julian_time, lval[*,0], /xstyle, yrange=minmax(lval), /ystyle, XTICKUNITS = ['Time', 'Time'], position=[0.1,0.55,0.9,0.95],$
    XTICKFORMAT='LABEL_DATE', ytitle='L-shell', chars=1.05, ticklen=-0.02
    
  FOR i=0l, N_ELEMENTS(target)-1 DO OPLOT, julian_time, lval[*,i], thick=3, color=clr[i]
  
  LEGENDARY, target, color=clr, chars=0.85, spacing=0.15, line=0, thick=6, /normal, position=[0.92,0.95]
  
  IF (N_ELEMENTS(ca) EQ 1) THEN BEGIN ;mark an event, e.g. CA of a flyby
  
    cspice_str2et, ca, etca
    cspice_et2utc, etca, 'J', 6, julian_ca_string; generate time in JD, useful for IDL plot labeling
    julian_time_ca=DOUBLE(STRSPLIT(julian_ca_string,'JD ',/EXTRACT))
    OPLOT, [julian_time_ca,julian_time_ca], [0,180], line=2
  
  ENDIF
  
ENDIF

IF (N_ELEMENTS(target) GT 1) THEN BEGIN ;based on the selected target[1], define the radius to estimate JUICE altitude
  
  CASE STRUPCASE(target[1]) OF ;define unit for distance

    'EUROPA': BEGIN
      rmoon=1560.8d ;km
      lrange=[9.2941315, 10.018656] ;lshell range of europa
    END
    'GANYMEDE': BEGIN
      rmoon=2634.1d ;km
      lrange=[14.928878, 16.897790] ;lshell range of ganymede
    END
    'CALLISTO': BEGIN
      rmoon=2410.3d;km
      lrange=[26.140178, 47.674842] ;lshell range of callisto
    END
  ENDCASE
  
  IF (wtype EQ 1) THEN condition[1]=condition[1]+rmoon/rj ;add the moon radius to the allowed L-shell difference, if that is calculated instantaneously (wtype=1)

  FOR i=1l, N_ELEMENTS(target)-1l DO BEGIN ;do this for every target (currently works for target[1], code is generalised if more targets added)

    londiff[*,i-1]=longitude[*,0] - longitude[*,i] ;longitude difference in deg
    lshelldiff[*,i-1]=ABS(lval[*,0] - lval[*,i]) ;absolute L-shell difference (not important for now if dL<0 or >0
    rdiff=SQRT((x[*,0]-x[*,i])^2.0+(y[*,0]-y[*,i])^2.0+(z[*,0]-z[*,i])^2.0)*rj-rmoon ;altitude from moon (target[1])

  ENDFOR

  testd=WHERE(londiff LT 0)
  IF (testd[0] NE -1) THEN londiff[testd]=360.0 + londiff[testd] ;bring longitude differences to the 0-->360 deg space (close to 0 deg is downstream, close to 360 is upstream)
  
  IF (wtype EQ 1) THEN testc=WHERE(londiff LE condition[0] and lshelldiff LE condition[1], nc) ;find condition for wake crossing (type 1, instanteneous L-shell/longitude difference)
  IF (wtype EQ 2) THEN $
        testc=WHERE(londiff LE condition[0] and lval[*,0]+condition[1] GE lrange[0] and lval[*,0]-condition[1] LE lrange[1], nc) ;(type 2 longitude difference within moon L-shell range)

  IF (testc[0] NE -1 and nc GT 1) THEN BEGIN
    
    differ=testc-SHIFT(testc,1)
    ind_start=WHERE(differ NE 1, nd)
    IF (nd GT 1) THEN ind_stop=[ind_start[1:nd-1]-1l, nc-1l] ELSE ind_stop=nc-1l
    cspice_et2utc, et_time[testc[ind_start]], 'ISOC', 1, isocstart; 
    cspice_et2utc, et_time[testc[ind_stop]], 'ISOC', 1, isocstop;
    duration=STRTRIM(STRING(et_time[testc[ind_stop]]-et_time[testc[ind_start]]),2)
    
    nozero=WHERE(duration GT 0d, nnz)
    
    IF (nozero[0] NE -1) THEN BEGIN 
      
      ind_start=ind_start[nozero]
      ind_stop=ind_stop[nozero]
      isocstart=isocstart[nozero]
      isocstop=isocstop[nozero]
      duration=duration[nozero]
      
    ENDIF ELSE BEGIN
      
      nnz=nd
      
    ENDELSE

    minlon=STRARR(nnz)
    minldiff=STRARR(nnz)
    minrdiff=STRARR(nnz)
    
    
    FOR i=0l, nnz-1l DO minlon[i]=STRTRIM(STRING(MIN(londiff[testc[ind_start[i]]:testc[ind_stop[i]]],/NaN)),2)
    FOR i=0l, nnz-1l DO minldiff[i]=STRTRIM(STRING(MIN(lshelldiff[testc[ind_start[i]]:testc[ind_stop[i]]],/NaN)),2)
    FOR i=0l, nnz-1l DO minrdiff[i]=STRTRIM(STRING(MIN(rdiff[testc[ind_start[i]]:testc[ind_stop[i]]],/NaN)),2)
    
    fmt='(A22,1X,A22,1X,A14,1X,A12,1X,A12,1X,A12,1X,A12)'

    filename=output_folder+target[1]+'_WAKE_CROSSINGS_'+metakernel[input_key]+'_Method_'+STRTRIM(STRING(wtype),2)+'.dat' ;define filename
    fsearch=FILE_SEARCH(filename, count=count) ;search if the same file exists from before.
    IF (fsearch EQ 1) THEN file_delete, filename ;make sure not to overwrite on previous file with the same filename & directory
    
    GET_LUN, lun
    OPENW, lun, filename
    PRINTF, lun, ';File generated (UTC): '+systime(/UTC)
    PRINTF, lun, ';Metakernel: '+metakernel[input_key]
    PRINTF, lun, ';Moon: '+target[1]
    PRINTF, lun, ';Time range: '+trange[0] + ' - ' +trange[1]
    PRINTF, lun, ';Time resolution (& minimum wake crossing duration): '+STRTRIM(STRING(step),2)+ ' sec.'
    PRINTF, lun, ';'
    IF (wtype EQ 1) THEN BEGIN 
      
      PRINTF, lun, ';Wake crossing calculation: Method 1 
      PRINTF, lun, '(instanteneous L-shell/longitude difference. Allowed Longitude/L-shell difference defined in conditions)'
      PRINTF, lun, ';'
      
    ENDIF ELSE IF (wtype EQ 2) THEN BEGIN 
      
      PRINTF, lun, ';Wake crossing calculation: Method 2
      PRINTF, lun, ';(L-shell/longitude difference while JUICE within moon''s L-shell range/lrange. Not recommended for Callisto)'
      PRINTF, lun, ';Allowed Longitude difference defined in conditions. Allowed JUICE L-shell is at MIN(lrange)-condition[1]<= L <= MAX(lrange)+condition[1])'
      PRINTF, lun, ';L-shell range for '+target[1]+': '+STRTRIM(string(lrange[0]),2)+' to '+STRTRIM(string(lrange[1]),2)
      PRINTF, lun, ';'
      
    ENDIF
    PRINTF, lun, ';Conditions for JUICE-Moon wake crossing: 
    PRINTF, lun, ';(a) Max. Longitude difference: '+STRTRIM(STRING(condition[0]),2)+' deg'
    PRINTF, lun, ';(b) Max. L-shell difference: '+STRTRIM(STRING(condition[1]),2)
    PRINTF, lun, ';                                              Event             Duration   Lon. diff.   L-shell      Minimum'
    PRINTF, lun, ';Start Date             Stop Date              Type              [sec]      [deg]        diff.        alt.[km]'
    PRINTF, lun, '--------------------------------------------------------------------------------------------------------------'
    PRINTF, lun, TRANSPOSE([[isocstart],[isocstop],[STRARR(nnz)+' '+target[1]+'_WAKE'],[duration],[minlon],[minldiff],[minrdiff]]),$
                 FORMAT = fmt
    FREE_LUN, lun
  ENDIF ELSE BEGIN
    
    PRINT, 'No wake crossings identified for the selected interval'
    
  ENDELSE
  
  IF keyword_set(plotmake) THEN BEGIN 
    
    ;for plotting l-shell vs time difference, do this only between JUICE and target[1] otherwise plot becomes very chaotic for more targets
  
    PLOT, julian_time, londiff[*,0], /xstyle, yrange=[1e-1,360], ylog=1, ystyle=9, XTICKUNITS = ['Time', 'Time'], position=[0.1,0.1,0.9,0.45],$
        XTICKFORMAT='LABEL_DATE', ytitle='Longitude difference [deg]', chars=1.05, ticklen=-0.02, /noerase, color=clr[0]
       
    IF (testc[0] NE -1) THEN OPLOT, julian_time[testc], londiff[testc,0], color=clr[0], psym=4
      
    ;Draw a second, logarithmic axis on the right-hand side of the plot.
    Axis, YAxis=1, YLog=1, YRange=[1e-2, 10], /Save, ytitle='L-shell difference', color=254
  
    ;Overplot the logarithmic data.
    OPLOT, julian_time, lshelldiff[*,0], color=clr[1]
    
    IF (testc[0] NE -1) THEN OPLOT, julian_time[testc], lshelldiff[testc,0], color=clr[1], psym=4 ;plot where condition is satisfied. 
  
    
    LEGENDARY, [cggreek('Delta')+cggreek('phi'),'|'+cggreek('Delta')+'L|'],  line=0, chars=0.85, spacing=0.15, thick=6, /normal, position=[0.95,0.45], color=clr[0:1] ; add a plot legend
    
  ENDIF

ENDIF

!P=p0; revert back to the original plot settings

cspice_kclear ;unload spice kernels

IF (keyword_set(ps) and keyword_set(plotmake)) THEN BEGIN
  
  Device, /Close_file  ; close postscript if my postscript plot calling method is used
  
  IF (strupcase(!version.os_family) EQ 'UNIX') THEN BEGIN ;If you want to convert to pdf and have ps2pdf installed (only works for unix)
    
    cd, output_folder
    spawn, 'ps2pdf '+output+'.ps'
    
  ENDIF
  
ENDIF

IF (strupcase(!version.os_family) EQ 'WINDOWS') THEN BEGIN

  Set_plot, 'Win'

ENDIF ELSE IF (strupcase(!version.os_family) EQ 'UNIX') THEN BEGIN

  Set_plot, 'X'

ENDIF

END