PRO nim_ram_angles,$
step=step,$
trange=trange,$ 
ca=ca,$ 
center=center,$ 
unit=unit,$ 
ps=ps,$ 
output=output,$ 
export=export,$ 
mark_ptr=mark_ptr ,$
mark_event=mark_event,$
event=event,$
dir=dir,$
out_dir=out_dir 

;+
; NAME:
; nim_ram_angles
;
; PURPOSE:
; Calculate NIM FoV angles wrt RAM direction of motion
;
; CATEGORY:
; JUICE pointing scripts
;
; CALLING SEQUENCE:
; nim_ram_angles, [step=step], [trange=trange], [center=center], [unit=unit], [ps=ps], [output=output], [export=export], [mark_ptr=mark_ptr], [mark_event=mark_event],[event=event]
;
; INPUTS:
;
; step: time step in seconds, default is 120 sec
; trange: time range (format example trange=['2032-01-11T05:44:04.000','2032-01-12T05:44:04.000'], time format accepted any UTC format that works for SPICE)
; ca: Mark CA time, if a flyby is plotted e.g. ca='2032-07-02T16:22:11.000'. Default: CA undefined
; center: For 2nd plot panel (distance vs time), define the center from which distance is estimated (options: 'jupiter', 'europa', 'ganymede', 'callisto'). Default is Jupiter
; unit: For 2nd plot panel (distance vs time), define the distance unit (options: 'km', 'jupiter', 'europa', 'ganymede', 'callisto'). Default is Jupiter
; ps: export to postscript
; output: ps output filename
; export: export results in a savefile
; mark_ptr: If a PTR file is available (e.g. from JUICE pointing tool, read and mark PTR events). Selecting this will prompt the user to select a ptr file.
; mark_event: Use SOC segmentation event files to mark events
; event: type of segment to be plotted
; dir: location of SPICE kernels directory. Default can be changed directly in the script
; out_dir: directory to store the output file. . Default can be changed directly in the script.
; 
;
; OUTPUTS:
;
; COMMON BLOCKS:
; None.
;
; SIDE EFFECTS:
; None.
;
; RESTRICTIONS:
; Requires JUICE SPICE kernels, downloaded ptr, event files (if to be used).
;
; PROCEDURE:
; Adjust input folders in the beginning of the script: out_dir, spice_dir
;
; Examples:
; 1)  (Europa 1 flyby, CREMA 5.0)
; nim_ram_angles, center='Europa', /ps, step=120d, ca='2032-07-02T16:22:22.000', trange=['2032-07-02T04:22:11.000','2032-07-03T04:22:11.000']
; 
;
; MODIFICATION HISTORY:
; Written, ER, October, 2022.

;Example (Europa 1 flyby, CREMA 5.0)
;nim_ram_angles, center='Europa', /ps, step=120d, ca='2032-07-02T16:22:22.000', trange=['2032-07-02T04:22:11.000','2032-07-03T04:22:11.000']

P0=!p ;Save default idl plot settings

LOADCT, 39
!p.thick = 2.5 ;make some modification of initial plot settings for this routine
!x.thick = 2.5
!y.thick = 2.5
!z.thick = 2.5

IF (N_ELEMENTS(dir) EQ 0) THEN dir='/Users/roussos/ownCloud/Work/JUICE/SPICE/kernels/' ;SPICE kernels directory
IF (N_ELEMENTS(out_dir) EQ 0) THEN out_dir='/Users/roussos/'

IF keyword_set(ps) THEN BEGIN ;If postscript output is required

  Set_Plot, 'ps'
  !p.font=0
  Device, Color=1, Bits_Per_pixel=8, /Landscape
  IF (N_ELEMENTS(output) EQ 0) THEN output='nim_angle' ;default name for plot file, if not given
  Device, Filename=out_dir+output+'_01'+'.ps' ;change here the folder path to save plots
  clr0=0

ENDIF ELSE BEGIN

  clr0=255

ENDELSE

;Define some default values
IF (N_ELEMENTS(step) EQ 0) THEN step=10d ;time step for ephemeris in seconds
IF (N_ELEMENTS(trange) EQ 0) THEN trange=['2032-07-02T04:22:11.000','2032-07-03T04:22:11.000']
IF (N_ELEMENTS(center) EQ 0) THEN center='europa'

dirmk = dir + 'mk/' ;metakernels directory

cd, dirmk ;change to metakernels directory

metakernel=file_search('*.tm', count=mkcount) ;find all metakernels

PRINT, 'SPICE metakernels' ;print metakernel names on screen
PRINT, '-----------------------'
FOR i=0l, mkcount-1l DO BEGIN

  PRINT, STRTRIM(STRING(i),2)+'. '+metakernel[i]

ENDFOR

READ, input_key, PROMPT='Select (0-'+STRTRIM(STRING(mkcount-1l),2)+'):' ;select metakernel

cspice_furnsh, dirmk + metakernel[input_key] ;load selected metakernel
sysval='JUICE_'+STRTRIM(STRUPCASE(center),2)+'_RAM' ;define moon ram system
xyzt=get_ephem_juice_generic(date=trange, $
  step=step, sys=sysval, center=center, target='JUICE', unit=1.0d)


CASE STRLOWCASE(center) OF ;define center to estimate distance from

  'jupiter': BEGIN
    ytpart2='Jupiter Distance'
    rmoon=rj
  END
  'europa': BEGIN
    ytpart2='Europa Distance'
    rmoon=1560.8d
  END
  'ganymede': BEGIN
    ytpart2='Ganymede Distance'
    rmoon=2634.1d
  END
  'callisto': BEGIN
    ytpart2='Callisto Distance'
    rmoon=2410.3d
  END
ENDCASE
  
et_time=xyzt[*,6] ;assign a variable to the ET-time for simplicity

;Begin the procedure to calculate and store angles to ram

;ang_vel=FLTARR(N_ELEMENTS(et_time)) ;Make arrays to store  angle

alt=SQRT(xyzt[*,0]^2.0 + xyzt[*,1]^2.0 + xyzt[*,2]^2.0)-rmoon ;altitude from moon

;JUICE_PEP_NIM_NEUION       -28531   ;these are the NIM spice frames (neutral and thermal)
;JUICE_PEP_NIM_NEUION_S0    -28532
;JUICE_PEP_NIM_NEUION_S1    -28533
;JUICE_PEP_NIM_NEUION_S2    -28534
;JUICE_PEP_NIM_NEUION_S3    -28535
;JUICE_PEP_NIM_NEUION_S4    -28536
;JUICE_PEP_NIM_NEUION_S5    -28537
;JUICE_PEP_NIM_THERMAL-1    -28538
;JUICE_PEP_NIM_THERMAL-2    -28539

val=([0.,1.,0.]) ;that is just sc-y direction, useful for validating some ram angle calculations
;add the boresights of NIM below
CSPICE_GETFOV, -28532, 6, shape, frame_in_which_fov_was_defined, bsight, bounds ;GET FoV params, store the boresight by expanding the "val" parameter everytime
val=[[val],[bsight]]
CSPICE_GETFOV, -28533, 6, shape, frame_in_which_fov_was_defined, bsight, bounds ;GET FoV params
val=[[val],[bsight]]
CSPICE_GETFOV, -28534, 6, shape, frame_in_which_fov_was_defined, bsight, bounds ;GET FoV params
val=[[val],[bsight]]
CSPICE_GETFOV, -28535, 6, shape, frame_in_which_fov_was_defined, bsight, bounds ;GET FoV params
val=[[val],[bsight]]
CSPICE_GETFOV, -28536, 6, shape, frame_in_which_fov_was_defined, bsight, bounds ;GET FoV params
val=[[val],[bsight]]
CSPICE_GETFOV, -28537, 6, shape, frame_in_which_fov_was_defined, bsight, bounds ;GET FoV params
val=[[val],[bsight]]
CSPICE_GETFOV, -28538, 6, shape, frame_in_which_fov_was_defined, bsight, bounds ;GET FoV params
val=[[val],[bsight]]
CSPICE_GETFOV, -28539, 600, shape, frame_in_which_fov_was_defined, bsight, bounds ;GET FoV params
val=[[val],[bsight]]

ang_nim_cor=FLTARR(N_ELEMENTS(et_time), 9l) ;Make arrays to store ram angles + angle to SC-Y

FOR k=0l, 9-1l DO BEGIN
  FOR i=0l, N_ELEMENTS(et_time)-1l DO BEGIN ;start a loop for all times
    
    IF (k NE 0) THEN BEGIN ;all but the 1st vector are defined in JUICE_PEP_NIM
      
      bsight=val[*,k]
      frame_in_which_fov_was_defined='JUICE_PEP_NIM' 
      CSPICE_PXFORM, frame_in_which_fov_was_defined, sysval, et_time[i], rotate ;rotation matrix into the desired frame (sysval='JUICE_'+STRTRIM(STRUPCASE(center),2)+'_RAM')
      CSPICE_MXV, rotate, bsight, bsight_nim ;make the rotation
      
    ENDIF ELSE IF (k EQ 0) THEN BEGIN ;calculation for the SC-Y view
      
      bsight=val[*,k]
      CSPICE_PXFORM, 'JUICE_SPACECRAFT', sysval, et_time[i], rotate
      CSPICE_MXV, rotate, bsight, bsight_nim ;jup_rot_sys in Jupiter Omega vector in the sysval frame
      
    ENDIF
    
    vel_dir_u = REFORM(xyzt[i,3:5])/SQRT(TOTAL(REFORM(xyzt[i,3:5])^2.0)); get the  unit velocity vector
    
    ang_nim_cor[i,k] = CSPICE_VSEP(vel_dir_u,bsight_nim) * !RADEG
  
  ENDFOR
  
ENDFOR

clr=[0, 254, 200, 150, 100, 50, 25, 0, 254]; define some colors, last two are the same as the two first but are for a different panel (for the neutral mode panel)

cspice_et2utc, et_time, 'J', 6, julian_time_string; generate time in JD, useful for IDL plot labeling with IDL time tools
julian_time=DBLARR(N_ELEMENTS(julian_time_string))
FOR i=0l,N_ELEMENTS(julian_time_string)-1l DO julian_time[i]=DOUBLE(STRSPLIT(julian_time_string[i],'JD ',/EXTRACT))  ;remove the "JD " part of the julian date string and convert to double

tlabels=LABEL_DATE(DATE_FORMAT=['%H:%I:%S','%Y-%M-%D']);format of plot labels for the time axis

FOR i=0l, 6 DO BEGIN
  
  IF (i EQ 0) THEN BEGIN 
    
    PLOT, julian_time, ang_nim_cor[*,i], /xstyle, yrange=[0,180], /ystyle, XTICKUNITS = ['Time', 'Time'], position=[0.1,0.55,0.9,0.95],$
    XTICKFORMAT='LABEL_DATE', ytitle='Ram Angle [deg]', chars=1.05,/nodata, ticklen=-0.02
    mm=minmax(julian_time)
    POLYFILL, [mm[0],mm[1],mm[1],mm[0]],[0,0,10,10], color=cgcolor('Gray'), line_fill=1, orient=45d
  
  ENDIF 
  
  OPLOT, julian_time, ang_nim_cor[*,i], color=clr[i]
  
ENDFOR
  
LEGEND, ['NIM_NEUION_S0','NIM_NEUION_S1','NIM_NEUION_S2','NIM_NEUION_S3','NIM_NEUION_S4','NIM_NEUION_S5'], color=clr[1:6], thick=2, position=[0.92,0.95], chars=0.4, line=0,/normal

IF (N_ELEMENTS(ca) EQ 1) THEN BEGIN ;mark an event, e.g. CA of a flyby

  cspice_str2et, ca, etca
  cspice_et2utc, etca, 'J', 6, julian_ca_string; generate time in JD, useful for IDL plot labeling
  julian_time_ca=DOUBLE(STRSPLIT(julian_ca_string,'JD ',/EXTRACT))
  OPLOT, [julian_time_ca,julian_time_ca], [0,180], line=2

ENDIF

IF keyword_set(mark_ptr) THEN BEGIN

  ptr=read_juice_ptr()

  sz=size(ptr)
  ptrcount=sz[1]

  PRINT, 'PTR blocks' ;print metakernel names on screen
  PRINT, '-----------------------'
  FOR i=0l, ptrcount-1l DO BEGIN

    PRINT, STRTRIM(STRING(i),2)+'. '+ptr[i,0]+' '+ptr[i,1]+' '+ptr[i,2]

  ENDFOR
  input_key=''
  READ, input_key, PROMPT='Select (0-'+STRTRIM(STRING(ptrcount-1l),2)+'). You may select a single event (e.g. 0), or multiple (e.g. 1,4,5,7):' ;select block
  input_key=LONG(STRSPLIT(input_key,',', /extract))

  FOR i=0l, N_ELEMENTS(input_key)-1l DO BEGIN

    tstart=ptr[input_key[i],0]
    cspice_str2et, tstart, etstart
    cspice_et2utc, etstart, 'J', 6, julian_tstart_string; generate time in JD, useful for IDL plot labeling
    julian_time_start=DOUBLE(STRSPLIT(julian_tstart_string,'JD ',/EXTRACT))


    tstop=ptr[input_key[i],1]
    cspice_str2et, tstop, etstop
    cspice_et2utc, etstop, 'J', 6, julian_tstop_string; generate time in JD, useful for IDL plot labeling
    julian_time_stop=DOUBLE(STRSPLIT(julian_tstop_string,'JD ',/EXTRACT))

    POLYFILL, [julian_time_start,julian_time_stop,julian_time_stop,julian_time_start], [0, 0, 180,180], /line_fill, orient=45.0, color=180, spacing=0.05, noclip=0

    comm=STRMID(ptr[input_key[i],2],0,30)

    XYOUTS, 0.5*(julian_time_start+julian_time_stop), 182.0, comm, /DATA, chars=0.45, orient=45.0

  ENDFOR

ENDIF

IF keyword_set(mark_event) THEN BEGIN

  ev=juice_segment_read(type_segment=event)

  IF (ev[0] NE '') THEN BEGIN

    evdesc=ev[0,*]
    evstart=ev[1,*]
    evstop=ev[2,*]
    evtype=ev[3,*]

    FOR i=0l, N_ELEMENTS(evstart)-1l DO BEGIN

      cspice_str2et, evstart[i], etstart
      cspice_et2utc, etstart, 'J', 6, julian_tstart_string; generate time in JD, useful for IDL plot labeling
      julian_time_start=DOUBLE(STRSPLIT(julian_tstart_string,'JD ',/EXTRACT))

      cspice_str2et, evstop[i], etstop
      cspice_et2utc, etstop, 'J', 6, julian_tstop_string; generate time in JD, useful for IDL plot labeling
      julian_time_stop=DOUBLE(STRSPLIT(julian_tstop_string,'JD ',/EXTRACT))

      IF (julian_time_stop GT MIN(julian_time) AND julian_time_start LT MAX(julian_time)) THEN plt=1 ELSE plt=0

      IF (plt EQ 1) THEN BEGIN

        POLYFILL, [julian_time_start,julian_time_stop,julian_time_stop,julian_time_start], [0, 0, 180,180], /line_fill, orient=45.0, color=180, spacing=0.05, noclip=0

        comm=STRMID(evdesc[i],0,30)

        XYOUTS, 0.5*(julian_time_start+julian_time_stop), 182.0, comm, /DATA, chars=0.45, orient=45.0

      ENDIF

    ENDFOR

  ENDIF

ENDIF

FOR i=7,8 DO BEGIN

  IF (i EQ 7) THEN BEGIN 
    
    PLOT, julian_time, ang_nim_cor[*,i], /xstyle, yrange=[0,180], /ystyle, XTICKUNITS = ['Time', 'Time'], position=[0.1,0.1,0.9,0.425],$
    XTICKFORMAT='LABEL_DATE', ytitle='Ram Angle [deg]', chars=1.05,/nodata, ticklen=-0.02,/noerase
    mm=minmax(julian_time)
    POLYFILL, [mm[0],mm[1],mm[1],mm[0]],[0,0,60,60], color=cgcolor('Green'), line_fill=1, orient=45d
  
  ENDIF 
  
  OPLOT, julian_time, ang_nim_cor[*,i], color=clr[i]

ENDFOR

LEGEND, ['NIM_THERMAL-1','NIM_THERMAL-2'], color=[0,254], thick=2, /top, /left, chars=0.4, line=0, position=[0.92,0.425], /normal

IF (N_ELEMENTS(ca) EQ 1) THEN BEGIN ;mark an event, e.g. CA of a flyby

  cspice_str2et, ca, etca
  cspice_et2utc, etca, 'J', 6, julian_ca_string; generate time in JD, useful for IDL plot labeling
  julian_time_ca=DOUBLE(STRSPLIT(julian_ca_string,'JD ',/EXTRACT))
  OPLOT, [julian_time_ca,julian_time_ca], [0,180], line=2

ENDIF

IF keyword_set(mark_ptr) THEN BEGIN

  FOR i=0l, N_ELEMENTS(input_key)-1l DO BEGIN

    tstart=ptr[input_key[i],0]
    cspice_str2et, tstart, etstart
    cspice_et2utc, etstart, 'J', 6, julian_tstart_string; generate time in JD, useful for IDL plot labeling
    julian_time_start=DOUBLE(STRSPLIT(julian_tstart_string,'JD ',/EXTRACT))


    tstop=ptr[input_key[i],1]
    cspice_str2et, tstop, etstop
    cspice_et2utc, etstop, 'J', 6, julian_tstop_string; generate time in JD, useful for IDL plot labeling
    julian_time_stop=DOUBLE(STRSPLIT(julian_tstop_string,'JD ',/EXTRACT))

    POLYFILL, [julian_time_start,julian_time_stop,julian_time_stop,julian_time_start], [0,0,180,180], /line_fill, orient=45.0, color=180, spacing=0.05, noclip=0

  ENDFOR

ENDIF

IF keyword_set(mark_event) THEN BEGIN

  IF (ev[0] NE '') THEN BEGIN

    evdesc=ev[0,*]
    evstart=ev[1,*]
    evstop=ev[2,*]
    evtype=ev[3,*]

    FOR i=0l, N_ELEMENTS(evstart)-1l DO BEGIN

      cspice_str2et, evstart[i], etstart
      cspice_et2utc, etstart, 'J', 6, julian_tstart_string; generate time in JD, useful for IDL plot labeling
      julian_time_start=DOUBLE(STRSPLIT(julian_tstart_string,'JD ',/EXTRACT))

      cspice_str2et, evstop[i], etstop
      cspice_et2utc, etstop, 'J', 6, julian_tstop_string; generate time in JD, useful for IDL plot labeling
      julian_time_stop=DOUBLE(STRSPLIT(julian_tstop_string,'JD ',/EXTRACT))

      IF (julian_time_stop GT MIN(julian_time) AND julian_time_start LT MAX(julian_time)) THEN plt=1 ELSE plt=0

      IF (plt EQ 1) THEN BEGIN

        POLYFILL, [julian_time_start,julian_time_stop,julian_time_stop,julian_time_start], [0, 0, 180,180], /line_fill, orient=45.0, color=180, spacing=0.05, noclip=0

        comm=STRMID(evdesc[i],0,30)

      ENDIF

    ENDFOR

  ENDIF

ENDIF


IF keyword_set(ps) THEN BEGIN
  Device, /Close_file  ; close postscript if my postscript plot calling method is used
  cd, out_dir
  spawn, 'ps2pdf '+output+'_01'+'.ps'
ENDIF

;start new plot panel, below is if ps=1 is defined to generate a new file (rather than a new page in the same file opened for the previous plot)
IF keyword_set(ps) THEN BEGIN ;If postscript output is required

  Set_Plot, 'ps'
  !p.font=0
  Device, Color=1, Bits_Per_pixel=8, /Landscape
  IF (N_ELEMENTS(output) EQ 0) THEN output='nim_angle' ;default name for plot file, if not given
  Device, Filename=out_dir+output+'_02'+'.ps' ;change here the folder path to save plots
  clr0=0

ENDIF ELSE BEGIN

  clr0=255

ENDELSE

PLOT, alt, ang_nim_cor[*,1], yr=[0,180], xtitle='Altitude [km]', ytitle='Ram Angle [deg]', chars=1.1, ticklen=-0.025, /xstyle,$ ;plot against altitude
    position=[0.1,0.55,0.9,0.95], xr=[100, 100000], /ystyle, /xlog,/nodata

POLYFILL, [100,100000,100000,100],[0,0,10,10], color=cgcolor('Gray'), line_fill=1, orient=45d ;indicate regime good for neutral mode
POLYFILL, [100,100000,100000,100],[0,0,60,60], color=cgcolor('Green'), line_fill=1, orient=-45d ;indicate regime good for thermal mode

OPLOT, alt, ang_nim_cor[*,1] ;selected directions
OPLOT, alt, ang_nim_cor[*,7], color=254
OPLOT, alt, ang_nim_cor[*,8], color=200



LEGEND, ['NIM_NEUION_S0','NIM_THERMAL-1','NIM_THERMAL-2'], color=[0,254,200], thick=2, position=[0.92,0.95], chars=0.4, line=0,/normal

!P=p0; revert back to the original plot settings

cd, dirmk
cspice_kclear ;unload spice kernels

IF keyword_set(ps) THEN BEGIN 
  Device, /Close_file  ; close postscript if my postscript plot calling method is used
  cd, out_dir
  spawn, 'ps2pdf '+output+'_02'+'.ps'
ENDIF

IF (strupcase(!version.os_family) EQ 'WINDOWS') THEN BEGIN

  Set_plot, 'Win'

ENDIF ELSE IF (strupcase(!version.os_family) EQ 'UNIX') THEN BEGIN

  Set_plot, 'X'

ENDIF

END