PRO pep_corotation,$
    step=step,$
    trange=trange,$
    sensor=sensor,$
    use_pixels=use_pixels,$ 
    corot=corot,$
    center=center,$ 
    unit=unit,$
    erange=erange, $
    ps=ps,$
    output=output,$ 
    ca=ca,$
    mark_ptr=mark_ptr,$
    mark_event=mark_event,$
    event=event,$
    shade=shade,$
    dir=dir,$
    out_dir=out_dir,$
    noise_sector=noise_sector
    
;+
; NAME:
; pep_corotation
;
; PURPOSE:
; Calculate PEP sensor corotation pointing
;
; CATEGORY:
; JUICE pointing scripts
;
; CALLING SEQUENCE:
; pep_corotation, [step=step], [trange=trange], [sensor=sensor], [use_pixels=use_pixels], [dir=dir], [out_dir=out_dir], [noise_sectors], $
;                  [center=center], [unit=unit], [ps=ps], [output=output],[erange=erange], [mark_ptr=mark_ptr], [mark_event=mark_event]
;
; INPUTS:
;
; step: time step in seconds, default is 120 sec
; trange: time range (format example trange=['2032-01-11T05:44:04.000','2032-01-12T05:44:04.000'], time format accepted any UTC format that works for SPICE)
; sensor: which PEP sensor (options: 'jdc','jei','jeni','joee', 'jeni_py','jeni_my': Note that 'jeni' combines JENI_PY & JENI_MY). Default is JOEE
; use_pixels: For JNA  (if selected, uses more detailed 11 pixel sectored FoV). For JEI indicate if to use CEM group '16', '8a', '8b', '4a','4b','4c','4d'
; ca: Mark CA time, if a flyby is plotted e.g. ca='2032-07-02T16:22:11.000'. Default: CA undefined
; center: For 2nd plot panel (distance vs time), define the center from which distance is estimated (options: 'jupiter', 'europa', 'ganymede', 'callisto'). Default is Jupiter
; unit: For 2nd plot panel (distance vs time), define the distance unit (options: 'km', 'jupiter', 'europa', 'ganymede', 'callisto'). Default is Jupiter
; ps: export to postscript
; output: ps output filename
; erange: JEI only, define if to use the different FoV for low ('L'), medium ('M') or high ('H') energies. Default is 'L'
; mark_ptr: If a PTR file is available (e.g. from JUICE pointing tool, read and mark PTR events). Selecting this will prompt the user to select a ptr file.
; mark_event: Use SOC segmentation event files to mark events
; shade: if set, then shades the area of corotation angle coverage
; dir: location of SPICE kernels directory. Default can be changed directly in the script
; out_dir: directory to store the output file. . Default can be changed directly in the script.
; noise_sector: array of integers indicating which sensor sectors should be discarded as noisy. JDC & JEI --> 0 to 15, JNA (if use_pixels=1): 0-->10, JoEE: 0-->8, JENI-->0 to 1 (PY or MY)
; 
;
; OUTPUTS:
; Depending on the keywords a two panel plot is shown on the screen with the corotation coverage of the selected sensor (top) and a distance/time plot on the bottom panel.
; If ps is set, plot is saved in the location defined by out_dir, with filename defined through output keyword. ca, mark_ptr and mark_event will annotate the plot
;
; COMMON BLOCKS:
; None.
;
; SIDE EFFECTS:
; None.
;
; RESTRICTIONS:
; Requires JUICE SPICE kernels, downloaded ptr, event files (if to be used). Does not yet include JNA, JENI, JoEE. JDC FoV independent of energy. JENI pixels not implemented yet.
;
; PROCEDURE:
; Adjust input folders in the beginning of the script: out_dir, spice_dir
    
; Examples (Europa 1 flyby, CREMA 5.0), assuming 100% (rigid) corotation 
;
; pep_corotation, step=120d, sensor='JDC', corot=1.0, /ps, output='JDC_corotation', ca='2032-07-02T16:22:11.000'
; pep_corotation, step=120d, sensor='JEI', corot=1.0, /ps, output='JEI_corotation', ca='2032-07-02T16:22:11.000', use_pixels='16'
; pep_corotation, step=120d, sensor='JEI', corot=1.0, /ps, output='JEI_corotation', ca='2032-07-02T16:22:11.000', unit='europa', center='europa', use_pixels='8a'
; 
; Examples (Extended period), assuming 80% (rigid) corotation, noise in certain instrument sectors. 2nd example uses the medium energy FoV of JEI, and CEM sector group '8b'
;
; pep_corotation, step=120d, sensor='JNA', /ps, output='JNA_corot', noise_sector=[0, 3, 5], trange=['2032-07-01T16:00:00.000','2032-07-09T04:00:00.000'],/use_pixels, corot=0.8
; pep_corotation, step=120d, sensor='JEI', /ps, output='JEI_corot', noise_sector=[0, 12], trange=['2032-07-01T16:00:00.000','2032-07-09T04:00:00.000'],use_pixels='8b', erange='M',/shade, corot=0.8

P0=!p ;Save default idl plot settings

LOADCT, 39
!p.thick = 2.5 ;make some modification of initial plot settings for this routine
!x.thick = 2.5
!y.thick = 2.5
!z.thick = 2.5

IF (N_ELEMENTS(dir) EQ 0) THEN dir='/Users/roussos/ownCloud/Work/JUICE/SPICE/kernels/'
IF (N_ELEMENTS(out_dir) EQ 0) THEN out_dir='/Users/roussos/'

;some inputs
dirmk = dir + 'mk/' ;metakernels directory
sysval='JUICE_JSS' ;use this coordinate system since the JUICE velocity vector in intertial frame is needed
                                ;Jupiter Inertial Frame at J2000 + has the rotation axis of Jupiter as +z (similar to IAU_JUPITER)
IF keyword_set(ps) THEN BEGIN ;If postscript output is required, prepare the environment

  Set_Plot, 'ps'
  !p.font=0
  Device, Color=1, Bits_Per_pixel=8, /Landscape
  IF (N_ELEMENTS(output) EQ 0) THEN output='pep_corotation' ;default name for plot file, if not given

  Device, Filename=out_dir+output+'.ps' ;change here the folder path to save plots

ENDIF

;Define some default values
IF (N_ELEMENTS(step) EQ 0) THEN step=120d ;time step for ephemeris in seconds
IF (N_ELEMENTS(trange) EQ 0) THEN trange=['2032-07-02T04:22:11.000','2032-07-03T04:22:11.000'] ; Europa flyby 1, CREMA 5.0
IF (N_ELEMENTS(sensor) EQ 0) THEN sensor='jdc'
IF (STRLOWCASE(sensor) NE 'joee' AND STRLOWCASE(sensor) NE 'jdc' AND STRLOWCASE(sensor) NE 'jna' AND STRLOWCASE(sensor) NE 'jeni' $
  AND STRLOWCASE(sensor) NE 'jeni_py' AND STRLOWCASE(sensor) NE 'jeni_my' AND STRLOWCASE(sensor) NE 'jei') THEN sensor='jdc' ;make sure to get a valid sensor definition. Default is jdc
IF (N_ELEMENTS(corot) EQ 0) THEN corot=1.0 ;corotation fraction, e.g. 0.8 is for 80% corotation velocity of the rigid (rigid corotation: Omega * r * cos(latitude)
IF (N_ELEMENTS(unit) EQ 0) THEN unit='jupiter' ;default distance unit is 1 Rj
IF (N_ELEMENTS(center) EQ 0) THEN center='jupiter' ;default coordinate system center is jupiter 
IF (N_ELEMENTS(erange) EQ 0) THEN erange='L' ;default 

IF keyword_set(noise) THEN noise=!Values.F_NaN

rj=71492.0 ;jupiter radius in km
omega=0.0001757 ;angular rotation velocity of Jupiter (rad/sec)

PRINT, 'Angle from corotation will be calculated for PEP/'+STRTRIM(sensor,2)
PRINT,''

cd, dirmk ;change to metakernels directory
metakernel=file_search('*.tm', count=mkcount) ;find all metakernels

PRINT, 'SPICE metakernels' ;print metakernel names on screen
PRINT, '-----------------------'
FOR i=0l, mkcount-1l DO BEGIN

  PRINT, STRTRIM(STRING(i),2)+'. '+metakernel[i]

ENDFOR

READ, input_key, PROMPT='Select (0-'+STRTRIM(STRING(mkcount-1l),2)+'):' ;select metakernel
cspice_furnsh, dirmk + metakernel[input_key] ;load selected metakernel

CASE STRLOWCASE(unit) OF ;define unit for distance

  'km': BEGIN
    unit=1.0d
    ytpart1=' [km]'
  END
  'jupiter': BEGIN
    unit=rj
    ytpart1=' [R!DJ!N]'
  END
  'europa': BEGIN
    unit=1560.8d
    ytpart1=' [R!DE!N]'
  END
  'ganymede': BEGIN
    unit=2634.1d
    ytpart1=' [R!DG!N]'
  END
  'callisto': BEGIN
    unit=2410.3d
    ytpart1=' [R!DC!N]'
  END
ENDCASE

CASE STRLOWCASE(center) OF ;define center to estimate distance from

  'jupiter': BEGIN
    ytpart2='Jupiter Distance'
  END
  'europa': BEGIN
    ytpart2='Europa Distance'
  END
  'ganymede': BEGIN
    ytpart2='Ganymede Distance'
  END
  'callisto': BEGIN
    ytpart2='Callisto Distance'
  END
ENDCASE

yt=ytpart2+ytpart1 ;y-label for second panel

;;;;;GET THE POSITION;;;;;;

xyzt=get_ephem_juice_generic(date=trange, $
  step=step, sys=sysval, center='JUPITER', target='JUICE', unit=1d) ;use this
  
x=xyzt[*,0] ;x,y,z variables
y=xyzt[*,1]
z=xyzt[*,2]
r=SQRT(x*x + y*y + z*z) ;radial distance
colat=ACOS(z/r) ;this is in the 0-->pi range, as required by SPICE
s3lone=ATAN(y,x) ;s3 longitude, east
test=WHERE(s3lone LT 0)
IF (test[0] NE -1) THEN s3lone[test]=2*!PI +s3lone[test] ;convert to 0-->2pi

xyzt_2nd=get_ephem_juice_generic(date=trange, $
  step=step, sys=sysval, center=center, target='JUICE', unit=unit) ;unit=unit so output in "unit" or "unit"/sec
  
x2=xyzt_2nd[*,0] ;x2,y2,z2 variables
y2=xyzt_2nd[*,1]
z2=xyzt_2nd[*,2]
r2=SQRT(x2*x2 + y2*y2 + z2*z2) ;radial distance

et_time=xyzt[*,6] ;assign a variable to the ET-time for simplicity

;Select instrument
;Get boresight and boundary corner vectors to get a good sample of pointings covered
IF (STRLOWCASE(sensor) EQ 'joee') THEN BEGIN

  VAR='INS-28550_SECTOR_DIRECTIONS' ;JoEE boresights of all 9 pixels
  MAXBND=1000
  cspice_gdpool, VAR, 0, MAXBND, values, found
  val=REFORM(values, 3, 9) ;store boresight pointings in val

  crsang=12.0*!dtor ;cross angle
  refang=11.25*!dtor ;reference angle
  refvec1=[0.0,1.0,0.0] ;reference vector 1
  refvec2=[1.0,0.0,0.0] ;reference vector 2 (use if refvec1 is parallel or antiparallel to boresight)
  shape='RECTANGLE'
  frame_in_which_fov_was_defined='JUICE_PEP_JOEE'

  FOR i=0l, 8 DO BEGIN ;construct joee pixel corners, get corner vectors of each pixel

    IF (i EQ 0 or i EQ 8) THEN refvec=refvec2 ELSE refvec=refvec1
    temp=spice_get_rectangular_corners(bsight=val[*,i], refang=refang, crsang=crsang, refvec=refvec)
    bounds=[[temp],[temp[*,0]]]

    bounds_int=spice_fov_interpolate(bounds_in=bounds, npoints=3.0, fshape=shape)

    IF (i EQ 0) THEN val2=bounds_int ELSE val2=[[[val2]],[[bounds_int]]] ;corners stored in val2

  ENDFOR
  


  IF (N_ELEMENTS(noise_sector) NE 0) THEN val2[*,*,noise_sector]=!Values.F_NaN

  IF keyword_set(high) THEN val=val2[*,*,[0,4,8]] ELSE val=val2

  nv=SIZE(val)
  nvec=nv[2] ;number of pointing vectors
  nbs=nv[3] ;number of boresights

ENDIF ELSE IF (STRLOWCASE(sensor) EQ 'jdc') THEN BEGIN

  jdc_pixels=2851000l +LINDGEN(192) ;JDC boresights 192 pixels
  
  CSPICE_GETFVN, 'JUICE_PEP_JDC', 6, shape_center, frame_center, bsight_center, bounds_center
  
  FOR i=0l, N_ELEMENTS(jdc_pixels)-1 DO BEGIN ;get pointings of boundaries of 192 JDC pixels

    CSPICE_GETFOV, -jdc_pixels[i], 6, shape, frame_in_which_fov_was_defined, bsight, bounds ;GET FoV params
    bounds=[[bounds],[bounds[*,0]]]
    bounds_int=spice_fov_interpolate(bounds_in=bounds, npoints=3.0, fshape=shape)
    IF (i EQ 0) THEN val=bounds_int ELSE val=[[val],[bounds_int]];

  ENDFOR

  val=reform(val,[3,144,16])

  nv=SIZE(val)
  nvec=nv[2] ;number of pointing vectors
  nbs=nv[3]

  IF (N_ELEMENTS(noise_sector) NE 0) THEN val[*,*,noise_sector]=!Values.F_NaN

ENDIF ELSE IF (STRLOWCASE(sensor) EQ 'jei') THEN BEGIN

  IF (N_ELEMENTS(use_pixels) EQ 0) THEN use_pixels='16'
  IF (N_ELEMENTS(erange) EQ 0) THEN erange='L'

  CASE STRLOWCASE(use_pixels) OF ;define center to estimate distance from

    '16': BEGIN
      jei_pixels_name=['JUICE_PEP_JEI_00_'+erange, $
        'JUICE_PEP_JEI_01_'+erange, $
        'JUICE_PEP_JEI_02_'+erange, $
        'JUICE_PEP_JEI_03_'+erange, $
        'JUICE_PEP_JEI_04_'+erange, $
        'JUICE_PEP_JEI_05_'+erange, $
        'JUICE_PEP_JEI_06_'+erange, $
        'JUICE_PEP_JEI_07_'+erange, $
        'JUICE_PEP_JEI_08_'+erange, $
        'JUICE_PEP_JEI_09_'+erange, $
        'JUICE_PEP_JEI_10_'+erange, $
        'JUICE_PEP_JEI_11_'+erange, $
        'JUICE_PEP_JEI_12_'+erange, $
        'JUICE_PEP_JEI_13_'+erange, $
        'JUICE_PEP_JEI_14_'+erange, $
        'JUICE_PEP_JEI_15_'+erange]
    END
    '8a': BEGIN
      jei_pixels_name=['JUICE_PEP_JEI_00_'+erange, $
        'JUICE_PEP_JEI_02_'+erange, $
        'JUICE_PEP_JEI_04_'+erange, $
        'JUICE_PEP_JEI_06_'+erange, $
        'JUICE_PEP_JEI_08_'+erange, $
        'JUICE_PEP_JEI_10_'+erange, $
        'JUICE_PEP_JEI_12_'+erange, $
        'JUICE_PEP_JEI_14_'+erange]

      IF (N_ELEMENTS(noise_sector) NE 0) THEN BEGIN

        sector_array=[0,2,4,6,8,10,12,14]
        FOR ns=0l, N_ELEMENTS(noise_sector)-1l DO BEGIN

          IF (ns EQ 0) THEN tmp=[]
          tst=WHERE(noise_sector[ns] EQ sector_array)
          IF (tst[0] NE -1) THEN tmp=[tmp, tst]

        ENDFOR

        noise_sector=tmp

      ENDIF

    END
    '8b': BEGIN
      jei_pixels_name=['JUICE_PEP_JEI_01_'+erange, $
        'JUICE_PEP_JEI_03_'+erange, $
        'JUICE_PEP_JEI_05_'+erange, $
        'JUICE_PEP_JEI_07_'+erange, $
        'JUICE_PEP_JEI_09_'+erange, $
        'JUICE_PEP_JEI_11_'+erange, $
        'JUICE_PEP_JEI_13_'+erange, $
        'JUICE_PEP_JEI_15_'+erange]

      IF (N_ELEMENTS(noise_sector) NE 0) THEN BEGIN

        sector_array=[1,3,5,7,9,11,13,15]
        FOR ns=0l, N_ELEMENTS(noise_sector)-1l DO BEGIN

          IF (ns EQ 0) THEN tmp=[]
          tst=WHERE(noise_sector[ns] EQ sector_array)
          IF (tst[0] NE -1) THEN tmp=[tmp, tst]

        ENDFOR

        noise_sector=tmp

      ENDIF

    END
    '4a': BEGIN
      jei_pixels_name=['JUICE_PEP_JEI_00_'+erange, $
        'JUICE_PEP_JEI_04_'+erange, $
        'JUICE_PEP_JEI_08_'+erange, $
        'JUICE_PEP_JEI_12_'+erange]

      IF (N_ELEMENTS(noise_sector) NE 0) THEN BEGIN

        sector_array=[0,4,8,12]
        
        FOR ns=0l, N_ELEMENTS(noise_sector)-1l DO BEGIN

          IF (ns EQ 0) THEN tmp=[]
          tst=WHERE(noise_sector[ns] EQ sector_array)
          IF (tst[0] NE -1) THEN tmp=[tmp, tst]
        
        ENDFOR

        noise_sector=tmp

      ENDIF

    END
    '4b': BEGIN
      jei_pixels_name=['JUICE_PEP_JEI_01_'+erange, $
        'JUICE_PEP_JEI_05_'+erange, $
        'JUICE_PEP_JEI_09_'+erange, $
        'JUICE_PEP_JEI_13_'+erange]

      IF (N_ELEMENTS(noise_sector) NE 0) THEN BEGIN

        sector_array=[1,5,9,13]
        FOR ns=0l, N_ELEMENTS(noise_sector)-1l DO BEGIN

          IF (ns EQ 0) THEN tmp=[]
          tst=WHERE(noise_sector[ns] EQ sector_array)
          IF (tst[0] NE -1) THEN tmp=[tmp, tst]

        ENDFOR

        noise_sector=tmp

      ENDIF

    END
    '4c': BEGIN
      jei_pixels_name=['JUICE_PEP_JEI_02_'+erange, $
        'JUICE_PEP_JEI_06_'+erange, $
        'JUICE_PEP_JEI_10_'+erange, $
        'JUICE_PEP_JEI_14_'+erange]

      IF (N_ELEMENTS(noise_sector) NE 0) THEN BEGIN

        sector_array=[2,6,10,14]
        FOR ns=0l, N_ELEMENTS(noise_sector)-1l DO BEGIN

          IF (ns EQ 0) THEN tmp=[]
          tst=WHERE(noise_sector[ns] EQ sector_array)
          IF (tst[0] NE -1) THEN tmp=[tmp, tst]

        ENDFOR

        noise_sector=tmp

      ENDIF

    END
    '4d': BEGIN
      jei_pixels_name=['JUICE_PEP_JEI_03_'+erange, $
        'JUICE_PEP_JEI_07_'+erange, $
        'JUICE_PEP_JEI_11_'+erange, $
        'JUICE_PEP_JEI_15_'+erange]

      IF (N_ELEMENTS(noise_sector) NE 0) THEN BEGIN

        sector_array=[3,7,11,15]
        FOR ns=0l, N_ELEMENTS(noise_sector)-1l DO BEGIN

          IF (ns EQ 0) THEN tmp=[]
          tst=WHERE(noise_sector[ns] EQ sector_array)
          IF (tst[0] NE -1) THEN tmp=[tmp, tst]

        ENDFOR

        noise_sector=tmp

      ENDIF

    END

  ENDCASE

  FOR i=0l, N_ELEMENTS(jei_pixels_name)-1 DO BEGIN ;get pointings of boundaries of 48 JEI pixels

    cspice_bods2c, jei_pixels_name[i], code, found
    CSPICE_GETFOV, code, 6, shape, frame_in_which_fov_was_defined, bsight, bounds ;GET FoV params
    bounds=[[bounds],[bounds[*,0]]]
    bounds_int=spice_fov_interpolate(bounds_in=bounds, npoints=10.0, fshape=shape)
    IF (i EQ 0) THEN val=bounds_int ELSE val=[[[val]],[[bounds_int]]]

  ENDFOR
  
  CSPICE_GETFVN, 'JUICE_PEP_JEI', 6, shape_center, frame_center, bsight_center, bounds_center

  IF (N_ELEMENTS(noise_sector) NE 0) THEN val[*,*,noise_sector]=!Values.F_NaN
  
  nv=SIZE(val)
  nvec=nv[2] ;number of pointing vectors
  nbs=nv[3] ;number of boresights

ENDIF ELSE IF (STRLOWCASE(sensor) EQ 'jeni' OR STRLOWCASE(sensor) EQ 'jeni_py' OR STRLOWCASE(sensor) EQ 'jeni_my') THEN BEGIN

  ;JUICE_PEP_JENI_PY
  CSPICE_GETFOV, -28561, 6, shape, frame_in_which_fov_was_defined, bsight, bounds ;GET FoV params JENI PY
  bounds=[[bounds],[bounds[*,0]]]
  bounds_int=spice_fov_interpolate(bounds_in=bounds, npoints=10.0, fshape=shape)
  val1=bounds_int
  nv1=SIZE(val1)

  ;JUICE_PEP_JENI_MY
  CSPICE_GETFOV, -28562, 6, shape, frame_in_which_fov_was_defined, bsight, bounds ;GET FoV params JENI MY
  bounds=[[bounds],[bounds[*,0]]]
  bounds_int=spice_fov_interpolate(bounds_in=bounds, npoints=10.0, fshape=shape)
  val2=bounds_int
  nv2=SIZE(val2)

  IF (STRLOWCASE(sensor) EQ 'jeni') THEN val=[[[val1]],[[val2]]] ;IF sensor is JENI (both units in ion mode) combine JENI_PY & MY
  IF (STRLOWCASE(sensor) EQ 'jeni_py') THEN val=REFORM(val1,[3,nv1[2],1]) ;IF sensor in ion mode is only JENI_PY
  IF (STRLOWCASE(sensor) EQ 'jeni_my') THEN val=REFORM(val2,[3,nv2[2],1]) ;IF sensor in ion mode is only JENI_MY

  IF (N_ELEMENTS(noise_sector) NE 0) THEN val[*,*,noise_sector]=!Values.F_NaN

  nv=SIZE(val)
  nvec=nv[2] ;number of pointing vectors
  nbs=nv[3] ;number of boresights

ENDIF ELSE IF (STRLOWCASE(sensor) EQ 'jna') THEN BEGIN

  IF keyword_set(use_pixels) THEN BEGIN

    jna_pixels=285201l +LINDGEN(11) ;directionality detector

    FOR i=0l, N_ELEMENTS(jna_pixels)-1 DO BEGIN ;get pointings, store in valradem

      CSPICE_GETFOV, -jna_pixels[i], 6, shape, frame_in_which_fov_was_defined, bsight, bounds ;GET FoV params
      bounds=[[bounds],[bounds[*,0]]]
      bounds_int=spice_fov_interpolate(bounds_in=bounds, npoints=3.0, fshape=shape)

      IF (i EQ 0) THEN val=bounds_int ELSE val=[[[val]],[[bounds_int]]];

    ENDFOR

  ENDIF ELSE BEGIN

    CSPICE_GETFOV, -28520l, 6, shape, frame_in_which_fov_was_defined, bsight, bounds ;GET FoV params
    bounds=[[bounds],[bounds[*,0]]]
    bounds_int=spice_fov_interpolate(bounds_in=bounds, npoints=10.0, fshape=shape)

    nv=SIZE(bounds_int)
    nvec=nv[2] ;number of pointing vectors
    val=REFORM(bounds_int,[3,nvec,1])

  ENDELSE

  IF (N_ELEMENTS(noise_sector) NE 0) THEN val[*,*,noise_sector]=!Values.F_NaN

  nv=SIZE(val)
  nvec=nv[2] ;number of pointing vectors
  nbs=nv[3] ;number of boresights

ENDIF
;Begin the procedure to calculate and store corotation angles
;jup_rot = [0.,0.,1.] ;jupiter rotation vector in IAU_JUPITER ir other systems like JUPITER_JSS
u_corotation_dir = [0d,0d,1.0d]; get the corotation unit vector in JUICE_JUPITER_RTP
corotation= []

FOR i=0l, N_ELEMENTS(et_time)-1l DO BEGIN ;start a loop for all times
  
  CSPICE_PXFORM, sysval, 'JUICE_JUPITER_RTP', et_time[i], rotatef ;rotation matrix into RTP which has +z along corotation
  CSPICE_MXV, rotatef, TRANSPOSE(xyzt[i,3:5]), juice_vel_rtp_interial ;make the rotation in the desired frame
  
  corotation_temp=u_corotation_dir*corot*omega*r[i]*sin(colat[i]) - juice_vel_rtp_interial ;shift to the JUICE frame (subtract JUICE velocity in JUICE_JUPITER_IF_J2000)
  corotation=[[corotation],[corotation_temp]]
  
ENDFOR

corotation_angle=FLTARR(N_ELEMENTS(et_time), nvec, nbs) ;Make arrays to store corot angle to JDC/JEI pixel coordinates
IF (STRLOWCASE(sensor) EQ 'jdc' OR STRLOWCASE(sensor) EQ 'jei') THEN corotation_angle_center=FLTARR(N_ELEMENTS(et_time))

FOR i=0l, N_ELEMENTS(et_time)-1l  DO BEGIN
  
  CSPICE_PXFORM, frame_in_which_fov_was_defined, 'JUICE_JUPITER_RTP', et_time[i], rotate_vec ;rotation matrix into the desired frame (JUICE_JUPITER_IF_J2000)
  
  FOR m=0l, nbs-1l DO BEGIN
      
    FOR k=0, nvec-1l DO BEGIN
    
      bsighttemp=val[*,k,m] ;value of pointing vector k
      
      IF (bsighttemp[0]*0.0 EQ 0.0) THEN BEGIN
      
        CSPICE_MXV, rotate_vec, bsighttemp, bsight_final ;make the rotation in the desired frame
      
        corotation_angle[i, k, m]=180.0 - CSPICE_VSEP(corotation[*,i], bsight_final) * !RADEG ; subtract from 180 since we want the corotation angle into the FoV of the sensor
        
      ENDIF ELSE BEGIN
        
        corotation_angle[i, k, m]=!Values.F_Nan
        
      ENDELSE
      
    ENDFOR
    
    IF (i EQ N_ELEMENTS(et_time)-1l) THEN PRINT, 'Calculation of corotation angles for ',sensor,' sector ', STRTRIM(STRING(m+1),2), ' out of ', STRTRIM(STRING(nbs),2), ' completed' 
  
  ENDFOR
  
  IF (STRLOWCASE(sensor) EQ 'jdc' OR STRLOWCASE(sensor) EQ 'jei') THEN BEGIN 

    CSPICE_MXV, rotate_vec, bsight_center, bsight_center_final ;make the rotation in the desired frame

    corotation_angle_center[i]=180.0 - CSPICE_VSEP(corotation[*,i], bsight_center_final) * !RADEG ; subtract from 180 since we want the corotation angle into the FoV of the sensor
    
  ENDIF
  
ENDFOR
  
;Corotation angle time series are now available for each JEI/JDC pixel and their central boresight
;Below is a simple procedure to plot corotation coverage for the given period

minval=FLTARR(N_ELEMENTS(et_time), nbs);array to store value of minimum corot angle covered
maxval=FLTARR(N_ELEMENTS(et_time), nbs);array to store value of maximum corot angle covered

FOR i=0l, N_ELEMENTS(et_time)-1l DO BEGIN ;loop for all times to get min/max values above
      
  FOR k=0l, nbs-1l DO BEGIN 

    minval[i,k]=MIN(corotation_angle[i,*, k],/NaN)
    maxval[i,k]=MAX(corotation_angle[i,*, k],/NaN)
    
  ENDFOR

ENDFOR

cspice_et2utc, et_time, 'J', 6, julian_time_string; generate time in JD, useful for IDL plot labeling with IDL time tools
julian_time=DBLARR(N_ELEMENTS(julian_time_string))
FOR i=0l,N_ELEMENTS(julian_time_string)-1l DO julian_time[i]=DOUBLE(STRSPLIT(julian_time_string[i],'JD ',/EXTRACT))  ;remove the "JD " part of the julian date string and convert to double

tlabels=LABEL_DATE(DATE_FORMAT=['%H:%I:%S','%Y-%M-%D']);format of plot labels for the time axis

PLOT, julian_time, minval, /xstyle, yrange=[0,180], /ystyle, XTICKUNITS = ['Time', 'Time'], position=[0.1,0.55,0.9,0.95],$
  XTICKFORMAT='LABEL_DATE', ytitle='Angle from corotation ('+STRUPCASE(sensor)+') [deg]', chars=1.25,/nodata, ticklen=-0.02
  
IF (STRLOWCASE(sensor) NE 'jdc' AND STRLOWCASE(sensor) NE 'jei') THEN FOR k=0l, nbs-1l DO POLYFILL, [julian_time, REVERSE(julian_time)],[minval[*,k], REVERSE(maxval[*,k])], color=254;/(nbs+1)*(k+1)

IF (STRLOWCASE(sensor) EQ 'jdc' OR STRLOWCASE(sensor) EQ 'jei') THEN BEGIN
  
  IF keyword_set(shade) THEN FOR k=0l, nbs-1l DO POLYFILL, [julian_time, REVERSE(julian_time)],[minval[*,k], REVERSE(maxval[*,k])], color=254;/(nbs+1)*(k+1)
  OPLOT, julian_time, corotation_angle_center, thick=3
  mm=[MIN(julian_time), MAX(julian_time)]
  OPLOT, [mm[0],mm[1]],[80,80], color=cgcolor('green'), thick=3 ;mark upper limit angle for which corot is within JDC/JEI FoV
  
ENDIF 

IF (N_ELEMENTS(ca) EQ 1) THEN BEGIN

  cspice_str2et, ca, etca
  cspice_et2utc, etca, 'J', 6, julian_ca_string; generate time in JD, useful for IDL plot labeling
  julian_time_ca=DOUBLE(STRSPLIT(julian_ca_string,'JD ',/EXTRACT))
  OPLOT, [julian_time_ca,julian_time_ca], [0,180], line=2

ENDIF

IF keyword_set(mark_ptr) THEN BEGIN

  ptr=read_juice_ptr()

  sz=size(ptr)
  ptrcount=sz[1]

  PRINT, 'PTR blocks' ;print metakernel names on screen
  PRINT, '-----------------------'
  FOR i=0l, ptrcount-1l DO BEGIN

    PRINT, STRTRIM(STRING(i),2)+'. '+ptr[i,0]+' '+ptr[i,1]+' '+ptr[i,2]

  ENDFOR
  input_key=''
  READ, input_key, PROMPT='Select (0-'+STRTRIM(STRING(ptrcount-1l),2)+'). You may select a single event (e.g. 0), or multiple (e.g. 1,4,5,7):' ;select block
  input_key=LONG(STRSPLIT(input_key,',', /extract))

  FOR i=0l, N_ELEMENTS(input_key)-1l DO BEGIN

    tstart=ptr[input_key[i],0]
    cspice_str2et, tstart, etstart
    cspice_et2utc, etstart, 'J', 6, julian_tstart_string; generate time in JD, useful for IDL plot labeling
    julian_time_start=DOUBLE(STRSPLIT(julian_tstart_string,'JD ',/EXTRACT))


    tstop=ptr[input_key[i],1]
    cspice_str2et, tstop, etstop
    cspice_et2utc, etstop, 'J', 6, julian_tstop_string; generate time in JD, useful for IDL plot labeling
    julian_time_stop=DOUBLE(STRSPLIT(julian_tstop_string,'JD ',/EXTRACT))

    POLYFILL, [julian_time_start,julian_time_stop,julian_time_stop,julian_time_start], [0, 0, 180,180], /line_fill, orient=45.0, color=180, spacing=0.05, noclip=0

    comm=STRMID(ptr[input_key[i],2],0,30)

    XYOUTS, 0.5*(julian_time_start+julian_time_stop), 182.0, comm, /DATA, chars=0.45, orient=45.0

  ENDFOR

ENDIF

IF keyword_set(mark_event) THEN BEGIN

  ev=juice_segment_read(type_segment=event)
  
  IF (ev[0] NE '') THEN BEGIN
     
    evdesc=ev[0,*]
    evstart=ev[1,*]
    evstop=ev[2,*]
    evtype=ev[3,*]
    
  
    FOR i=0l, N_ELEMENTS(evstart)-1l DO BEGIN
  
      cspice_str2et, evstart[i], etstart
      cspice_et2utc, etstart, 'J', 6, julian_tstart_string; generate time in JD, useful for IDL plot labeling
      julian_time_start=DOUBLE(STRSPLIT(julian_tstart_string,'JD ',/EXTRACT))
  
      cspice_str2et, evstop[i], etstop
      cspice_et2utc, etstop, 'J', 6, julian_tstop_string; generate time in JD, useful for IDL plot labeling
      julian_time_stop=DOUBLE(STRSPLIT(julian_tstop_string,'JD ',/EXTRACT))
  
      IF (julian_time_stop GT MIN(julian_time) AND julian_time_start LT MAX(julian_time)) THEN plt=1 ELSE plt=0
  
      IF (plt EQ 1) THEN BEGIN
  
        POLYFILL, [julian_time_start,julian_time_stop,julian_time_stop,julian_time_start], [0, 0, 180,180], /line_fill, orient=45.0, color=180, spacing=0.05, noclip=0
  
        comm=STRMID(evdesc[i],0,30)
  
        XYOUTS, 0.5*(julian_time_start+julian_time_stop), 182.0, comm, /DATA, chars=0.45, orient=45.0
  
      ENDIF
  
    ENDFOR
    
  ENDIF 

ENDIF

;2nd panel plot

PLOT, julian_time, r2, /xstyle, yrange=[MIN(r2), MAX(r2)], /ystyle, XTICKUNITS = ['Time', 'Time'], position=[0.1,0.1,0.9,0.425],$
  XTICKFORMAT='LABEL_DATE', ytitle=yt, chars=1.25, ticklen=-0.02, thick=3, /noerase

IF (N_ELEMENTS(ca) EQ 1) THEN BEGIN ;mark an event, e.g. CA of a flyby

  cspice_str2et, ca, etca
  cspice_et2utc, etca, 'J', 6, julian_ca_string; generate time in JD, useful for IDL plot labeling
  julian_time_ca=DOUBLE(STRSPLIT(julian_ca_string,'JD ',/EXTRACT))
  OPLOT, [julian_time_ca,julian_time_ca], [0,180], line=2

ENDIF

IF keyword_set(mark_ptr) THEN BEGIN

  FOR i=0l, N_ELEMENTS(input_key)-1l DO BEGIN

    tstart=ptr[input_key[i],0]
    cspice_str2et, tstart, etstart
    cspice_et2utc, etstart, 'J', 6, julian_tstart_string; generate time in JD, useful for IDL plot labeling
    julian_time_start=DOUBLE(STRSPLIT(julian_tstart_string,'JD ',/EXTRACT))

    tstop=ptr[input_key[i],1]
    cspice_str2et, tstop, etstop
    cspice_et2utc, etstop, 'J', 6, julian_tstop_string; generate time in JD, useful for IDL plot labeling
    julian_time_stop=DOUBLE(STRSPLIT(julian_tstop_string,'JD ',/EXTRACT))

    POLYFILL, [julian_time_start,julian_time_stop,julian_time_stop,julian_time_start], [MIN(r2), MIN(r2), MAX(r2), MAX(r2)], /line_fill, orient=45.0, color=180, spacing=0.05, noclip=0

  ENDFOR

ENDIF

IF keyword_set(mark_event) THEN BEGIN

  IF (ev[0] NE '') THEN BEGIN

    FOR i=0l, N_ELEMENTS(evstart)-1l DO BEGIN
  
      cspice_str2et, evstart[i], etstart
      cspice_et2utc, etstart, 'J', 6, julian_tstart_string; generate time in JD, useful for IDL plot labeling
      julian_time_start=DOUBLE(STRSPLIT(julian_tstart_string,'JD ',/EXTRACT))
  
      cspice_str2et, evstop[i], etstop
      cspice_et2utc, etstop, 'J', 6, julian_tstop_string; generate time in JD, useful for IDL plot labeling
      julian_time_stop=DOUBLE(STRSPLIT(julian_tstop_string,'JD ',/EXTRACT))
  
      IF (julian_time_stop GT MIN(julian_time) AND julian_time_start LT MAX(julian_time)) THEN plt=1 ELSE plt=0
  
      IF (plt EQ 1) THEN POLYFILL, [julian_time_start,julian_time_stop,julian_time_stop,julian_time_start], [MIN(r2), MIN(r2), MAX(r2), MAX(r2)], $
        /line_fill, orient=45.0, color=180, spacing=0.05, noclip=0
  
    ENDFOR
  
  ENDIF 

ENDIF

save, filename=out_dir+output+'.sav', et_time, corotation_angle, sensor, trange

!P=p0; revert back to the original plot settings

cd, dirmk
cspice_kclear ;unload spice kernels

IF keyword_set(ps) THEN BEGIN
  
  Device, /Close_file  ; close postscript if my postscript plot calling method is used
  
  IF (strupcase(!version.os_family) EQ 'UNIX') THEN BEGIN ;If you want to convert to pdf and have ps2pdf installed (only works for unix)
    
    cd, out_dir
    spawn, 'ps2pdf '+output+'.ps'
    
  ENDIF
  
ENDIF

IF (strupcase(!version.os_family) EQ 'WINDOWS') THEN BEGIN

  Set_plot, 'Win'

ENDIF ELSE IF (strupcase(!version.os_family) EQ 'UNIX') THEN BEGIN

  Set_plot, 'X'

ENDIF

END