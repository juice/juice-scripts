FUNCTION get_ephem_juice_generic, date=date, step=step, init=init, target=target, center=center, sys=sys, unit=unit, icy=icy, dir=dir

  ;+
  ; NAME:
  ; get_ephem_juice_generic
  ;
  ; PURPOSE:
  ; Generate ephemeris data (position, velocity) for selected object and coordinate system
  ;
  ; CATEGORY:
  ; JUICE orbit scripts
  ;
  ; CALLING SEQUENCE:
  ; out=get_ephem_juice_generic(date=date, step=step, [init=init], [icy=icy], [target=target], [center=center], [sys=sys], [unit=unit])
  ;
  ; INPUTS:
  ; 
  ; date: Date range in calendar input format accepted in SPICE. Two element string array, e.g. date=['2032-01-11T05:44:04.000','2032-01-12T05:44:04.000']
  ; step: Time step in seconds, e.g. step=60d (60 sec time step)
  ; init: If set, SPICE is initialised by calling the IDL ICY module (icy.dlm)
  ; icy: If init is set, define the location of the IDL icy module. Default is icy='/Users/roussos/data/SPICE/icy/lib/icy.dlm'
  ; target: SPICE Object to calculate ephemeris for. Default is 'JUICE'
  ; center: SPICE center for the coordinate system origin. Default is 'JUPITER'
  ; sys: Coordinate system for generating ephemeris. Default is IAU_JUPITER
  ; unit: Unit of the output in km. E.g. to export the output in Rj, Rj/sec (Rj: Jupiter radius o 71492 km), set unit=71492.0. Default is unit=1d (1 km)
  ; dir: Location of the JUICE SPICE kernels, relevant only if /init is set  ; 
  ; 
  ; OUTPUTS:
  ; STRING array of [No of time steps, 7]
  ; Column 0-2: x,y,z position (in defined unit)
  ; Column 3-5: vx, vy, vz velocity (in defined unit)
  ; Column 7: Time in ET seconds (J2000) 
  ;
  ; COMMON BLOCKS:
  ; None.
  ;
  ; SIDE EFFECTS:
  ; None.
  ;
  ; RESTRICTIONS:
  ; Requires JUICE SPICE kernels. Can work with any spice kernel set, but some default values wont be accepted in JUICE kernels not loaded (e.g. target='JUICE')
  ;
  ; PROCEDURE:
  ; 1) Generate position, velocity of JUICE for ['2032-01-11T00:00:00.000','2032-03-11T00:00:00.000'], in IAU_JUPITER, centered at JUPITER, in Rj unit, 1 minute time step
  ; 
  ; Rj=71492d ;Rj in km
  ; cd, '/Users/roussos/owncloud/Work/JUICE/SPICE/kernels/mk' ; Load SPICE metakernels. Go to the metakernel folder. This is necessary if the PATH in the mk file is given in relative form 
  ; cspice_furnsh, '/Users/roussos/owncloud/Work/JUICE/SPICE/kernels/mk/juice_crema_5_0b23_1.tm' ;load a JUICE metakernel
  ; out=get_ephem_juice_generic(date=['2032-01-11T00:00:00.000','2032-03-11T00:00:00.000'], target='JUICE', center='Jupiter', sys='IAU_JUPITER', unit=Rj, step=60d)
  
  ; 2) Same as above, for the orbit of Europa  during '2032-01-11T00:00:00.000','2032-01-15T00:00:00.000', in JUICE_JUPITER_MAG_S3RH2009, centered at JUPITER, in Rj unit,1 minute time step
  ; 
  ; Rj=71492d ;Rj in km
  ; cd, '/Users/roussos/owncloud/Work/JUICE/SPICE/kernels/mk' ; Load SPICE metakernels. Go to the metakernel folder. This is necessary if the PATH in the mk file is given in relative form 
  ; cspice_furnsh, '/Users/roussos/owncloud/Work/JUICE/SPICE/kernels/mk/juice_crema_5_0b23_1.tm' ;load a JUICE metakernel
  ; out=get_ephem_juice_generic(date=['2032-01-11T00:00:00.000','2032-01-15T00:00:00.000'], target='EUROPA', center='Jupiter', sys='JUICE_JUPITER_MAG_S3RH2009', unit=Rj, step=60d)

  ;
  ; MODIFICATION HISTORY:
  ; Written, ER, October, 2022.

IF (N_ELEMENTS(target) EQ 0) THEN target='JUICE' ELSE target=STRUPCASE(target)
IF (N_ELEMENTS(center) EQ 0) THEN center='JUPITER' ELSE center=STRUPCASE(center)
IF (N_ELEMENTS(sys) EQ 0) THEN sys='IAU_JUPITER' ELSE sys=STRUPCASE(sys)
IF (N_ELEMENTS(unit) EQ 0) THEN unit=1d
IF (N_ELEMENTS(icy) EQ 0) THEN icy='/Users/roussos/data/SPICE/icy/lib/icy.dlm'

IF keyword_set(init) THEN BEGIN
  
  IF (N_ELEMENTS(dir) EQ 0) THEN dir = '/Users/roussos/ownCloud/Work/JUICE/SPICE/kernels/' ;SPICE kernels directory

  dirmk = dir + 'mk/' ;metakernels directory for JUICE
  
  dlm_register, icy
  
  cd, dirmk ;change to metakernels directory

  metakernel=file_search('*.tm', count=mkcount) ;find all metakernels

  PRINT, 'SPICE metakernels' ;print metakernel names on screen
  PRINT, '-----------------------'
  FOR i=0l, mkcount-1l DO BEGIN

    PRINT, STRTRIM(STRING(i),2)+'. '+metakernel[i]

  ENDFOR

  READ, input_key, PROMPT='Select (0-'+STRTRIM(STRING(mkcount-1l),2)+'):' ;select metakernel

  cspice_furnsh, dirmk + metakernel[input_key] ;load selected metakernels for JUICE
ENDIF

;SPICE kernels should have been loaded independently

;Extract the starting and ending date

str1 = date[0]
str2 = date[1]

;Assing the time step (step) to 'dt' and make sure it is double value
;Value is in seconds
dt = step
dt=DOUBLE(dt)

cspice_str2et, str1, et1
cspice_str2et, str2, et2

N = 1l + long( (et2 - et1) / dt)
et_array = DINDGEN(N)*dt + et1

IF (et_array[N-1l] GT et2) THEN BEGIN
  
  et_array=et_array[0l:N-2l]
  N=N-1l
  
ENDIF

;Create empty arrays to store the data
ephem_array=DBLARR(N,6) ;LEMMS pointing (3 elements), moon position (3 elements), Cassini position(3 elements)

FOR i=0l,N-1l DO BEGIN

   et = et_array[i]   
                                ; Calculate position in a given frame:
                                ; CSPICE_SPKEZR, `targ`, _et_, `ref`,`abcorr`, `obs`, _STARG[6]_, _LTIME_

   cspice_spkezr, target, et, sys, 'none', center, pos_vel, ltime_ignored

   ;Store parameters in arrays

   ephem_array[i,*]=pos_vel[0:5]/unit

ENDFOR

RETURN, [[ephem_array], [et_array]]

END

