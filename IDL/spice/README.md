# IDL SPICE scripts

**Script 1 line descriptions**

- **get_ephem_juice_generic.pro**: Retrieve object position/velocity vs time in any available JUICE frame
- **juice_event_read.pro**: Read times of events provided by the JUICE mission timeline	
- **juice_mission_phase_read.pro**: Read time ranges of mission phases provided by the JUICE mission timeline
- **juice_orbit_read.pro**: Read the JUICE orbit number, as defined in the JUICE spice kernels
- **juice_segment_read.pro**: Read time ranges of JUICE segments, as provided by the JUICE segmentation
- **nim_ram_angles.pro**: Estimate the RAM angle of the NIM FoV directions in a Jupiter or moon RAM frame, defined in the JUICE kernels
- **pep_corotation.pro**: Estimate angle to corotation for different PEP sensors (JDC, JEI)
- **pep_pitch_angle.pro**: Estime pitch angle coverage for different PEP sensors JEI, JDC, JENI Ion mode, JoEE(+RADEM directionallity detector)
- **radem_pitch_angle.pro**: Estime pitch angle coverage for different RADEM sensors (directionallity electron detector and unidirectional detectors)
- **read_juice_ptr.pro**: Read timing of events defined in a JUICE PTR file
- **juice_find_wake.pro**: Find JUICE intervals that JUICE is crossing the wake of Europa, Ganymede or Callisto
- **juice_pitch_angle_generic.pro**: Calculate pitch angle between any arbitrary magnetic field direction and a vector available in a JUICE frame
- **spice_fov_interpolate.pro**: Draws the shape of a circular, rectangular or polygonal FoV with larger number of points than provided by SPICE getfov
- **spice_get_rectangular_corners.pro**: Obtain the 4 corners of a rectangle FoV, if boresight, reference/cross angles and reference vector are available

