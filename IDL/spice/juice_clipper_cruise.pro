PRO juice_clipper_cruise, $
    vsw=vsw, $
    tol=tol, $
    trange=trange,$
    step=step,$
    targets=targets,$
    condition=condition,$
    export=export,$
    dir=dir,$
    cdir=cdir,$
    out_dir=out_dir,$
    plotmake=plotmake,$
    ps=ps,$
    ouput=output,$
    mark_events=mark_events,$
    constraints=constraints
    
  ;+
  ; NAME:
  ; juice_clipper_cruise
  ;
  ; PURPOSE:
  ; Calculate interesting geometries between any two objects during the JUICE/CLIPPER cruise phase.
  ; Works with any object loaded from the JUICE/CLIPPER SPICE KERNELS
  ; 
  ;
  ; CATEGORY:
  ; JUICE trajectory scripts
  ;
  ; CALLING SEQUENCE:
  ; juice_clipper_cruise, [step=step], [trange=trange], [targets=targets], [dir=dir], [out_dir=out_dir], [condition=condition], [export=export], [plotmake=plotmake]$
  ;                       [ps=ps], [output=output]
  ;
  ; INPUTS:
  ;
  ; step: time step in seconds, default is 120 sec
  ; trange: time range (format example trange=['2032-01-11T05:44:04.000','2032-01-12T05:44:04.000'], time format accepted any UTC format that works for SPICE)
  ; dir: location of SPICE kernels directory. Default can be changed directly in the script
  ; cdir: location of the Europa Clipper trajectory file (bsp)
  ; out_dir: directory to store the output file. . Default can be changed directly in the script.
  ; targets: Two element string array, containing names of SPICE objects for calculating relative geometries beween them. Default (also if array has not two elements) is 
  ;          ['JUICE', 'EUROPA CLIPPER']. Any combination of SPICE objects can be used, e.g. EARTH, MARS, JUPITER, SATURN, MERCURY, VENUS etc. Input is case sensitive.
  ;          If you want to include SPICE objects like e.g. BEPI COLOMBO, you need to load the mission kernels by modifying the source code. 
  ; condition: One element strong array, indicating what kind of geometry we want to evaluate between the two targets. 
  ;            Options are 'radial' (targets in the same heliocentric distance), 'longitude' (targets in the same helio longitude), 'parker' (targets in the same Parker spiral)
  ;            If 'parker' is selected, one or more SW velocities should be defined through the vsw parameter
  ; vsw: Solar wind velocity in km/sec for evaluating Parker spiral alignements. It can be a scalar (e.g. vsw=400.0) or an array vsw=[300.,350.,400.,450.,500.,550.,600.]
  ; tol: Tolerance for evaluating alignments. If condition='radial', tolerance should be given in AU (means that the code will search for intervals that the two targets
  ;      are in the same radial distance +/-tol (default 0.25 AU). For 'longitude' and 'parker', tol is given in deg (default 5 deg)
  ; plotmake: make a plot of the output. Plots are made only if alignments are found. For the case of condition='parker', vsw should be at least 2-element array
  ; export: Export the results in an ASCII file
  ; output: filename of text of ps/pdf file, if plotmake=1, or export=1 are selected. If output not given, the code generates one automatically.
  ; ps: if plotmake=1, then save a plot in a ps/pdf file.
  ; mark_event: If there is a plotted output, mark some mission events (manually added for now in the source code)
  ; constraints: two element array, 1st element radial distance operational or other constraint for target[0], 2nd element applies for target[1]
  ;
  ; OUTPUTS:
  ;
  ; COMMON BLOCKS:
  ; None.
  ;
  ; SIDE EFFECTS:
  ; None.
  ;
  ; RESTRICTIONS:
  ; Requires JUICE/CLIPPER SPICE kernels,event files (if to be used)
  ; CLIPPER Trajectory temporarilly available at https://juigitlab.esac.esa.int/notebooks/juice-clipper/-/tree/main/data
  ; 
  ; PROCEDURE:
  ; Adjust input folders in the beginning of the script: out_dir, spice_dir

  ; Examples
  ; 1) Estimate the parker alignment between JUICE/CLIPPER (tolerance 5 deg), for 500 SW velocities between 300 and 800 km/s, plot the output with mission events marked, 
  ;    save it in ps/pdf and also export the alignment intervals in an ascii file 
  ;    
  ; juice_clipper_cruise, vsw=FINDGEN(500)+300.0, tol=5d, step=86400d, condition=['parker'], targets=['JUICE','EUROPA CLIPPER'], /plot, /ps,/mark_events,/export, constraints=[1.3,0.9]
  ; juice_clipper_cruise, tol=5d, condition=['longitude'], targets=['JUICE','EUROPA CLIPPER'], /plot, /ps,/mark_events,/export, constraints=[1.3,0.9]
  ; juice_clipper_cruise, tol=0.1d, step=86400d, condition=['radial'], targets=['JUICE','EUROPA CLIPPER'], /plot, /ps,/mark_events,/export, constraints=[1.3,0.9]
  ; juice_clipper_cruise, tol=20d, step=86400d, condition=['conjunction'], targets=['JUICE','EUROPA CLIPPER'], /plot, /ps,/mark_events,/export, constraints=[1.3,0.9]
  ;  
  ; Note the above if you reverse the order of targets, i.e. ['EUROPA CLIPPER','JUICE'] results are the same
  ;
  ; 2) Same as above for a single vsw value of 400 km/sec and for two random objects, e.g. 'JUICE', 'MECRURY'. The code will message that not plot is produced for 
  ;    condition='parker' if only one vsw value is given, however an ascii file with intervals will be produced. 
  ;
  ; juice_clipper_cruise, vsw=400.0, tol=5d, step=86400d, condition=['parker'], targets=['JUICE','MERCURY'], /plot, /ps,/mark_events,/export
  ;
  ; You can try the same with condition=['longitude'] or {condition=['radial'], tol=0.25}, then a plot will be generated (vsw input is not necessary then)
  ;
  ; 3) Same as (1), but define a manual location for the JUICE metakernels and the Clipper trajectory file 21F31_MEGA_L241010_A300411_LP01_V4_postLaunch_scpse.bsp
  ;
  ;    dir1='/Users/roussos/ownCloud/Work/JUICE/SPICE/kernels/'
  ;    dir2='/Users/roussos/owncloud/Work/Clipper/kernels_clipper/spk/'
  ;
  ; juice_clipper_cruise, vsw=FINDGEN(500)+300.0, tol=5d, step=86400d, condition=['parker'], targets=['JUICE','EUROPA CLIPPER'], /plot, /ps,/mark_events,/export, dir=dir1, cdir=dir2
    
  P0=!p ;Save default idl plot settings

  LOADCT, 39
  !p.thick = 2.5 ;make some modification of initial plot settings for this routine
  !x.thick = 2.5
  !y.thick = 2.5
  !z.thick = 2.5

  ;define needed folders
  IF (N_ELEMENTS(dir) EQ 0) THEN dir = '/Users/roussos/ownCloud/Work/JUICE/SPICE/kernels/' ;SPICE kernels directory
  IF (N_ELEMENTS(cdir) EQ 0) THEN cdir='/Users/roussos/owncloud/Work/Clipper/kernels_clipper/spk/'  ;SPICE kernels spk directory for Clipper

  dirmk = dir + 'mk/' ;metakernels directory for JUICE
  ;dirmkc = cdir + 'mk/' ;metakernels directory for Clipper

  IF (N_ELEMENTS(out_dir) EQ 0) THEN out_dir='/Users/roussos/'  ;output folder for plot files, text files etc
  
  IF keyword_set(ps) THEN BEGIN ;If postscript output is required, prepare the environment

    Set_Plot, 'ps'
    !p.font=0
    Device, Color=1, Bits_Per_pixel=8, /Landscape
    IF (N_ELEMENTS(output) EQ 0) THEN output=targets[0]+'_'+targets[1]+'_'+condition ;default name for plot file, if not given
    output_fixed=STRJOIN(STRSPLIT(output, /EXTRACT), '_')
    Device, Filename=out_dir+output_fixed+'.ps' ;change here the folder path to save plots

  ENDIF

  ;Define some default values
  IF (N_ELEMENTS(step) EQ 0) THEN step=3600d ;time step for ephemeris in seconds
  IF (N_ELEMENTS(trange) EQ 0) THEN trange=['2024 OCT 10 06:00:00.000','2031-09-01T00:00:00.000'] ;default time range, first is Clipper launch, 2nd is JUICE arrival at Jupiter
  IF (N_ELEMENTS(condition) EQ 0) THEN condition=['parker']
  IF (N_ELEMENTS(targets) EQ 0) THEN targets=['JUICE','EUROPA CLIPPER']
  IF (N_ELEMENTS(constraints) LT 2) THEN constraints=[0.0,0.0]

  Omega = 2*!DPI/(25.38d * 86400.0d) ;solar rotation
  sysval='ECLIPJ2000' ;coordinate system for trajectory evaluation
  au=149597870.700d ;1 AU in km
  rsun=696340d/au ;solar radius in AU
  
  IF (N_ELEMENTS(vsw) NE 0) THEN vsw=vsw/au ;convert input solar wind velocity in AU/sec
  
  cd, dirmk ;change to metakernels directory

  metakernel=file_search('*.tm', count=mkcount) ;find all metakernels

  PRINT, 'SPICE metakernels' ;print metakernel names on screen
  PRINT, '-----------------------'
  FOR i=0l, mkcount-1l DO BEGIN

    PRINT, STRTRIM(STRING(i),2)+'. '+metakernel[i]

  ENDFOR

  READ, input_key, PROMPT='Select (0-'+STRTRIM(STRING(mkcount-1l),2)+'):' ;select metakernel

  cspice_furnsh, dirmk + metakernel[input_key] ;load selected metakernels for JUICE
  cspice_furnsh, cdir + '21F31_MEGA_L241010_A300411_LP01_V4_postLaunch_scpse.bsp'; load trajectory for Clipper
  ;Now we have loaded enough kernel data for plotting trajectories of JUICE, Clipper and planets

  ;below get the Target 1 position in the desired coordinate system (sysval) in unit=1.0 km, stored
  ;in the xyzt_juice array
  ;xyzt_juice[0:2]--> x, y, z position
  ;xyzt_juice[6]--> ET time (seconds)

  xyzt0=get_ephem_juice_generic(date=trange, $
    step=step, sys=sysval, center='SUN', target=targets[0], unit=au) ;get trajectory of targets[0]

  et_time=xyzt0[*,6] ;assign a variable to the ET-seconds time for simplicity, these values will be the same for targets[1]
  cspice_et2utc, et_time, 'ISOC', 0, utc ;convert time stamps in calendar utc time, these will be the same for targets[1]
  
  cspice_et2utc, et_time, 'J', 6, julian_time_string; generate time in JD, useful for IDL plot labeling
  julian_time=DBLARR(N_ELEMENTS(julian_time_string))
  FOR i=0l,N_ELEMENTS(julian_time_string)-1l DO julian_time[i]=DOUBLE(STRSPLIT(julian_time_string[i],'JD ',/EXTRACT))  ;remove the "JD " part of the julian date string and convert to double

  events=[['EARTH_MOON_FB1','VENUS_FB','EARTH_FB2','EARTH_FB3'],$
          ['2024-08-20T22:10:03','2025-08-31T06:30:49','2026-09-28T11:52:20','2029-01-17T18:28:32']]
  
  events_et = DBLARR(N_ELEMENTS(events[*,0]))
  ;'MOON_FB':'2024-08-19T21:28:40'
  
  FOR i=0l, N_ELEMENTS(events[*,0])-1 DO BEGIN
    
    cspice_utc2et, events[i,1], events_et_temp
    events_et[i]=events_et_temp
    
  ENDFOR
  
  cspice_et2utc, events_et, 'J', 6, events_julian_time_string; generate time in JD, useful for IDL plot labeling
  events_julian_time=DBLARR(N_ELEMENTS(events_julian_time_string))
  FOR i=0l,N_ELEMENTS(events_julian_time_string)-1l DO events_julian_time[i]=DOUBLE(STRSPLIT(events_julian_time_string[i],'JD ',/EXTRACT))  ;remove the "JD " part of the julian date string and convert to double

  x0=xyzt0[*,0] ;x,y,z position in AU
  y0=xyzt0[*,1]
  z0=xyzt0[*,2]
  
  lon0=ATAN(y0,x0) ;helio longitude in rad
  tst=WHERE(lon0 LT 0)
  IF (tst[0] NE -1) THEN lon0[tst]=2*!DPI +lon0[tst] ;convert to 0-->2pi
  
  r0=SQRT(x0*x0 + y0*y0 + z0*z0) ;targets[0] radial distance in AU
  
  xyzt1=get_ephem_juice_generic(date=trange, $
    step=step, sys=sysval, center='SUN', target=targets[1], unit=au) ;same for targets[1]
  
  x1=xyzt1[*,0]
  y1=xyzt1[*,1]
  z1=xyzt1[*,2]
  
  lon1=ATAN(y1,x1)
  tst=WHERE(lon1 LT 0)
  IF (tst[0] NE -1) THEN lon1[tst]=2*!DPI +lon1[tst] ;convert to 0-->2pi
  
  r1=SQRT(x1*x1 + y1*y1 + z1*z1) ;radial distance, targets[1]
  
  CASE STRLOWCASE(condition) OF ;define unit for distance
    'radial': BEGIN
      IF (N_ELEMENTS(tol) EQ 0) THEN tol=0.25 ;0.25 au max radial distance difference between targets, if 'radial' condition selected
      tol=ABS(tol) ; make sure its a positive value
      diff_val = ABS(r0-r1) ; compare that actual longitude of targets[1] with the predicted one for longiutude alignment
      tst = WHERE(diff_val LE tol and r0 GE constraints[0] and r1 GE constraints[1]) ;find where this difference is within a tolerance (tol), bodies within a range of s/c distance from each other
    END
    'longitude': BEGIN
      IF (N_ELEMENTS(tol) EQ 0) THEN tol=5d ;5 deg longiutude max difference for heliolongitude alignment, if 'longitude' condition selected
      tol=tol*!dtor ;convert to radians
      tol=ABS(tol) ; make sure its a positive value
      diff_val = ABS(lon0-lon1) ; compare that actual longitude of targets[1] with the predicted one for longiutude alignment
      tst = where(diff_val GT 2*!DPI)
      IF (tst[0] NE -1) THEN diff_val[tst]=diff_val[tst]-2.0*!DPI
      tst = WHERE(diff_val LE tol OR diff_val GE 2d*!DPI - tol and r0 GE constraints[0] and r1 GE constraints[1]) ;find where this difference is within a tolerance (tol), i.e 0-->tol or 2*!PI-tol -->2*!PI (tol>0)
    END
    'parker': BEGIN
      IF (N_ELEMENTS(tol) EQ 0) THEN tol=5d ;5 deg longiutude max difference for Parker spiral alignment, if 'parker' condition selected
      tol=tol*!dtor ;convert to radians
      tol=ABS(tol) ; make sure its a positive value
      ;here the diff_val parameter is defined later, as we have to loop for the different vsw values provided
    END
    'conjunction': BEGIN
      IF (N_ELEMENTS(tol) EQ 0) THEN tol=20d ;20 deg angular difference
      tol=tol*!dtor ;convert to radians
      tol=ABS(tol) ; make sure its a positive value
    END
  ENDCASE
  
  valid_final = [] ;create an empty array to store final results later (time series with zero indicating no parker-alignment, 1 indicating parker alignment, for every vsw value)
  intervals_final = LIST() ;create an empty list to store results later, start-stop intervals in utc time, for every vsw value)
  intervals_et_final = LIST() ;create an empty list to store results later, start-stop intervals in et time, for every vsw value)
  intervals_jt_final = LIST() ;create an empty list to store results later, start-stop intervals in julian time, for every vsw value)
  
  IF (STRLOWCASE(condition) EQ 'conjunction') THEN BEGIN ;parker spiral alignment
    
    trgsep0=FLTARR(N_ELEMENTS(et_time))
    trgsep1=FLTARR(N_ELEMENTS(et_time))
    FOR i=0l, N_ELEMENTS(et_time)-1l DO trgsep0[i] = cspice_trgsep( et_time[i], targets[0], 'POINT', 'IAU_SUN', 'SUN',  'POINT', 'IAU_SUN', 'EARTH', 'none' )
    FOR i=0l, N_ELEMENTS(et_time)-1l DO trgsep1[i] = cspice_trgsep( et_time[i], targets[1], 'POINT', 'IAU_SUN', 'SUN',  'POINT', 'IAU_SUN', 'EARTH', 'none' )
    
    tst = where(trgsep0 LE tol and trgsep0 LE tol and r0 GE constraints[0] and r1 GE constraints[1])
    
    valid=LONARR(N_ELEMENTS(et_time)); create array with n_elememts as many time steps the trajectory has, to store times that the above condition is achieved
    ;this is a temporary array only

    IF (tst[0] NE -1) THEN valid[tst]=1 ;find valid time tags

    ;Now we start a procedure to find discrete intervals (start stop times) that the condition is satisfied. For this we have to run a process that loops
    ;through each et_time (time tag) value
    intervals = [] ;create an empty array in the for loop first step, to store results of intervals in utc time. This is a temporary parameter
    ;Its values will be copied into intervals_final
    intervals_et = [] ;create an empty array in the for loop first step, to store results of intervals in ET time. This is a temporary parameter
    ;Its values will be copied into intervals_et_final
    intervals_jt = [] ;create an empty array in the for loop first step, to store results of intervals in Julian time. This is a temporary parameter, only for vsw[k].
    ;Its values will be copied into intervals_et_final

    start_time = 0 ;test parameter to monitor in the loop below if a valid interval start has been found

    FOR i=0l, N_ELEMENTS(et_time)-1l DO BEGIN ;loop across all time tags for vsw[k]

      IF (valid[i] EQ 1) AND (start_time EQ 0) THEN BEGIN ;if for this time tag valid[i]=1 and start_time=0, this means we have detected a start of an valid interval

        start_time = utc[i] ;assign to start the utc time
        start_time_et = et_time[i] ;make also the corresponding param in ET-time
        start_time_jt = julian_time[i] ;make also the corresponding param in Julian-time

      ENDIF

      IF (valid[i] EQ 0) and (start_time NE 0) THEN BEGIN ;if while looping, we find a time that valid[i], but start_time=1, it means that we have exited the valid interval

        stop_time = utc[i-1] ;the stop time of the valid intervals is at the previous step
        stop_time_et = et_time[i-1] ;same in et_time
        stop_time_jt = julian_time[i-1] ;same in et_time
        intervals = [intervals,[[start_time], [stop_time]]] ;expand the utc intervals array created outside the loop
        intervals_et = [intervals_et,[[start_time_et], [stop_time_et]]] ;same for the ET intervals array
        intervals_jt = [intervals_jt,[[start_time_jt], [stop_time_jt]]] ;same for the ET intervals array
        start_time = 0

      ENDIF

    ENDFOR

    ; Add last interval if v equals 1 at end
    IF (start_time NE 0) THEN BEGIN

      intervals = [intervals, TRANSPOSE([start_time, utc[N_ELEMENTS(utc)-1l]])]
      intervals_et = [intervals_et, TRANSPOSE([start_time_et, et_time[N_ELEMENTS(et_time)-1l]])]
      intervals_jt = [intervals_jt, TRANSPOSE([start_time_jt, julian_time[N_ELEMENTS(julian_time)-1l]])]

    ENDIF

    intervals_final.Add, intervals ;now add everything in the intervals_final. Since this is a LIST, we simply use the ::Add procedure
    intervals_et_final.Add, intervals_et ;same as above, for intervals_et
    intervals_jt_final.Add, intervals_jt ;same as above, for intervals_et
    valid_final = [[valid_final], [valid]] ;add valid time series to the valid_final. This is an array, so expanding the array is commanded in a different way than above.

  ENDIF
  
  IF (STRLOWCASE(condition) EQ 'parker') THEN BEGIN ;parker spiral alignment
    
    FOR k=0l,N_ELEMENTS(vsw)-1l DO BEGIN ;for every provided value of SW velocity
      
      lon1_predict= lon0 + omega*(r0-r1)/vsw[k] ;predicted helio longitude that targets[1] has to be to be Parker-aligned with targets[0]
      
      tst=WHERE(lon1_predict LT 0) ;make sure this value is 0-->2pi
      IF (tst[0] NE -1) THEN lon1_predict[tst]=2*!DPI +lon1_predict[tst] ;convert to 0-->2pi
      
      diff_val = ABS(lon1-lon1_predict) ; compare that actual longitude of targets[1] with the predicted one for Parker alignment
      
      tst = where(diff_val GT 2*!DPI and r0 GE constraints[0] and r1 GE constraints[1])
      IF (tst[0] NE -1) THEN diff_val[tst]=diff_val[tst]-2.0*!DPI 
      
      tst = WHERE(diff_val LE tol OR diff_val GE 2d*!DPI - tol) ;find where this difference is within a tolerance (tol), i.e 0-->tol or 2*!PI-tol -->2*!PI (tol>0)
      
      valid=LONARR(N_ELEMENTS(et_time)); create array with n_elememts as many time steps the trajectory has, to store times that the above condition is achieved
                                       ;this is a temporary array only for the given value of vsw in the FOR loop
      
      IF (tst[0] NE -1) THEN valid[tst]=1 ;find valid time tags, change the value of the temp parameter valid into 1 for those timetags for the given vsw[k] in the loop
      
      ;Now we start a procedure to find discrete intervals (start stop times) that the condition is satisfied. For this we have to run a process that loops
      ;through each et_time (time tag) value, for the given vsw[k] in the current for loop
      
      intervals = [] ;create an empty array in the for loop first step, to store results of intervals in utc time. This is a temporary parameter, only for vsw[k]. 
                     ;Its values will be copied into intervals_final   
      intervals_et = [] ;create an empty array in the for loop first step, to store results of intervals in ET time. This is a temporary parameter, only for vsw[k].
                     ;Its values will be copied into intervals_et_final
      intervals_jt = [] ;create an empty array in the for loop first step, to store results of intervals in Julian time. This is a temporary parameter, only for vsw[k].
                     ;Its values will be copied into intervals_et_final

      start_time = 0 ;test parameter to monitor in the loop below if a valid interval start has been found
      
      FOR i=0l, N_ELEMENTS(et_time)-1l DO BEGIN ;loop across all time tags for vsw[k]
        
        IF (valid[i] EQ 1) AND (start_time EQ 0) THEN BEGIN ;if for this time tag valid[i]=1 and start_time=0, this means we have detected a start of an valid interval
         
          start_time = utc[i] ;assign to start the utc time
          start_time_et = et_time[i] ;make also the corresponding param in ET-time
          start_time_jt = julian_time[i] ;make also the corresponding param in Julian-time
          
        ENDIF
        
        IF (valid[i] EQ 0) and (start_time NE 0) THEN BEGIN ;if while looping, we find a time that valid[i], but start_time=1, it means that we have exited the valid interval
         
          stop_time = utc[i-1] ;the stop time of the valid intervals is at the previous step
          stop_time_et = et_time[i-1] ;same in et_time
          stop_time_jt = julian_time[i-1] ;same in et_time
          intervals = [intervals,[[start_time], [stop_time]]] ;expand the utc intervals array created outside the loop
          intervals_et = [intervals_et,[[start_time_et], [stop_time_et]]] ;same for the ET intervals array
          intervals_jt = [intervals_jt,[[start_time_jt], [stop_time_jt]]] ;same for the ET intervals array
          start_time = 0
        
        ENDIF
        
      ENDFOR
    
      ; Add last interval if v equals 1 at end
      IF (start_time NE 0) THEN BEGIN
        
        intervals = [intervals, TRANSPOSE([start_time, utc[N_ELEMENTS(utc)-1l]])]
        intervals_et = [intervals_et, TRANSPOSE([start_time_et, et_time[N_ELEMENTS(et_time)-1l]])]
        intervals_jt = [intervals_jt, TRANSPOSE([start_time_jt, julian_time[N_ELEMENTS(julian_time)-1l]])] 
        
      ENDIF
    
      intervals_final.Add, intervals ;now add everything in the intervals_final. Since this is a LIST, we simply use the ::Add procedure
      intervals_et_final.Add, intervals_et ;same as above, for intervals_et
      intervals_jt_final.Add, intervals_jt ;same as above, for intervals_et
      valid_final = [[valid_final], [valid]] ;add valid time series to the valid_final. This is an array, so expanding the array is commanded in a different way than above.
    
    ENDFOR
    
  ENDIF 
  
  IF (STRLOWCASE(condition) EQ 'longitude' or STRLOWCASE(condition) EQ 'radial') THEN BEGIN ;parker spiral alignment

    valid=LONARR(N_ELEMENTS(et_time)); create array with n_elememts as many time steps the trajectory has, to store times that the above condition is achieved
    ;this is a temporary array only

    IF (tst[0] NE -1) THEN valid[tst]=1 ;find valid time tags

    ;Now we start a procedure to find discrete intervals (start stop times) that the condition is satisfied. For this we have to run a process that loops
    ;through each et_time (time tag) value
    intervals = [] ;create an empty array in the for loop first step, to store results of intervals in utc time. This is a temporary parameter
    ;Its values will be copied into intervals_final
    intervals_et = [] ;create an empty array in the for loop first step, to store results of intervals in ET time. This is a temporary parameter
    ;Its values will be copied into intervals_et_final
    intervals_jt = [] ;create an empty array in the for loop first step, to store results of intervals in Julian time. This is a temporary parameter, only for vsw[k].
    ;Its values will be copied into intervals_et_final

    start_time = 0 ;test parameter to monitor in the loop below if a valid interval start has been found

    FOR i=0l, N_ELEMENTS(et_time)-1l DO BEGIN ;loop across all time tags for vsw[k]

      IF (valid[i] EQ 1) AND (start_time EQ 0) THEN BEGIN ;if for this time tag valid[i]=1 and start_time=0, this means we have detected a start of an valid interval

        start_time = utc[i] ;assign to start the utc time
        start_time_et = et_time[i] ;make also the corresponding param in ET-time
        start_time_jt = julian_time[i] ;make also the corresponding param in Julian-time

      ENDIF

      IF (valid[i] EQ 0) and (start_time NE 0) THEN BEGIN ;if while looping, we find a time that valid[i], but start_time=1, it means that we have exited the valid interval

          stop_time = utc[i-1] ;the stop time of the valid intervals is at the previous step
          stop_time_et = et_time[i-1] ;same in et_time
          stop_time_jt = julian_time[i-1] ;same in et_time
          intervals = [intervals,[[start_time], [stop_time]]] ;expand the utc intervals array created outside the loop
          intervals_et = [intervals_et,[[start_time_et], [stop_time_et]]] ;same for the ET intervals array
          intervals_jt = [intervals_jt,[[start_time_jt], [stop_time_jt]]] ;same for the ET intervals array
          start_time = 0

      ENDIF

    ENDFOR

    ; Add last interval if v equals 1 at end
    IF (start_time NE 0) THEN BEGIN

      intervals = [intervals, TRANSPOSE([start_time, utc[N_ELEMENTS(utc)-1l]])]
      intervals_et = [intervals_et, TRANSPOSE([start_time_et, et_time[N_ELEMENTS(et_time)-1l]])]
      intervals_jt = [intervals_jt, TRANSPOSE([start_time_jt, julian_time[N_ELEMENTS(julian_time)-1l]])] 

    ENDIF

    intervals_final.Add, intervals ;now add everything in the intervals_final. Since this is a LIST, we simply use the ::Add procedure
    intervals_et_final.Add, intervals_et ;same as above, for intervals_et
    intervals_jt_final.Add, intervals_jt ;same as above, for intervals_et
    valid_final = [[valid_final], [valid]] ;add valid time series to the valid_final. This is an array, so expanding the array is commanded in a different way than above.
    
  ENDIF
  
  IF keyword_set(plotmake) THEN BEGIN
    
    tlabels=LABEL_DATE(DATE_FORMAT=['%Y!C%M-%D']);format of plot labels for the time axis
    
    IF (STRLOWCASE(condition) EQ 'parker') THEN BEGIN
     
      IF (N_ELEMENTS(vsw) LT 2 OR N_ELEMENTS(intervals_final) EQ 0) THEN BEGIN 
        
        PRINT, 'Not enough Solar Wind velocity values for plotting or No alignments found to make a plot'
        
      ENDIF ELSE BEGIN 
      
        PLOT, julian_time, julian_time*0, /xstyle, yrange=minmax(vsw*au), /ystyle, XTICKUNITS = ['Time'], position=[0.1,0.55,0.9,0.95],$
          XTICKFORMAT='LABEL_DATE', ytitle='Solar Wind Velocity [km/sec]', chars=1.25,/nodata, ticklen=-0.02, $
          title='!C!C!C!C!C!C!C!C!CParker Spiral Alignment (tolerance: '+STRTRIM(STRING(tol*!radeg,FORMAT='(F4.2)'),2)+' deg)' 
        
        CONTOUR, valid_final, julian_time, vsw*au, /fill, levels=[0,1], c_thick=0, c_color=[255,125], /over
        
        IF keyword_set(mark_events) THEN BEGIN
  
          FOR i=0l, N_ELEMENTS(events[*,0])-1l DO BEGIN
  
            OPLOT, [events_julian_time[i], events_julian_time[i]], minmax(vsw*au), color=70, linestyle=1
            XYOUTS, events_julian_time[i], 1.025*max(vsw*au), events[i,0], /DATA, chars=0.45, orient=45.0, color=70
  
          ENDFOR
  
        ENDIF
        
        PLOT, julian_time, r0, /xstyle, yrange=[0.,6], /ystyle, XTICKUNITS = ['Time'], position=[0.1,0.1,0.9,0.425],$
          XTICKFORMAT='LABEL_DATE', ytitle='Heliocentric Distance [AU]', chars=1.25, ticklen=-0.02, thick=3, /noerase
          
        OPLOT, julian_time, r1, thick=3, color=254
        IF (total(constraints) GT 0) THEN OPLOT, julian_time, julian_time*0+constraints[0], linestyle=1
        IF (total(constraints) GT 0) THEN OPLOT, julian_time, julian_time*0+constraints[1], linestyle=1, color=254
        
        LEGEND, [targets[0], targets[1]], linestyle=0, thick=3, color=[0,254], /center, /right, chars=0.75
        IF (total(constraints) GT 0) THEN LEGEND, [targets[0]+' Ops. Limit', targets[1]+' Ops. Limit'], linestyle=1, thick=3, color=[0,254], /top, /left, chars=0.75,/vertical
        
        IF keyword_set(mark_events) THEN BEGIN 
          
          FOR i=0l, N_ELEMENTS(events[*,0])-1l DO OPLOT, [events_julian_time[i], events_julian_time[i]], [0,6], color=70, linestyle=1
          
        ENDIF
        
        sz1 = SIZE(intervals_et_final)
        
        earth=get_ephem_juice_generic(date=trange, $
          step=step, sys=sysval, center='SUN', target='EARTH', unit=au) ;get trajectory of targets[0]
        mars=get_ephem_juice_generic(date=trange, $
          step=step, sys=sysval, center='SUN', target='MARS', unit=au) ;get trajectory of targets[0]
        jupiter=get_ephem_juice_generic(date=trange, $
          step=step, sys=sysval, center='SUN', target='JUPITER', unit=au) ;get trajectory of targets[0]
        mercury=get_ephem_juice_generic(date=trange, $
          step=step, sys=sysval, center='SUN', target='MERCURY', unit=au) ;get trajectory of targets[0]
        venus=get_ephem_juice_generic(date=trange, $
          step=step, sys=sysval, center='SUN', target='VENUS', unit=au) ;get trajectory of targets[0]
        
        ang=FINDGEN(361)*!dtor
        
        FOR i=0l, sz1[1]-1l DO BEGIN
          
          temp=intervals_et_final[i]
          temp_str=intervals_final[i]
          sz2=sIZE(temp)
          
          FOR k=0, sz2[1]-1l DO BEGIN
            
            tst=WHERE(et_time EQ temp[k,0])
            plot, x0, y0, /iso, title=temp_str[k,0]+' to '+temp_str[k,1]+' V!DSW!N = '+ STRTRIM(STRING(vsw[i]*au),2)+ ' km/s', xtitle='X-ECLIPJ2000 [AU]', ytitle='Y-ECLIPJ2000 [AU]', xr=[-6,6], yr=[-6,6],/nodata
            oplot, earth[*,0], earth[*,1], color=60, thick=4
            oplot, mars[*,0], mars[*,1], color=230, thick=4
            oplot, jupiter[*,0], jupiter[*,1], color=200,thick=4
            oplot, mercury[*,0], mercury[*,1], color=20, thick=4
            oplot, venus[*,0], venus[*,1], color=170,thick=4
;            oplot, x0, y0, color=254, thick=0.5
;            oplot, x1, y1, color=254, thick=0.5
            
            polyfill, 0.1*cos(ang), 0.1*sin(ang), color=210
            polyfill, 0.1*cos(ang)+earth[tst[0],0], 0.1*sin(ang)+earth[tst[0],1], color=60
            polyfill, 0.1*cos(ang)+mars[tst[0],0], 0.1*sin(ang)+mars[tst[0],1], color=230
            polyfill, 0.1*cos(ang)+jupiter[tst[0],0], 0.1*sin(ang)+jupiter[tst[0],1], color=200
            polyfill, 0.1*cos(ang)+mercury[tst[0],0], 0.1*sin(ang)+mercury[tst[0],1], color=20
            polyfill, 0.1*cos(ang)+venus[tst[0],0], 0.1*sin(ang)+venus[tst[0],1], color=170
            
            plots, x0[tst[0]], y0[tst[0]], psym=4, syms=2
            plots, x1[tst[0]], y1[tst[0]], psym=4, color=254, syms=2
            
            rval=findgen(1000)/999d*6d
            lonss = lon1[tst[0]] + omega*(r1[tst[0]] - rval)/vsw[i]
            
            oplot, rval*cos(lonss), rval*sin(lonss), color=5, thick=3, linestyle=2
            
            LEGEND, [targets[0], targets[1]], psym=4, color=[0,254], /top,/right, chars=0.75

          ENDFOR
          
          
        ENDFOR

      ENDELSE 
      
    ENDIF 
    
    IF (STRLOWCASE(condition) EQ 'radial' OR STRLOWCASE(condition) EQ 'longitude' OR STRLOWCASE(condition) EQ 'conjunction' AND N_ELEMENTS(intervals_final) GT 0) THEN BEGIN
      
      IF (STRLOWCASE(condition) EQ 'radial') THEN tl='!C!C!C!C!C!C!C!C!CHeliocentric Distance Alignment (tolerance: '+STRTRIM(STRING(tol,FORMAT='(F4.2)'),2)+' AU)'
      IF (STRLOWCASE(condition) EQ 'longitude') THEN tl='!C!C!C!C!C!C!C!C!CLongitudinal Alignment (tolerance: '+STRTRIM(STRING(tol*!radeg,FORMAT='(F4.2)'),2)+' deg)'
      IF (STRLOWCASE(condition) EQ 'conjunction') THEN tl='!C!C!C!C!C!C!C!C!CSuperior Conjunction (tolerance: '+STRTRIM(STRING(tol*!radeg,FORMAT='(F6.2)'),2)+' deg)'

      IF (STRLOWCASE(condition) EQ 'conjunction') THEN BEGIN 
        
        pos1=[0.1,0.75,0.9,0.9]
        pos2=[0.1,0.425,0.9,0.65]
        
      ENDIF ELSE BEGIN 
        
        pos1=[0.1,0.65,0.9,0.9]
        pos2=[0.1,0.1,0.9,0.6]
        
      ENDELSE 
      
      PLOT, julian_time, julian_time*0, /xstyle, yrange=[0,1], ystyle=5, XTICKUNITS = ['Time'], position=pos1,$
        XTICKFORMAT='LABEL_DATE', ytitle='', chars=1.25,/nodata, ticklen=-0.02, title=tl, YTICK_GET=ytv
      
      AXIS, YAXIS=0,YTICKS=N_ELEMENTS(ytv)-1, ytickn=REPLICATE(' ', N_ELEMENTS(ytv)),/ystyle, ytickv=ytv, yrange=[0,1], TICKLEN=0
      AXIS, YAXIS=1,YTICKS=N_ELEMENTS(ytv)-1, ytickn=REPLICATE(' ', N_ELEMENTS(ytv)),/ystyle, ytickv=ytv, yrange=[0,1], TICKLEN=0      

      result = intervals_jt_final.ToArray(dim=1)
      sz=SIZE(result)
      nl=sz[1]
      
      FOR i=0l, nl-1 DO POLYFILL, [result[i,0], result[i,1],result[i,1],result[i,0]], [0,0,1,1], thick=20, color=125
      
      IF keyword_set(mark_events) THEN BEGIN

        FOR i=0l, N_ELEMENTS(events[*,0])-1l DO BEGIN

          OPLOT, [events_julian_time[i], events_julian_time[i]], [0,1], color=70, linestyle=1
          XYOUTS, events_julian_time[i], 1.025, events[i,0], /DATA, chars=0.45, orient=45.0, color=70

        ENDFOR

      ENDIF    

      PLOT, julian_time, r0, /xstyle, yrange=[0.,6], /ystyle, XTICKUNITS = ['Time'], position=pos2,$
        XTICKFORMAT='LABEL_DATE', ytitle='Heliocentric!CDistance [AU]', chars=1.25, ticklen=-0.02, thick=3, /noerase

      OPLOT, julian_time, r1, thick=3, color=254
      IF (total(constraints) GT 0) THEN OPLOT, julian_time, julian_time*0+constraints[0], linestyle=1
      IF (total(constraints) GT 0) THEN OPLOT, julian_time, julian_time*0+constraints[1], linestyle=1, color=254
      
      IF keyword_set(mark_events) THEN BEGIN

        FOR i=0l, N_ELEMENTS(events[*,0])-1l DO OPLOT, [events_julian_time[i], events_julian_time[i]], [0,6], color=70,linestyle=1

      ENDIF

      LEGEND, [targets[0], targets[1]], linestyle=0, thick=3, color=[0,254], /center, /right, chars=0.75
      IF (total(constraints) GT 0) THEN LEGEND, [targets[0]+' Ops. Limit', targets[1]+' Ops. Limit'], linestyle=1, thick=3, color=[0,254], /top, /left, chars=0.75,/vertical
      
      IF (STRLOWCASE(condition) EQ 'conjunction') THEN BEGIN 
        
        PLOT, julian_time, trgsep0*!radeg, /xstyle, yrange=[0,180], /ystyle, XTICKUNITS = ['Time'], position=[0.1,0.075,0.9,0.325],$
          XTICKFORMAT='LABEL_DATE', ytitle='SEP Angle [deg]', chars=1.25, ticklen=-0.02, thick=3, /noerase
        
        OPLOT, julian_time, trgsep1*!radeg, thick=3, color=254  
        
      ENDIF

    ENDIF ELSE IF (STRLOWCASE(condition) EQ 'radial' OR STRLOWCASE(condition) EQ 'longitude' AND N_ELEMENTS(intervals_final) GT 0) THEN BEGIN
    
      PRINT, 'No alignments found to make a plot'
      
    ENDIF 
  
  ENDIF 
  
  IF keyword_set(export) THEN BEGIN
    
    filename=out_dir+targets[0]+'_'+targets[1]+'_'+condition+'.dat' ;define filename
    fsearch=FILE_SEARCH(filename, count=count) ;search if the same file exists from before.
    IF (count EQ 1) THEN file_delete, filename ;make sure not to overwrite on previous file with the same filename & directory
    
    GET_LUN, lun
    
    OPENW, lun, filename
    PRINTF, lun, ';File generated (UTC): '+systime(/UTC)
    PRINTF, lun, ';Metakernels: '+metakernel[input_key], ', ','21F31_V4_Tour.tm'
    PRINTF, lun, ';Targets: '+targets[0], ', ',targets[1]
    PRINTF, lun, ';Time range: '+trange[0] + ' - ' +trange[1]
    PRINTF, lun, ';Time resolution: '+STRTRIM(STRING(step),2)+ ' sec.'
    PRINTF, lun, ';'
    
    IF (STRLOWCASE(condition) EQ 'parker') THEN BEGIN

      mm=STRTRIM(STRING(minmax(vsw*au)),2)
      PRINTF, lun, ';Alignment Condition: Parker Spiral
      PRINTF, lun, ';Tolerance [deg]: ', tol*!radeg
      PRINTF, lun, ';Solar wind velocity range [km/sec]: ', mm[0] + ' - ' +mm[1]
      PRINTF, lun, ';Operational Constraints [AU]: ', targets[0]+': '+STRTRIM(STRING(constraints[0]),2) + ' & ' + targets[0]+': '+ STRTRIM(STRING(constraints[1]),2)
      PRINTF, lun, ';'
      PRINTF, lun, ';'
      
      FOR i=0l, N_ELEMENTS(vsw)-1l DO BEGIN
        
        IF (N_ELEMENTS(intervals_final) NE 0) THEN BEGIN
            
            PRINTF, lun, ';--------------------------------------------------------------'
            PRINTF, lun, ';Solar Wind Velocity [km/sec]: ', STRTRIM(STRING((vsw[i]*au)),2)
            PRINTF, lun, ';--------------------------------------------------------------'
            PRINTF, lun, ';                                '
            PRINTF, lun, ';Start Date           Stop Date'
            PRINTF, lun, ';-------------------------------'
            PRINTF, lun, TRANSPOSE(intervals_final[i])
            PRINTF, lun, ';'
            PRINTF, lun, ';'
            
        ENDIF ELSE BEGIN

          PRINT, ';No valid Intervals where found'

        ENDELSE

      ENDFOR
      
    ENDIF
    
    IF (STRLOWCASE(condition) EQ 'longitude' OR STRLOWCASE(condition) EQ 'radial' OR STRLOWCASE(condition) EQ 'conjunction') THEN BEGIN
      
      CASE STRLOWCASE(condition) OF ;define unit for distance
        'radial': BEGIN
          PRINTF, lun, ';Alignment Condition: Heliospheric distance
          PRINTF, lun, ';Tolerance [AU]: ', tol
        END
        'longitude': BEGIN
          PRINTF, lun, ';Alignment Condition: Heliospheric Longitude (Ecliptic coordinates)
          PRINTF, lun, ';Tolerance [deg]: ', tol*!radeg
        END
        'conjunction': BEGIN
          PRINTF, lun, ';Alignment Condition: Superior Conjunction (Ecliptic coordinates)
          PRINTF, lun, ';Tolerance [deg]: ', tol*!radeg
        END
      ENDCASE
       
      PRINTF, lun, ';'
      PRINTF, lun, ';Operational Constraints [AU]: ', targets[0]+': '+STRTRIM(STRING(constraints[0]),2) + ' & ' + targets[0]+': '+ STRTRIM(STRING(constraints[1]),2)
      PRINTF, lun, ';'

      IF (N_ELEMENTS(intervals_final) NE 0) THEN BEGIN

        PRINTF, lun, ';Start Date           Stop Date'
        PRINTF, lun, ';-------------------------------'
        PRINTF, lun, TRANSPOSE(intervals_final[0])
        PRINTF, lun, ';'
        PRINTF, lun, ';'
        
      ENDIF ELSE BEGIN

        PRINT, ';No valid Intervals where found'

      ENDELSE

    ENDIF

    FREE_LUN, lun
    
  ENDIF
  
  cspice_kclear
  
  !P=p0; revert back to the original plot settings

  IF keyword_set(ps) THEN BEGIN

    Device, /Close_file  ; close postscript if my postscript plot calling method is used

    IF (strupcase(!version.os_family) EQ 'UNIX') THEN BEGIN ;If you want to convert to pdf and have ps2pdf installed (only works for unix)

      cd, out_dir
      spawn, 'ps2pdf '+output_fixed+'.ps'

    ENDIF

  ENDIF

  IF (strupcase(!version.os_family) EQ 'WINDOWS') THEN BEGIN

    Set_plot, 'Win'

  ENDIF ELSE IF (strupcase(!version.os_family) EQ 'UNIX') THEN BEGIN

    Set_plot, 'X'

  ENDIF

END