FUNCTION read_juice_ptr

ptr_folder='/Users/roussos/owncloud/mylib/juice_ptr/' ;folder to store ptr/xml files
filename=Dialog_Pickfile(path=ptr_folder);open window to choose file

data=read_xml(filename) ;read xm into data structure

tags=tag_names(data.prm.body.segment.data.timeline) ;get tag names in timeline structure

tag_type=STRMID(tags, 0, 5)

test_block=WHERE(tag_type EQ 'BLOCK')

tags=tags[test_block]

tstart=STRARR(N_ELEMENTS(tags))
tstop=STRARR(N_ELEMENTS(tags))
comment=STRARR(N_ELEMENTS(tags))

FOR i=0l, N_ELEMENTS(tags)-1l DO BEGIN
  
  dummy=execute('temp=data.prm.body.segment.data.timeline.'+tags[i]+'.ref')
  
  IF (temp EQ 'OBS')  THEN dummy=execute('tstart[i]=data.prm.body.segment.data.timeline.'+tags[i]+'.starttime._text') ELSE tstart[i]=''
  IF (temp EQ 'OBS')  THEN dummy=execute('tstop[i]=data.prm.body.segment.data.timeline.'+tags[i]+'.endtime._text') ELSE tstop[i]=''
  IF (temp EQ 'OBS')  THEN dummy=execute('comment[i]=data.prm.body.segment.data.timeline.'+tags[i]+'.metadata.comment._text') ELSE comment[i]=''

ENDFOR

novalue=WHERE(tstart NE '')
IF (novalue[0] NE -1) THEN BEGIN
  
  tstart=tstart[novalue]
  tstop=tstop[novalue]
  comment=comment[novalue]
  
ENDIF

arr=[[tstart],[tstop],[comment]]

RETURN, arr

END