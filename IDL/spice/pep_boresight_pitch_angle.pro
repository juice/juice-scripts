PRO pep_boresight_pitch_angle,$
    step=step,$ 
    trange=trange,$ 
    sensor=sensor,$ 
    radem=radem,$ 
    high=high,$ 
    center=center,$ 
    unit=unit,$
    ps=ps,$ 
    output=output,$
    export=export,$
    discrete=discrete, $
    mark_ptr=mark_ptr, $
    mark_event=mark_event,$
    event=event, $
    order=order, $
    dir=dir,$
    out_dir=out_dir,$
    noplot=noplot
    
  ;+
  ; NAME:
  ; pep_boresight_pitch_angle
  ;
  ; PURPOSE:
  ; Calculate PEP sensor pitch angle coverage
  ;
  ; CATEGORY:
  ; JUICE pointing scripts
  ;
  ; CALLING SEQUENCE:
  ; pep_boresight_pitch_angle, [step=step], [trange=trange], [sensor=sensor], [use_pixels=use_pixels], [radem=radem], [dir=dir], [noplot=noplot],$
  ;                  [high=high], [center=center], [unit=unit], [ps=ps], [output=output], [export=export], [erange=erange],[mark_ptr=mark_ptr], [mark_event=mark_event],[order=order] 
  ;
  ; INPUTS:
  ;
  ; step: time step in seconds, default is 120 sec
  ; trange: time range (format example trange=['2032-01-11T05:44:04.000','2032-01-12T05:44:04.000'], time format accepted any UTC format that works for SPICE)
  ; sensor: which PEP sensor (options: 'jdc','jei','jeni','joee', 'jeni_py','jeni_my': Note that 'jeni' combines JENI_PY & JENI_MY). Default is JOEE
  ; use_pixels: For JEI only (if selected, uses more detailed 128 pixel sectored FoV). Default use_pixels=0
  ; ca: Mark CA time, if a flyby is plotted e.g. ca='2032-07-02T16:22:11.000'. Default: CA undefined
  ; radem: If JoEE and /high are selected, overplot RADEM pitch angle coverage for the directionality detector. Default: radem=0
  ; high: For JoEE only, plots only the three highest energy (>300 keV) JoEE sectors. Default: high=0
  ; center: For 2nd plot panel (distance vs time), define the center from which distance is estimated (options: 'jupiter', 'europa', 'ganymede', 'callisto'). Default is Jupiter
  ; unit: For 2nd plot panel (distance vs time), define the distance unit (options: 'km', 'jupiter', 'europa', 'ganymede', 'callisto'). Default is Jupiter
  ; ps: export to postscript
  ; output: ps output filename
  ; export: export results in a savefile
  ; discrete: JEI only, estimate FoV if 16, 8 or 4 CEMs are used. Options '16', '8a', '8b', '4a', '4b', '4c', '4d' for the different CEM groups used. If defined, each segment is plotted separately
  ; erange: JEI only, define if to use the different FoV for low ('L'), medium ('M') or high ('H') energies. Default is 'L' 
  ; mark_ptr: If a PTR file is available (e.g. from JUICE pointing tool, read and mark PTR events). Selecting this will prompt the user to select a ptr file. 
  ; mark_event: Use SOC segmentation event files to mark events
  ; order: Jovian field expansion order, options:  'vit4', 'vip4_4', 'vipal_5', 'o6_3', 'isaac_10', 'jrm09_10', 'jrm33_13','jrm33_18'. Default: jrm09_10, invlalid input uses the default
  ; dir: location of SPICE kernels directory. Default can be changed directly in the script
  ; out_dir: directory to store the output file. . Default can be changed directly in the script.
  ; noplot: just calculate pitch angle information, no plotting involved
  ; 
  ;
  ; OUTPUTS:
  ; Depending on the keywords a two panel plot is shown on the screen with the PAD coverage of the selected sensor (top) and a distance/time plot on the bottom panel. 
  ; If ps is set, plot is saved in the location defined by out_dir, with filename defined through output keyword. ca, mark_ptr and mark_event will annotate the plot
  ; export will save some of the calculations in an IDL savefile (output folder, name defined by output keyword). 
  ; 
  ; COMMON BLOCKS:
  ; None.
  ;
  ; SIDE EFFECTS:
  ; None.
  ;
  ; RESTRICTIONS:
  ; Requires JUICE SPICE kernels, downloaded ptr, event files (if to be used). Does not yet include JNA. JDC FoV independent of energy. JENI pixels not implemented yet.
  ;
  ; PROCEDURE:
  ; Adjust input folders in the beginning of the script to the desired default values: out_dir, dir
  ; 
  ; Examples 
  ; 1) Europa flyby-1, JDC
  ; pep_pitch_angle, step=120d, sensor='JDC', /ps, output='JDC_PAD', ca='2032-07-02T16:22:25.000'
  ; 
  ; 2)Europa flyby-1, JoEE (<300 keV)
  ; pep_pitch_angle, step=120d, sensor='Joee', /ps, output='JoEE_PAD', ca='2032-07-02T16:22:25.000', trange=['2032-07-02T16:00:00.000','2032-07-03T04:00:00.000']
  ;
  ; 3)Europa flyby-1, JoEE (>300 keV) & RADEM directionality detector & distance from Europa in Europa radii (2nd panel)
  ; pep_pitch_angle, step=120d, sensor='Joee', /ps, output='JoEE_PAD_HIGH', ca='2032-07-02T16:22:25.000', /radem, /high, center='europa', unit='europa'
  ;
  ; 4)Extended time period, JoEE (>300 keV) & RADEM directionality detector
  ; pep_pitch_angle, step=600d, sensor='Joee', /ps, output='JoEE_PAD_HIGH_EXTENDED', trange=['2032-06-20T00:00:0.000','2032-07-15T00:00:0.000'], /radem, /high
  ;
  ; MODIFICATION HISTORY:
  ; Written, ER, October, 2022.

P0=!p ;Save default idl plot settings

LOADCT, 39
!p.thick = 2.5 ;make some modification of initial plot settings for this routine
!x.thick = 2.5
!y.thick = 2.5
!z.thick = 2.5

IF (N_ELEMENTS(dir) EQ 0) THEN dir='/Users/roussos/ownCloud/Work/JUICE/SPICE/kernels/' ;SPICE kernels directory
IF (N_ELEMENTS(out_dir) EQ 0) THEN out_dir='/Users/roussos/'

;some inputs. Adjust accordingly
dirmk = dir + 'mk/' ;metakernels directory
sysval='IAU_JUPITER' ; use this coordinate system for follow-up SPICE calculations since magnetic field models return mag vector in that

IF keyword_set(ps) THEN BEGIN ;If postscript output is required, prepare the environment

  Set_Plot, 'ps'
  !p.font=0
  Device, Color=1, Bits_Per_pixel=8, /Landscape
  IF (N_ELEMENTS(output) EQ 0) THEN output='pep_pitch' ;default name for plot file, if not given

  Device, Filename=out_dir+output+'.ps' ;change here the folder path to save plots

ENDIF

;Define some default values
IF (N_ELEMENTS(step) EQ 0) THEN step=120d ;time step for ephemeris in seconds
IF (N_ELEMENTS(trange) EQ 0) THEN trange=['2032-07-02T04:22:11.000','2032-07-03T04:22:11.000'] ; Europa flyby 1, CREMA 5.0
IF (N_ELEMENTS(sensor) EQ 0) THEN sensor='joee'
IF (STRLOWCASE(sensor) NE 'joee') THEN high=0;make sure to deactivate the /high keyword if JoEE is not the sensor
IF (STRLOWCASE(sensor) NE 'joee' AND STRLOWCASE(sensor) NE 'jdc' AND STRLOWCASE(sensor) NE 'jeni' $
  AND STRLOWCASE(sensor) NE 'jeni_py' AND STRLOWCASE(sensor) NE 'jeni_my' AND STRLOWCASE(sensor) NE 'jei') THEN sensor='joee' ;make sure to get a valid sensor definition. Default is joEE
IF (N_ELEMENTS(unit) EQ 0) THEN unit='jupiter' ;default distance unit is 1 Rj
IF (N_ELEMENTS(center) EQ 0) THEN center='jupiter' ;default coordinate system center is jupiter
IF (N_ELEMENTS(discrete) EQ 0) THEN discrete='' 
IF (N_ELEMENTS(use_pixels) EQ 0) THEN use_pixels=0
IF (N_ELEMENTS(order) EQ 0) THEN order='jrm09_10'
order=STRLOWCASE(order); make sure input of order is in lowercase

rj=71492.0 ;jupiter radius in km

PRINT, 'Pitch Angles will be calculated for PEP/'+STRTRIM(sensor,2)
PRINT, ''

cd, dirmk ;change to metakernels directory
metakernel=file_search('*.tm', count=mkcount) ;find all metakernels

PRINT, 'SPICE metakernels' ;print metakernel names on screen
PRINT, '-----------------------'
FOR i=0l, mkcount-1l DO PRINT, STRTRIM(STRING(i),2)+'. '+metakernel[i]


READ, input_key, PROMPT='Select (0-'+STRTRIM(STRING(mkcount-1l),2)+'):' ;select metakernel
cspice_furnsh, dirmk + metakernel[input_key] ;load selected metakernel

;;;;;GET THE POSITION;;;;;; 
;this is for calculations in the pitch angle panel
;below get the JUICE position in the desired coordinate system (sysval) in unit=1 Rj, stored
;in the xyzt array
;xyzt[0:2]--> x, y, z position
;xyzt[3:5]--> x, y, z velocity
;xyzt[6]--> ET time (seconds)

xyzt=get_ephem_juice_generic(date=trange, $
  step=step, sys=sysval, center='JUPITER', target='JUICE', unit=rj) ;unit=Rj so output in Rj or Rj/sec ; 

x=xyzt[*,0] ;x,y,z variables
y=xyzt[*,1]
z=xyzt[*,2]
r=SQRT(x*x + y*y + z*z) ;radial distance
  
et_time=xyzt[*,6] ;assign a variable to the ET-time for simplicity (same for both panels since they use the same trange & step)

;Select instrument
;Get boresight and boundary corner vectors to get a good sample of pointings covered
IF (STRLOWCASE(sensor) EQ 'joee') THEN BEGIN
  
  VAR='INS-28550_SECTOR_DIRECTIONS' ;JoEE boresights of all 9 pixels
  MAXBND=1000
  cspice_gdpool, VAR, 0, MAXBND, values, found
  val=REFORM(values, 3, 9) ;store boresight pointings in val
  
  IF keyword_set(high) THEN vall=val[*,[0,4,8]] ;only boresights of pixels 0, 4, 8 are relevant for >300 keV
  
  nvec=SIZE(val)
  nvec=nvec[2] ;number of pointing vectors
  
ENDIF ELSE IF (STRLOWCASE(sensor) EQ 'jdc') THEN BEGIN
  
  jdc_pixels=2851000l +LINDGEN(192) ;JDC boresights 192 pixels

  FOR i=0l, N_ELEMENTS(jdc_pixels)-1 DO BEGIN ;get pointings of boundaries of 192 JDC pixels

    CSPICE_GETFOV, -jdc_pixels[i], 6, shape, frame_in_which_fov_was_defined, bsight, bounds ;GET FoV params 
    IF (i EQ 0) THEN val=bsight ELSE val=[[val],[bsight]];

  ENDFOR

  nvec=SIZE(val)
  nvec=nvec[2] ;number of pointing vectors

ENDIF ELSE IF (STRLOWCASE(sensor) EQ 'jei') THEN BEGIN
    
  VAR='INS-28540_SECTOR_DIRECTIONS' ;JEI boresights, 128 pixels
  MAXBND=1000
  cspice_gdpool, VAR, 0, MAXBND, values, found
  val=REFORM(values, 3, 128)
  
  val00=val[*,[LINDGEN(8)*16]]
  val01=val[*,[LINDGEN(8)*16+1]]
  val02=val[*,[LINDGEN(8)*16+2]]
  val03=val[*,[LINDGEN(8)*16+3]]
  val04=val[*,[LINDGEN(8)*16+4]]
  val05=val[*,[LINDGEN(8)*16+5]]
  val06=val[*,[LINDGEN(8)*16+6]]
  val07=val[*,[LINDGEN(8)*16+7]]
  val08=val[*,[LINDGEN(8)*16+8]]
  val09=val[*,[LINDGEN(8)*16+9]]
  val10=val[*,[LINDGEN(8)*16+10]]
  val11=val[*,[LINDGEN(8)*16+11]]
  val12=val[*,[LINDGEN(8)*16+12]]
  val13=val[*,[LINDGEN(8)*16+13]]
  val14=val[*,[LINDGEN(8)*16+14]]
  val15=val[*,[LINDGEN(8)*16+15]]

  CASE STRLOWCASE(discrete) OF ;define center to estimate distance from

    '16': BEGIN
      val=[[val00],[val01],[val02],[val03],[val04],[val05],[val06],[val07],[val08],[val09],[val10],[val11],[val12],[val13],[val14],[val15]]
    END
    '8a': BEGIN
      val=[[val00],[val02],[val04],[val06],[val08],[val10],[val12],[val14]]
    END
    '8b': BEGIN
      val=[[val01],[val03],[val05],[val07],[val09],[val11],[val13],[val15]]
    END
    '4a': BEGIN
      val=[[val00],[val04],[val08],[val12]]
    END
    '4b': BEGIN
      val=[[val01],[val05],[val09],[val13]]
    END
    '4c': BEGIN
      val=[[val02],[val06],[val10],[val14]]
    END
    '4d': BEGIN
      val=[[val03],[val07],[val11],[val15]]
    END
    ELSE: val=val
  ENDCASE
  
  frame_in_which_fov_was_defined='JUICE_PEP_JEI' ;define this parameter, since the CSPICE_GETFOV used to obtained it is not called

  nvec=SIZE(val)
  nvec=nvec[2] ;number of pointing vectors

ENDIF ELSE IF (STRLOWCASE(sensor) EQ 'jeni' OR STRLOWCASE(sensor) EQ 'jeni_py' OR STRLOWCASE(sensor) EQ 'jeni_my') THEN BEGIN

    ;JUICE_PEP_JENI_PY
    CSPICE_GETFOV, -28561, 6, shape, frame_in_which_fov_was_defined, bsight, bounds ;GET FoV params JENI PY
    val1=[[bounds],[bsight]]
    ;JUICE_PEP_JENI_MY
    CSPICE_GETFOV, -28562, 6, shape, frame_in_which_fov_was_defined, bsight, bounds ;GET FoV params JENI MY
    val2=[[bounds],[bsight]]
    
    IF (STRLOWCASE(sensor) EQ 'jeni') THEN val=[[val1],[val2]] ;IF sensor is JENI (both units in ion mode) combine JENI_PY & MY
    IF (STRLOWCASE(sensor) EQ 'jeni_py') THEN val=val1 ;IF sensor in ion mode is only JENI_PY 
    IF (STRLOWCASE(sensor) EQ 'jeni_my') THEN val=val2 ;IF sensor in ion mode is only JENI_MY 

    nvec=SIZE(val)
    nvec=nvec[2] ;number of pointing vectors

ENDIF

;Begin the procedure to calculate and store pitch angles

CASE order OF
  'vit4': imodel = 'vit4_order04'
  'vip4_4': imodel  = 'vip4_order04'
  'vipal_5': imodel = 'vipal_order05'
  'o6_3': imodel = 'o6_order03'
  'isaac_10': imodel = 'isaac_order10'
  'jrm09_10': imodel = 'jrm09_order10'
  'jrm33_13': imodel = 'jrm33_order13'
  'jrm33_18': imodel = 'jrm33_order18'
  ELSE: BEGIN
    order =  'jrm09_10'
    imodel = 'jrm09_order10'
  END
ENDCASE
  
pitch_angle=FLTARR(N_ELEMENTS(et_time), nvec) ;Make arrays to store pitch angle

FOR k=0l, nvec-1l DO BEGIN ;loop over number of vectors

  FOR i=0l, N_ELEMENTS(et_time)-1l DO BEGIN ;start a loop for all times

    bsight=val[*,k] ;value of pointing vector k

    CSPICE_PXFORM, frame_in_which_fov_was_defined, sysval, et_time[i], rotate ;rotation matrix into the desired frame (sysval)
    CSPICE_MXV, rotate, bsight, bsight_final ;make the rotation in the desired frame-->bsight final

    ;get an estimate for the magnetic field (internal field model + CAN current sheet)
    ;output in IAU_JUPITER coordinates (cartesian)
    
    dummy= execute('Bmag=jovian_'+imodel+'_internal_xyz(x[i], y[i], z[i]) +con2020_model_xyz(''analytic'', x[i], y[i], z[i])')
    u_mag = TRANSPOSE(bmag/SQRT(TOTAL(bmag^2.0))); get the magnetic field unit vector

    pitch_angle[i,k] = 180.0-CSPICE_VSEP(u_mag,bsight_final) * !RADEG ;get the pitch angle. The subtraction from 180 is for converting from instrument to particle pitch angle

  ENDFOR

ENDFOR

!P=p0; revert back to the original plot settings

cd, dirmk
cspice_kclear ;unload spice kernels

IF keyword_set(ps) THEN BEGIN
  
  Device, /Close_file  ; close postscript if my postscript plot calling method is used
  IF (strupcase(!version.os_family) EQ 'UNIX') THEN BEGIN ;If you want to convert to pdf and have ps2pdf installed (only works for unix)
    
    cd, out_dir
    spawn, 'ps2pdf '+output+'.ps'
    
  ENDIF
  
ENDIF

IF (N_ELEMENTS(export) NE 0) THEN save, filename=out_dir+output+'.sav', et_time, pitch_angle, sensor, trange

IF (strupcase(!version.os_family) EQ 'WINDOWS') THEN BEGIN

  Set_plot, 'Win'

ENDIF ELSE IF (strupcase(!version.os_family) EQ 'UNIX') THEN BEGIN

  Set_plot, 'X'

ENDIF

END