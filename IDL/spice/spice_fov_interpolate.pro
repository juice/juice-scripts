FUNCTION spice_fov_interpolate, $
         fshape=fshape, $
         bounds_in=bounds_in,$
         npoints=npoints,$
         bsight=bsight

 ;+
 ; NAME:
 ; spice_fov_interpolate
 ;
 ; PURPOSE:
 ; Interpolate FoV between boundary corners. Works for circular and rectangle- or polygon-shaped FoVs
 ;
 ; CATEGORY:
 ; JUICE spice scripts
 ;
 ; CALLING SEQUENCE:
 ; spice_get_rectangular_corners, bsight=bsight, npoints=npoints, fshape=fshape, bounds_in=bounds_in
 ;
 ; INPUTS:
 ; bsight: 3-element array of the sector boresight. not necessary to be unit vector. relevant for the circle shaped FoVs
 ; bounds_in: FoV boundary corner definitions obtained by cspice_getfov or other method
 ; npoints: how many points to place between two corners (rectangle, polygon) or in how many segments to divide the circle FoV
 ; fshape: shape of FoV (can be 'rectangle', 'circle', 'polygon')
 
IF (N_ELEMENTS(npoints) EQ 0) THEN npoints=10.0        

IF (STRLOWCASE(fshape) EQ 'rectangle' or STRLOWCASE(fshape) EQ 'polygon') THEN BEGIN
  
  sz=SIZE(bounds_in)
  corners=sz[2]
  
  FOR k=0l,corners-2l DO BEGIN

    delta=bounds_in[*,k+1]-bounds_in[*,k]
    ds=delta/npoints

    FOR s=0l,npoints-1l DO BEGIN

      temp=s*ds + bounds_in[*,k]
      IF (s EQ 0 and k EQ 0) THEN bounds_int=temp ELSE bounds_int=[[bounds_int],[temp]]

    ENDFOR

  ENDFOR
  
ENDIF ELSE IF (STRLOWCASE(fshape) EQ 'circle') THEN BEGIN
  
  angle_int=FINDGEN(npoints)*360.0/(npoints-1.0)*!dtor

  bounds_int=FLTARR(npoints,3)

  FOR k=0l, N_ELEMENTS(angle_int)-1l DO BEGIN

    cspice_vrotv, bounds_in, bsight, angle_int[k], tempv
    bounds_int[k,*]=tempv

  ENDFOR
    
  bounds_int=TRANSPOSE(bounds_int)
  
ENDIF

RETURN, bounds_int

END