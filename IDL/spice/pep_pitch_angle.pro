PRO pep_pitch_angle,$
    step=step,$ 
    trange=trange,$ 
    sensor=sensor,$ 
    use_pixels=use_pixels,$ 
    ca=ca,$ 
    radem=radem,$ 
    high=high,$ 
    center=center,$ 
    unit=unit,$
    ps=ps,$ 
    output=output,$
    export=export,$
    erange=erange, $
    mark_ptr=mark_ptr, $
    mark_event=mark_event,$
    event=event, $
    order=order, $
    dir=dir,$
    out_dir=out_dir,$
    noplot=noplot,$
    noise_sector=noise_sector,$
    add_ganymede=add_ganymede,$
    sysval=sysval
    
  ;+
  ; NAME:
  ; pep_pitch_angle
  ;
  ; PURPOSE:
  ; Calculate PEP sensor pitch angle coverage
  ;
  ; CATEGORY:
  ; JUICE pointing scripts
  ;
  ; CALLING SEQUENCE:
  ; pep_pitch_angle, [step=step], [trange=trange], [sensor=sensor], [use_pixels=use_pixels], [radem=radem], [dir=dir], [noplot=noplot],$
  ;                  [high=high], [center=center], [unit=unit], [ps=ps], [output=output], [export=export], [erange=erange],[mark_ptr=mark_ptr], [mark_event=mark_event],[order=order] 
  ;
  ; INPUTS:
  ;
  ; step: time step in seconds, default is 120 sec
  ; trange: time range (format example trange=['2032-01-11T05:44:04.000','2032-01-12T05:44:04.000'], time format accepted any UTC format that works for SPICE)
  ; sensor: which PEP sensor (options: 'jdc','jei','jeni','joee', 'jeni_py','jeni_my': Note that 'jeni' combines JENI_PY & JENI_MY). Default is JOEE
  ; use_pixels: For JNA  (if selected, uses more detailed 11 pixel sectored FoV). For JEI indicate if to use CEM group '16', '8a', '8b', '4a','4b','4c','4d'
  ; ca: Mark CA time, if a flyby is plotted e.g. ca='2032-07-02T16:22:11.000'. Default: CA undefined
  ; radem: If JoEE is selected, overplot RADEM pitch angle coverage for the directionality detector. Default: radem=0
  ; high: For JoEE only, plots only the three highest energy (>300 keV) JoEE sectors. Default: high=0
  ; center: For 2nd plot panel (distance vs time), define the center from which distance is estimated (options: 'jupiter', 'europa', 'ganymede', 'callisto'). Default is Jupiter
  ; unit: For 2nd plot panel (distance vs time), define the distance unit (options: 'km', 'jupiter', 'europa', 'ganymede', 'callisto'). Default is Jupiter
  ; ps: export to postscript
  ; output: ps output filename
  ; export: export results in a savefile
  ; erange: JEI only, define if to use the different FoV for low ('L'), medium ('M') or high ('H') energies. Default is 'L' 
  ; mark_ptr: If a PTR file is available (e.g. from JUICE pointing tool, read and mark PTR events). Selecting this will prompt the user to select a ptr file. 
  ; mark_event: Use SOC segmentation event files to mark events
  ; order: Jovian field expansion order, options:  'vit4', 'vip4_4', 'vipal_5', 'o6_3', 'isaac_10', 'jrm09_10', 'jrm33_13','jrm33_18'. Default: jrm09_10, invlalid input uses the default
  ; dir: location of SPICE kernels directory. Default can be changed directly in the script
  ; out_dir: directory to store the output file. . Default can be changed directly in the script.
  ; noplot: just calculate pitch angle information, no plotting involved
  ; noise_sector: array of integers indicating which sensor sectors should be discarded as noisy. JDC & JEI --> 0 to 15, JNA (if use_pixels=1): 0-->10, JoEE: 0-->8, JENI-->0 to 1 (PY or MY)
  ; add_ganymede: adds simple Ganymede dipole field to the jovian
  ; sysval: two element array, indicating the coordinate systems for field vector calculations and for plotting the 2nd panel ephemeris. sysval=['IAU_JUPITER','IAU_JUPITER']. For now, sysval[0] is forced to 'IAU_JUPITER'
  ;
  ; OUTPUTS:
  ; Depending on the keywords a two panel plot is shown on the screen with the PAD coverage of the selected sensor (top) and a distance/time plot on the bottom panel. 
  ; If ps is set, plot is saved in the location defined by out_dir, with filename defined through output keyword. ca, mark_ptr and mark_event will annotate the plot
  ; export will save some of the calculations in an IDL savefile (output folder, name defined by output keyword). 
  ; 
  ; COMMON BLOCKS:
  ; None.
  ;
  ; SIDE EFFECTS:
  ; None.
  ;
  ; RESTRICTIONS:
  ; Requires JUICE SPICE kernels, downloaded ptr, event files (if to be used). Does not yet include JNA. JDC FoV independent of energy. JENI pixels not implemented yet.
  ;
  ; PROCEDURE:
  ; Adjust input folders in the beginning of the script to the desired default values: out_dir, dir
  ; 
  ; Examples 
  ; 1) Europa flyby-1, JDC
  ; pep_pitch_angle, step=120d, sensor='JDC', /ps, output='JDC_PAD', ca='2032-07-02T16:22:25.000'
  ; 
  ; 2)Europa flyby-1, JoEE (<300 keV)
  ; pep_pitch_angle, step=120d, sensor='Joee', /ps, output='JoEE_PAD', ca='2032-07-02T16:22:25.000', trange=['2032-07-02T16:00:00.000','2032-07-03T04:00:00.000']
  ;
  ; 3)Europa flyby-1, JoEE (>300 keV) & RADEM directionality detector & distance from Europa in Europa radii (2nd panel)
  ; pep_pitch_angle, step=120d, sensor='Joee', /ps, output='JoEE_PAD_HIGH', ca='2032-07-02T16:22:25.000', /radem, /high, center='europa', unit='europa'
  ;
  ; 4)Extended time period, JoEE (>300 keV) & RADEM directionality detector
  ; pep_pitch_angle, step=600d, sensor='Joee', /ps, output='JoEE_PAD_HIGH_EXTENDED', trange=['2032-06-20T00:00:0.000','2032-07-15T00:00:0.000'], /radem, /high
  ; 
  ; 5) Europa flyby-1, JEI, with noise in sectors 7, 9, 12
  ; pep_pitch_angle, step=120d, sensor='JEI', /ps, output='JEI_PAD', ca='2032-07-02T16:22:25.000', noise_sector=[7,9,12], trange=['2032-07-02T16:00:00.000','2032-07-03T04:00:00.000']
  ; 
  ; 6) Ganymede Orbit insertion, compare JoEE PAD coverage with and without Ganymede dipole field
  ; pep_pitch_angle, step=300d, sensor='JoEE', output='JoEE_PAD_with_Ganymede_Dipole', ca='2032-07-02T16:22:25.000', trange=['2034-12-15T16:00:00.000','2034-12-25T04:00:00.000'], /ps, /add_ganymede
  ; pep_pitch_angle, step=300d, sensor='JoEE', output='JoEE_PAD_no_Ganymede_Dipole', ca='2032-07-02T16:22:25.000', trange=['2034-12-15T16:00:00.000','2034-12-25T04:00:00.000'], /ps,
  ; 
  ; MODIFICATION HISTORY:
  ; Written, ER, October, 2022.
  ; Oct. 22 --> Apr. 2023: Functionallity updates
  ; May 2023: simplified code, changed location of magn. field calculation to significantly increase calculation speed, added noise option, sector-divided sensor calculations added
  ;           obsolete keywords removed (discrete), added JNA, use_pixels option applies only for JNA and JEI. 

P0=!p ;Save default idl plot settings

LOADCT, 39
!p.thick = 2.5 ;make some modification of initial plot settings for this routine
!x.thick = 2.5
!y.thick = 2.5
!z.thick = 2.5

IF (N_ELEMENTS(dir) EQ 0) THEN dir='/Users/roussos/ownCloud/Work/JUICE/SPICE/kernels/' ;SPICE kernels directory
IF (N_ELEMENTS(out_dir) EQ 0) THEN out_dir='/Users/roussos/'

;some inputs. Adjust accordingly
dirmk = dir + 'mk/' ;metakernels directory
IF (N_ELEMENTS(sysval) LT 2) THEN sysval=['IAU_JUPITER','IAU_JUPITER'] ; use this coordinate system for follow-up SPICE calculations since magnetic field models return mag vector in that
sysval[0]='IAU_JUPITER'
IF keyword_set(ps) THEN BEGIN ;If postscript output is required, prepare the environment

  Set_Plot, 'ps'
  !p.font=0
  Device, Color=1, Bits_Per_pixel=8, /Landscape
  IF (N_ELEMENTS(output) EQ 0) THEN output='pep_pitch' ;default name for plot file, if not given

  Device, Filename=out_dir+output+'.ps' ;change here the folder path to save plots

ENDIF

;Define some default values
IF (N_ELEMENTS(step) EQ 0) THEN step=120d ;time step for ephemeris in seconds
IF (N_ELEMENTS(trange) EQ 0) THEN trange=['2032-07-02T04:22:11.000','2032-07-03T04:22:11.000'] ; Europa flyby 1, CREMA 5.0
IF (N_ELEMENTS(sensor) EQ 0) THEN sensor='joee'
IF (STRLOWCASE(sensor) NE 'joee') THEN high=0;make sure to deactivate the /high keyword if JoEE is not the sensor
IF (STRLOWCASE(sensor) NE 'joee' AND STRLOWCASE(sensor) NE 'jdc' AND STRLOWCASE(sensor) NE 'jna' AND STRLOWCASE(sensor) NE 'jeni' $
  AND STRLOWCASE(sensor) NE 'jeni_py' AND STRLOWCASE(sensor) NE 'jeni_my' AND STRLOWCASE(sensor) NE 'jei') THEN sensor='joee' ;make sure to get a valid sensor definition. Default is joEE
IF (N_ELEMENTS(unit) EQ 0) THEN unit='jupiter' ;default distance unit is 1 Rj
IF (N_ELEMENTS(center) EQ 0) THEN center='jupiter' ;default coordinate system center is jupiter
IF (N_ELEMENTS(order) EQ 0) THEN order='jrm09_10'

IF keyword_set(noise) THEN noise=!Values.F_NaN

order=STRLOWCASE(order); make sure input of order is in lowercase

rj=71492.0 ;jupiter radius in km

PRINT, 'Pitch Angles will be calculated for PEP/'+STRTRIM(sensor,2)
PRINT, ''

cd, dirmk ;change to metakernels directory
metakernel=file_search('*.tm', count=mkcount) ;find all metakernels

PRINT, 'SPICE metakernels' ;print metakernel names on screen
PRINT, '-----------------------'
FOR i=0l, mkcount-1l DO PRINT, STRTRIM(STRING(i),2)+'. '+metakernel[i]


READ, input_key, PROMPT='Select (0-'+STRTRIM(STRING(mkcount-1l),2)+'):' ;select metakernel
cspice_furnsh, dirmk + metakernel[input_key] ;load selected metakernel

;;;;;GET THE POSITION;;;;;; 
;this is for calculations in the pitch angle panel
;below get the JUICE position in the desired coordinate system (sysval) in unit=1 Rj, stored
;in the xyzt array
;xyzt[0:2]--> x, y, z position
;xyzt[3:5]--> x, y, z velocity
;xyzt[6]--> ET time (seconds)

xyzt=get_ephem_juice_generic(date=trange, $
  step=step, sys=sysval[0], center='JUPITER', target='JUICE', unit=rj) ;unit=Rj so output in Rj or Rj/sec ; 

x=xyzt[*,0] ;x,y,z variables
y=xyzt[*,1]
z=xyzt[*,2]
r=SQRT(x*x + y*y + z*z) ;radial distance
colat=ACOS(z/r) ;this is in the 0-->pi range, as required by SPICE
s3lone=ATAN(y,x) ;s3 longitude, east
test=WHERE(s3lone LT 0)
IF (test[0] NE -1) THEN s3lone[test]=2*!PI +s3lone[test] ;convert to 0-->2pi

;;;;;GET THE POSITION;;;;;;
;this is for calculations in the bottom (distance) panel
;below get the JUICE position in the desired coordinate system (sysval) in unit=unit, stored
;in the xyzt_2nd array
;xyzt_2nd[0:2]--> x, y, z position
;xyzt_2nd[3:5]--> x, y, z velocity
;xyzt_2nd[6]--> ET time (seconds)  
  
CASE STRLOWCASE(unit) OF ;define unit for distance
  
  'km': BEGIN 
          unit=1.0d
          ytpart1=' [km]'
        END
  'jupiter': BEGIN 
              unit=rj
              ytpart1=' [R!DJ!N]'
             END
  'europa': BEGIN 
              unit=1560.8d
              ytpart1=' [R!DE!N]'
            END
  'ganymede': BEGIN
                unit=2634.1d
                ytpart1=' [R!DG!N]'
              END
  'callisto': BEGIN 
                unit=2410.3d
                ytpart1=' [R!DC!N]'
              END
ENDCASE

CASE STRLOWCASE(center) OF ;define center to estimate distance from
  
  'jupiter': BEGIN
              ytpart2='Jupiter Distance'
            END
  'europa': BEGIN
              ytpart2='Europa Distance'
            END
  'ganymede': BEGIN
                ytpart2='Ganymede Distance'
              END
  'callisto': BEGIN
                ytpart2='Callisto Distance'
              END
ENDCASE

yt=ytpart2+ytpart1 ;y-label for second panel
  
xyzt_2nd=get_ephem_juice_generic(date=trange, $
    step=step, sys=sysval[1], center=center, target='JUICE', unit=unit) ;unit=unit so output in "unit" or "unit"/sec
    
x2=xyzt_2nd[*,0] ;x2,y2,z2 variables
y2=xyzt_2nd[*,1]
z2=xyzt_2nd[*,2]
r2=SQRT(x2*x2 + y2*y2 + z2*z2) ;radial distance

et_time=xyzt[*,6] ;assign a variable to the ET-time for simplicity (same for both panels since they use the same trange & step)

;Select instrument
;Get boresight and boundary corner vectors to get a good sample of pointings covered
IF (STRLOWCASE(sensor) EQ 'joee') THEN BEGIN
  
  VAR='INS-28550_SECTOR_DIRECTIONS' ;JoEE boresights of all 9 pixels
  MAXBND=1000
  cspice_gdpool, VAR, 0, MAXBND, values, found
  val=REFORM(values, 3, 9) ;store boresight pointings in val
  
  crsang=12.0*!dtor ;cross angle
  refang=11.25*!dtor ;reference angle
  refvec1=[0.0,1.0,0.0] ;reference vector 1
  refvec2=[1.0,0.0,0.0] ;reference vector 2 (use if refvec1 is parallel or antiparallel to boresight)
  shape='RECTANGLE'
  frame_in_which_fov_was_defined='JUICE_PEP_JOEE'
  
  FOR i=0l, 8 DO BEGIN ;construct joee pixel corners, get corner vectors of each pixel

    IF (i EQ 0 or i EQ 8) THEN refvec=refvec2 ELSE refvec=refvec1
    temp=spice_get_rectangular_corners(bsight=val[*,i], refang=refang, crsang=crsang, refvec=refvec)
    bounds=[[temp],[temp[*,0]]]
    
    bounds_int=spice_fov_interpolate(bounds_in=bounds, npoints=3.0, fshape=shape)
    
    IF (i EQ 0) THEN val2=bounds_int ELSE val2=[[[val2]],[[bounds_int]]] ;corners stored in val2
    
  ENDFOR
  
  IF (N_ELEMENTS(noise_sector) NE 0) THEN val2[*,*,noise_sector]=!Values.F_NaN

  IF keyword_set(high) THEN val=val2[*,*,[0,4,8]] ELSE val=val2

  nv=SIZE(val)
  nvec=nv[2] ;number of pointing vectors
  nbs=nv[3] ;number of boresights

  IF keyword_set(radem) THEN BEGIN ;IF /radem is selected, estimate the RADEM FoV
    
    dd_pixels=2892400l +LINDGEN(28) ;directionality detector

    FOR i=0l, N_ELEMENTS(dd_pixels)-1 DO BEGIN ;get pointings, store in valradem

      CSPICE_GETFOV, -dd_pixels[i], 6, shape, frame_in_which_fov_was_defined_radem, bsightradem, radembounds ;GET FoV params
      bounds_int=spice_fov_interpolate(bounds_in=radembounds, npoints=10.0, fshape=shape, bsight=bsightradem)
      
      IF (i EQ 0) THEN valradem=bounds_int ELSE valradem=[[[valradem]],[[bounds_int]]];
      
    ENDFOR

    nvradem=SIZE(valradem)
    nvecradem=nvradem[2] ;number of pointing vectors
    nbsradem=nvradem[3] ;number of boresights
    
  ENDIF
  
ENDIF ELSE IF (STRLOWCASE(sensor) EQ 'jdc') THEN BEGIN
  
  jdc_pixels=2851000l +LINDGEN(192) ;JDC boresights 192 pixels

  FOR i=0l, N_ELEMENTS(jdc_pixels)-1 DO BEGIN ;get pointings of boundaries of 192 JDC pixels

    CSPICE_GETFOV, -jdc_pixels[i], 6, shape, frame_in_which_fov_was_defined, bsight, bounds ;GET FoV params
    bounds=[[bounds],[bounds[*,0]]] 
    IF (i EQ 0) THEN val=bounds ELSE val=[[val],[bounds]];

  ENDFOR
  
  val=reform(val,[3,12*5,16])
  
  nv=SIZE(val)
  nvec=nv[2] ;number of pointing vectors
  nbs=nv[3]
  
  IF (N_ELEMENTS(noise_sector) NE 0) THEN val[*,*,noise_sector]=!Values.F_NaN
  
ENDIF ELSE IF (STRLOWCASE(sensor) EQ 'jei') THEN BEGIN
  
  IF (N_ELEMENTS(use_pixels) EQ 0) THEN use_pixels='16'
  IF (N_ELEMENTS(erange) EQ 0) THEN erange='L'
  
  CASE STRLOWCASE(use_pixels) OF ;define center to estimate distance from

    '16': BEGIN
      jei_pixels_name=['JUICE_PEP_JEI_00_'+erange, $
                      'JUICE_PEP_JEI_01_'+erange, $
                      'JUICE_PEP_JEI_02_'+erange, $
                      'JUICE_PEP_JEI_03_'+erange, $
                      'JUICE_PEP_JEI_04_'+erange, $
                      'JUICE_PEP_JEI_05_'+erange, $
                      'JUICE_PEP_JEI_06_'+erange, $
                      'JUICE_PEP_JEI_07_'+erange, $
                      'JUICE_PEP_JEI_08_'+erange, $
                      'JUICE_PEP_JEI_09_'+erange, $
                      'JUICE_PEP_JEI_10_'+erange, $
                      'JUICE_PEP_JEI_11_'+erange, $
                      'JUICE_PEP_JEI_12_'+erange, $
                      'JUICE_PEP_JEI_13_'+erange, $
                      'JUICE_PEP_JEI_14_'+erange, $
                      'JUICE_PEP_JEI_15_'+erange]
    END
    '8a': BEGIN
      jei_pixels_name=['JUICE_PEP_JEI_00_'+erange, $
                      'JUICE_PEP_JEI_02_'+erange, $
                      'JUICE_PEP_JEI_04_'+erange, $
                      'JUICE_PEP_JEI_06_'+erange, $
                      'JUICE_PEP_JEI_08_'+erange, $
                      'JUICE_PEP_JEI_10_'+erange, $
                      'JUICE_PEP_JEI_12_'+erange, $
                      'JUICE_PEP_JEI_14_'+erange]
      
      IF (N_ELEMENTS(noise_sector) NE 0) THEN BEGIN

        sector_array=[0,2,4,6,8,10,12,14]
        FOR ns=0l, N_ELEMENTS(noise_sector)-1l DO BEGIN
          
          IF (ns EQ 0) THEN tmp=[]
          tst=WHERE(noise_sector[ns] EQ sector_array)
          IF (tst[0] NE -1) THEN tmp=[tmp, tst]
                  
        ENDFOR
        
        noise_sector=tmp 
         
      ENDIF
      
    END
    '8b': BEGIN
      jei_pixels_name=['JUICE_PEP_JEI_01_'+erange, $
                      'JUICE_PEP_JEI_03_'+erange, $
                      'JUICE_PEP_JEI_05_'+erange, $
                      'JUICE_PEP_JEI_07_'+erange, $
                      'JUICE_PEP_JEI_09_'+erange, $
                      'JUICE_PEP_JEI_11_'+erange, $
                      'JUICE_PEP_JEI_13_'+erange, $
                      'JUICE_PEP_JEI_15_'+erange]
                      
      IF (N_ELEMENTS(noise_sector) NE 0) THEN BEGIN

        sector_array=[1,3,5,7,9,11,13,15]
        FOR ns=0l, N_ELEMENTS(noise_sector)-1l DO BEGIN
          
          IF (ns EQ 0) THEN tmp=[]
          tst=WHERE(noise_sector[ns] EQ sector_array)
          IF (tst[0] NE -1) THEN tmp=[tmp, tst]
                  
        ENDFOR
        
        noise_sector=tmp 
         
      ENDIF

    END
    '4a': BEGIN
      jei_pixels_name=['JUICE_PEP_JEI_00_'+erange, $
                      'JUICE_PEP_JEI_04_'+erange, $
                      'JUICE_PEP_JEI_08_'+erange, $
                      'JUICE_PEP_JEI_12_'+erange]
                      
      IF (N_ELEMENTS(noise_sector) NE 0) THEN BEGIN

        sector_array=[0,4,8,12]
        FOR ns=0l, N_ELEMENTS(noise_sector)-1l DO BEGIN
          
          IF (ns EQ 0) THEN tmp=[]
          tst=WHERE(noise_sector[ns] EQ sector_array)
          IF (tst[0] NE -1) THEN tmp=[tmp, tst]
                  
        ENDFOR
        
        noise_sector=tmp 
         
      ENDIF
      
    END
    '4b': BEGIN
      jei_pixels_name=['JUICE_PEP_JEI_01_'+erange, $
                      'JUICE_PEP_JEI_05_'+erange, $
                      'JUICE_PEP_JEI_09_'+erange, $
                      'JUICE_PEP_JEI_13_'+erange]
                      
      IF (N_ELEMENTS(noise_sector) NE 0) THEN BEGIN

        sector_array=[1,5,9,13]
        FOR ns=0l, N_ELEMENTS(noise_sector)-1l DO BEGIN
          
          IF (ns EQ 0) THEN tmp=[]
          tst=WHERE(noise_sector[ns] EQ sector_array)
          IF (tst[0] NE -1) THEN tmp=[tmp, tst]
                  
        ENDFOR
        
        noise_sector=tmp 
         
      ENDIF
                    
    END
    '4c': BEGIN
      jei_pixels_name=['JUICE_PEP_JEI_02_'+erange, $
                      'JUICE_PEP_JEI_06_'+erange, $
                      'JUICE_PEP_JEI_10_'+erange, $
                      'JUICE_PEP_JEI_14_'+erange]
                      
      IF (N_ELEMENTS(noise_sector) NE 0) THEN BEGIN

        sector_array=[2,6,10,14]
        FOR ns=0l, N_ELEMENTS(noise_sector)-1l DO BEGIN
          
          IF (ns EQ 0) THEN tmp=[]
          tst=WHERE(noise_sector[ns] EQ sector_array)
          IF (tst[0] NE -1) THEN tmp=[tmp, tst]
                  
        ENDFOR
        
        noise_sector=tmp 
         
      ENDIF                     
           
    END
    '4d': BEGIN
      jei_pixels_name=['JUICE_PEP_JEI_03_'+erange, $
                      'JUICE_PEP_JEI_07_'+erange, $
                      'JUICE_PEP_JEI_11_'+erange, $
                      'JUICE_PEP_JEI_15_'+erange]
   
      IF (N_ELEMENTS(noise_sector) NE 0) THEN BEGIN

        sector_array=[3,7,11,15]
        FOR ns=0l, N_ELEMENTS(noise_sector)-1l DO BEGIN
          
          IF (ns EQ 0) THEN tmp=[]
          tst=WHERE(noise_sector[ns] EQ sector_array)
          IF (tst[0] NE -1) THEN tmp=[tmp, tst]
                  
        ENDFOR
        
        noise_sector=tmp 
         
      ENDIF                       
                      
    END      
    
  ENDCASE
  
  FOR i=0l, N_ELEMENTS(jei_pixels_name)-1 DO BEGIN ;get pointings of boundaries of 48 JEI pixels

    cspice_bods2c, jei_pixels_name[i], code, found
    CSPICE_GETFOV, code, 6, shape, frame_in_which_fov_was_defined, bsight, bounds ;GET FoV params
    bounds=[[bounds],[bounds[*,0]]]
    bounds_int=spice_fov_interpolate(bounds_in=bounds, npoints=10.0, fshape=shape)
    IF (i EQ 0) THEN val=bounds_int ELSE val=[[[val]],[[bounds_int]]]

  ENDFOR  
  
  IF (N_ELEMENTS(noise_sector) NE 0) THEN val[*,*,noise_sector]=!Values.F_NaN

  nv=SIZE(val)
  nvec=nv[2] ;number of pointing vectors
  nbs=nv[3] ;number of boresights

ENDIF ELSE IF (STRLOWCASE(sensor) EQ 'jeni' OR STRLOWCASE(sensor) EQ 'jeni_py' OR STRLOWCASE(sensor) EQ 'jeni_my') THEN BEGIN

    ;JUICE_PEP_JENI_PY
    CSPICE_GETFOV, -28561, 6, shape, frame_in_which_fov_was_defined, bsight, bounds ;GET FoV params JENI PY
    bounds=[[bounds],[bounds[*,0]]]
    bounds_int=spice_fov_interpolate(bounds_in=bounds, npoints=10.0, fshape=shape)
    val1=bounds_int
    nv1=SIZE(val1)
    
    ;JUICE_PEP_JENI_MY
    CSPICE_GETFOV, -28562, 6, shape, frame_in_which_fov_was_defined, bsight, bounds ;GET FoV params JENI MY
    bounds=[[bounds],[bounds[*,0]]]
    bounds_int=spice_fov_interpolate(bounds_in=bounds, npoints=10.0, fshape=shape)
    val2=bounds_int
    nv2=SIZE(val2)
    
    IF (STRLOWCASE(sensor) EQ 'jeni') THEN val=[[[val1]],[[val2]]] ;IF sensor is JENI (both units in ion mode) combine JENI_PY & MY
    IF (STRLOWCASE(sensor) EQ 'jeni_py') THEN val=REFORM(val1,[3,nv1[2],1]) ;IF sensor in ion mode is only JENI_PY 
    IF (STRLOWCASE(sensor) EQ 'jeni_my') THEN val=REFORM(val2,[3,nv2[2],1]) ;IF sensor in ion mode is only JENI_MY 
    
    IF (N_ELEMENTS(noise_sector) NE 0) THEN val[*,*,noise_sector]=!Values.F_NaN
    
    nv=SIZE(val)
    nvec=nv[2] ;number of pointing vectors
    nbs=nv[3] ;number of boresights

ENDIF ELSE IF (STRLOWCASE(sensor) EQ 'jna') THEN BEGIN
  
  IF keyword_set(use_pixels) THEN BEGIN
  
    jna_pixels=285201l +LINDGEN(11) ;directionality detector
  
    FOR i=0l, N_ELEMENTS(jna_pixels)-1 DO BEGIN ;get pointings, store in valradem
  
      CSPICE_GETFOV, -jna_pixels[i], 6, shape, frame_in_which_fov_was_defined, bsight, bounds ;GET FoV params
      bounds=[[bounds],[bounds[*,0]]]
      bounds_int=spice_fov_interpolate(bounds_in=bounds, npoints=3.0, fshape=shape)
  
      IF (i EQ 0) THEN val=bounds_int ELSE val=[[[val]],[[bounds_int]]];
  
    ENDFOR
    
  ENDIF ELSE BEGIN
    
    CSPICE_GETFOV, -28520l, 6, shape, frame_in_which_fov_was_defined, bsight, bounds ;GET FoV params
    bounds=[[bounds],[bounds[*,0]]]
    bounds_int=spice_fov_interpolate(bounds_in=bounds, npoints=10.0, fshape=shape)
    
    nv=SIZE(bounds_int)
    nvec=nv[2] ;number of pointing vectors
    val=REFORM(bounds_int,[3,nvec,1])
    
  ENDELSE
  
  IF (N_ELEMENTS(noise_sector) NE 0) THEN val[*,*,noise_sector]=!Values.F_NaN
  
  nv=SIZE(val)
  nvec=nv[2] ;number of pointing vectors
  nbs=nv[3] ;number of boresights 
  
ENDIF

;Begin the procedure to calculate and store pitch angles

CASE order OF
  'vit4': imodel = 'vit4_order04'
  'vip4_4': imodel  = 'vip4_order04'
  'vipal_5': imodel = 'vipal_order05'
  'o6_3': imodel = 'o6_order03'
  'isaac_10': imodel = 'isaac_order10'
  'jrm09_10': imodel = 'jrm09_order10'
  'jrm33_13': imodel = 'jrm33_order13'
  'jrm33_18': imodel = 'jrm33_order18'
  ELSE: BEGIN
    order =  'jrm09_10'
    imodel = 'jrm09_order10'
  END
ENDCASE

;get an estimate for the magnetic field (internal field model + CAN current sheet)
;output in IAU_JUPITER coordinates (cartesian)
;put this calculation outside the latter triple loop, for faster execution
IF keyword_set(add_ganymede) THEN BEGIN

  xyztg=get_ephem_juice_generic(date=trange, $
    step=step, sys='JUICE_GANYMEDE_MAG', center='GANYMEDE', target='JUICE', unit=2634.1d) ;unit=Rj so output in Rg or Rg/sec ;

  xg=xyztg[*,0]
  yg=xyztg[*,1]
  zg=xyztg[*,2]

  rg=SQRT(xg*xg + yg*yg + zg*zg)
  rgm=rg*2641.3e3
  
  bxg=719.0*3.0*xg*zg/rg^5.0
  byg=719.0*3.0*yg*zg/rg^5.0
  bzg=719.0*(3.0*zg^2.0/rg^2.0 -1.0)/rg^3.0
  
ENDIF

FOR i=0l, N_ELEMENTS(et_time)-1l DO BEGIN ;start a loop for all times
  
  dummy= execute('Bmag=jovian_'+imodel+'_internal_xyz(x[i], y[i], z[i]) +con2020_model_xyz(''analytic'', x[i], y[i], z[i])')
  
  IF keyword_set(add_ganymede) THEN BEGIN
    
    CSPICE_PXFORM, sysval[0], 'JUICE_GANYMEDE_MAG',  et_time[i], rotjup ;rotation matrix into the desired frame
    CSPICE_MXV, rotjup, transpose(Bmag), bj_final ;make the rotation in the desired frame
;    
    m0=1.25663706212e-6
    mom=-4.0*!PI/m0*bj_final*1e-9*2634.1e3^3.0/2.0
    rdotmom= cspice_vdot([xg[i],yg[i],zg[i]]*2641.3e3, mom)
    rdotmomr = rdotmom*[xg[i],yg[i],zg[i]]*2641.3e3
    
    bsec = m0/(4*!PI)*(3*rdotmomr - rgm[i]^2.0*mom)/rgm[i]^5.0*1e9
        
    CSPICE_PXFORM, 'JUICE_GANYMEDE_MAG', sysval[0], et_time[i], rotgan ;rotation matrix into the desired frame
    CSPICE_MXV, rotgan, [bxg[i],byg[i],bzg[i]], bg_final ;make the rotation in the desired frame
    CSPICE_MXV, rotgan, bsec, bsec_final ;make the rotation in the desired frame
    Bmag=Bmag+bg_final+bsec_final
    
  ENDIF

  um = TRANSPOSE(bmag/SQRT(TOTAL(bmag^2.0))); get the magnetic field unit vector
  
  IF (i EQ 0) THEN u_mag=um ELSE  u_mag=[[u_mag],[um]]
  
  bmag=TRANSPOSE(bmag)
  IF (i EQ 0) THEN b_mag=bmag ELSE  b_mag=[[b_mag],[bmag]]
  
ENDFOR

IF (STRLOWCASE(sensor) EQ 'joee') THEN BEGIN ;if keyword_set(high)

  IF keyword_set(radem) THEN BEGIN ;if RADEM is selected (only possible under sensor='joee' and /high , you need to make a separate estimation of its pitch angle coverage
    
    pitch_angle_radem=FLTARR(N_ELEMENTS(et_time), nvecradem, nbsradem) ;Make arrays to store pitch angle for radem

    FOR m=0l, nbsradem-1l DO BEGIN ;loop over number of vectors
      
      PRINT, 'Calculating pitch angles for RADEM direction ', STRTRIM(STRING(m+1),2), ' out of ', STRTRIM(STRING(nbsradem),2)
      
      FOR i=0l, N_ELEMENTS(et_time)-1l DO BEGIN ;start a loop for all times
        
        FOR k=0l, nvecradem-1 DO BEGIN
  
          bsightradem=valradem[*,k,m]
          CSPICE_PXFORM, frame_in_which_fov_was_defined_radem, sysval[0], et_time[i], rotate ;rotation matrix into the desired frame
          CSPICE_MXV, rotate, bsightradem, bsightradem_final ;make the rotation in the desired frame
  
          pitch_angle_radem[i,k,m] = 180.0-CSPICE_VSEP(u_mag[*,i],bsightradem_final) * !RADEG ;get the pitch angle. The subtraction from 180 is for converting from instrument to particle pitch angle
          
        ENDFOR

      ENDFOR
    
    ENDFOR

  ENDIF
  
  pitch_angle=FLTARR(N_ELEMENTS(et_time), nvec, nbs) ;nvec: number of pointing vectors, nbs: number of discrete boresights
  
  FOR m=0l, nbs-1l DO BEGIN ;loop over number of vectors
    
    PRINT, 'Calculating pitch angles for JoEE direction ', STRTRIM(STRING(m+1),2), ' out of ', STRTRIM(STRING(nbs),2)

    FOR i=0l, N_ELEMENTS(et_time)-1l DO BEGIN ;start a loop for all times
      
      FOR k=0, nvec-1l DO BEGIN
        
        bsighttemp=val[*,k,m] ;value of pointing vector k
        
        IF (bsighttemp[0]*0.0 EQ 0.0) THEN BEGIN
          
          CSPICE_PXFORM, frame_in_which_fov_was_defined, sysval[0], et_time[i], rotate ;rotation matrix into the desired frame
          CSPICE_MXV, rotate, bsighttemp, bsight_final ;make the rotation in the desired frame
    
          pitch_angle[i,k,m] = 180.0-CSPICE_VSEP(u_mag[*,i],bsight_final) * !RADEG ;get the pitch angle. The subtraction from 180 is for converting from instrument to particle pitch angle
          
        ENDIF ELSE BEGIN 
      
          pitch_angle[i,k,m]=!Values.F_NaN
      
        ENDELSE 
        
      ENDFOR
      
    ENDFOR

  ENDFOR
  
ENDIF ELSE BEGIN
  
  pitch_angle=FLTARR(N_ELEMENTS(et_time), nvec, nbs) ;nvec: number of pointing vectors, nbs: number of discrete boresights

  FOR m=0l, nbs-1l DO BEGIN ;loop over number of vectors

    PRINT, 'Calculating pitch angles for ',sensor,' sector ', STRTRIM(STRING(m+1),2), ' out of ', STRTRIM(STRING(nbs),2)

    FOR i=0l, N_ELEMENTS(et_time)-1l DO BEGIN ;start a loop for all times

      FOR k=0, nvec-1l DO BEGIN
        
        bsighttemp=val[*,k,m] ;value of pointing vector k
        
        IF (bsighttemp[0]*0.0 EQ 0.0) THEN BEGIN
  
          CSPICE_PXFORM, frame_in_which_fov_was_defined, sysval[0], et_time[i], rotate ;rotation matrix into the desired frame
          CSPICE_MXV, rotate, bsighttemp, bsight_final ;make the rotation in the desired frame
  
          pitch_angle[i,k,m] = 180.0-CSPICE_VSEP(u_mag[*,i],bsight_final) * !RADEG ;get the pitch angle. The subtraction from 180 is for converting from instrument to particle pitch angle
          
        ENDIF ELSE BEGIN
          
          pitch_angle[i,k,m]=!Values.F_NaN
          
        ENDELSE

      ENDFOR

    ENDFOR

  ENDFOR 
  
ENDELSE

;Pitch angle time series for all defined pointing directions have been calculated in pitch_angle
;Below is a simple procedure to plot pitch angle coverage for the given period
IF NOT keyword_set(noplot) THEN BEGIN 

  IF (STRLOWCASE(sensor) EQ 'joee') THEN BEGIN ;if keyword_et(high) do the same as above for each of the three discrete FoVs
    
    minval=FLTARR(N_ELEMENTS(et_time), nbs);array to store value of minimum pitch angle covered
    maxval=FLTARR(N_ELEMENTS(et_time), nbs);array to store value of maximum pitch angle covered
  
    FOR i=0l, N_ELEMENTS(et_time)-1l DO BEGIN ;loop for all times to get min/max values above
      
      FOR k=0l, nbs-1l DO BEGIN 
  
        minval[i,k]=MIN(pitch_angle[i,*, k],/NaN)
        maxval[i,k]=MAX(pitch_angle[i,*, k],/NaN)
        
      ENDFOR
  
    ENDFOR
  
    cspice_et2utc, et_time, 'J', 6, julian_time_string; generate time in JD, useful for IDL plot labeling
    julian_time=DBLARR(N_ELEMENTS(julian_time_string))
    FOR i=0l,N_ELEMENTS(julian_time_string)-1l DO julian_time[i]=DOUBLE(STRSPLIT(julian_time_string[i],'JD ',/EXTRACT))  ;remove the "JD " part of the julian date string and convert to double
  
    tlabels=LABEL_DATE(DATE_FORMAT=['%H:%I:%S','%Y-%M-%D']);format of plot labels for the time axis
  
    PLOT, julian_time, minval, /xstyle, yrange=[0,180], /ystyle, XTICKUNITS = ['Time', 'Time'], position=[0.1,0.55,0.9,0.95],$
      XTICKFORMAT='LABEL_DATE', ytitle='Pitch Angle ('+STRUPCASE(sensor)+') [deg]', chars=1.05,/nodata, ticklen=-0.02
  
    FOR k=0l, nbs-1l DO POLYFILL, [julian_time, REVERSE(julian_time)],[minval[*,k], REVERSE(maxval[*,k])], color=254;/(nbs+1)*(k+1)
    
    IF keyword_set(radem) THEN BEGIN ;add radem FoV
  
      minvalradem=FLTARR(N_ELEMENTS(et_time),nbsradem);array to store value of minimum pitch angle covered
      maxvalradem=FLTARR(N_ELEMENTS(et_time),nbsradem);array to store value of maximum pitch angle covered
  
      FOR i=0l, N_ELEMENTS(et_time)-1l DO BEGIN ;loop for all times to get min/max values above
  
        FOR k=0l, nbsradem-1l DO BEGIN 
    
          minvalradem[i,k]=MIN(pitch_angle_radem[i,*, k],/NaN)
          maxvalradem[i,k]=MAX(pitch_angle_radem[i,*, k],/NaN)
          
        ENDFOR
  
      ENDFOR
      
      FOR k=0l, nbsradem-1l DO POLYFILL, [julian_time, REVERSE(julian_time)],[minvalradem[*,k], REVERSE(maxvalradem[*,k])], color=45, /line_fill, orientation=45., thick=0.5, spacing=0.02
  
      LEGEND, ['JOEE','RADEM'], color=[254,45], chars=0.85, spacing=0.15, line=0, thick=6, /normal, position=[0.92,0.95]
  
    ENDIF
  
  
  ENDIF ELSE BEGIN
    
    minval=FLTARR(N_ELEMENTS(et_time), nbs);array to store value of minimum pitch angle covered
    maxval=FLTARR(N_ELEMENTS(et_time), nbs);array to store value of maximum pitch angle covered
    
    FOR i=0l, N_ELEMENTS(et_time)-1l DO BEGIN ;loop for all times to get min/max values above
  
      FOR k=0l, nbs-1l DO BEGIN 
  
        minval[i,k]=MIN(pitch_angle[i,*, k],/NaN)
        maxval[i,k]=MAX(pitch_angle[i,*, k],/NaN)
        
      ENDFOR
  
    ENDFOR
  
    cspice_et2utc, et_time, 'J', 6, julian_time_string; generate time in JD, useful for IDL plot labeling
    julian_time=DBLARR(N_ELEMENTS(julian_time_string))
    FOR i=0l,N_ELEMENTS(julian_time_string)-1l DO julian_time[i]=DOUBLE(STRSPLIT(julian_time_string[i],'JD ',/EXTRACT))  ;remove the "JD " part of the julian date string and convert to double
  
    tlabels=LABEL_DATE(DATE_FORMAT=['%H:%I:%S','%Y-%M-%D']);format of plot labels for the time axis
  
    PLOT, julian_time, minval, /xstyle, yrange=[0,180], /ystyle, XTICKUNITS = ['Time', 'Time'], position=[0.1,0.55,0.9,0.95],$
      XTICKFORMAT='LABEL_DATE', ytitle='Pitch Angle ('+STRUPCASE(sensor)+') [deg]', chars=1.05,/nodata, ticklen=-0.02
  
    FOR k=0l, nbs-1l DO POLYFILL, [julian_time, REVERSE(julian_time)],[minval[*,k], REVERSE(maxval[*,k])], color=254;/(nbs+1)*(k+1)
    
  ENDELSE
  
  IF (N_ELEMENTS(ca) EQ 1) THEN BEGIN ;mark an event, e.g. CA of a flyby
    
    cspice_str2et, ca, etca
    cspice_et2utc, etca, 'J', 6, julian_ca_string; generate time in JD, useful for IDL plot labeling
    julian_time_ca=DOUBLE(STRSPLIT(julian_ca_string,'JD ',/EXTRACT))
    OPLOT, [julian_time_ca,julian_time_ca], [0,180], line=2
    
  ENDIF
  
  IF keyword_set(mark_ptr) THEN BEGIN
  
    ptr=read_juice_ptr()
  
    sz=size(ptr)
    ptrcount=sz[1]
  
    PRINT, 'PTR blocks' ;print metakernel names on screen
    PRINT, '-----------------------'
    FOR i=0l, ptrcount-1l DO BEGIN
  
      PRINT, STRTRIM(STRING(i),2)+'. '+ptr[i,0]+' '+ptr[i,1]+' '+ptr[i,2]
  
    ENDFOR
    input_key=''
    READ, input_key, PROMPT='Select (0-'+STRTRIM(STRING(ptrcount-1l),2)+'). You may select a single event (e.g. 0), or multiple (e.g. 1,4,5,7):' ;select block
    input_key=LONG(STRSPLIT(input_key,',', /extract))
    
    FOR i=0l, N_ELEMENTS(input_key)-1l DO BEGIN
  
      tstart=ptr[input_key[i],0]
      cspice_str2et, tstart, etstart
      cspice_et2utc, etstart, 'J', 6, julian_tstart_string; generate time in JD, useful for IDL plot labeling
      julian_time_start=DOUBLE(STRSPLIT(julian_tstart_string,'JD ',/EXTRACT))
    
    
      tstop=ptr[input_key[i],1]
      cspice_str2et, tstop, etstop
      cspice_et2utc, etstop, 'J', 6, julian_tstop_string; generate time in JD, useful for IDL plot labeling
      julian_time_stop=DOUBLE(STRSPLIT(julian_tstop_string,'JD ',/EXTRACT))
    
      POLYFILL, [julian_time_start,julian_time_stop,julian_time_stop,julian_time_start], [0, 0, 180,180], /line_fill, orient=45.0, color=180, spacing=0.05, noclip=0
    
      comm=STRMID(ptr[input_key[i],2],0,30)
    
      XYOUTS, 0.5*(julian_time_start+julian_time_stop), 182.0, comm, /DATA, chars=0.45, orient=45.0
      
    ENDFOR
    
  ENDIF
  
  IF keyword_set(mark_event) THEN BEGIN
  
    ev=juice_segment_read(type_segment=event)
    
    IF (ev[0] NE '') THEN BEGIN
      
      evdesc=ev[0,*]
      evstart=ev[1,*]
      evstop=ev[2,*]
      evtype=ev[3,*]
  
      FOR i=0l, N_ELEMENTS(evstart)-1l DO BEGIN
    
        cspice_str2et, evstart[i], etstart
        cspice_et2utc, etstart, 'J', 6, julian_tstart_string; generate time in JD, useful for IDL plot labeling
        julian_time_start=DOUBLE(STRSPLIT(julian_tstart_string,'JD ',/EXTRACT))
    
        cspice_str2et, evstop[i], etstop
        cspice_et2utc, etstop, 'J', 6, julian_tstop_string; generate time in JD, useful for IDL plot labeling
        julian_time_stop=DOUBLE(STRSPLIT(julian_tstop_string,'JD ',/EXTRACT))
          
        IF (julian_time_stop GT MIN(julian_time) AND julian_time_start LT MAX(julian_time)) THEN plt=1 ELSE plt=0
        
        IF (plt EQ 1) THEN BEGIN 
          
          POLYFILL, [julian_time_start,julian_time_stop,julian_time_stop,julian_time_start], [0, 0, 180,180], /line_fill, orient=45.0, color=180, spacing=0.05, noclip=0
    
          comm=STRMID(evdesc[i],0,30)
    
          XYOUTS, 0.5*(julian_time_start+julian_time_stop), 182.0, comm, /DATA, chars=0.45, orient=45.0
          
        ENDIF
  
      ENDFOR
      
    ENDIF
  
  ENDIF
  
  ;2nd panel plot
  
  PLOT, julian_time, r2, /xstyle, yrange=[min([r2,x2,y2,z2]), max([r2,x2,y2,z2])], /ystyle, XTICKUNITS = ['Time', 'Time'], position=[0.1,0.1,0.9,0.425],$
    XTICKFORMAT='LABEL_DATE', ytitle=yt, chars=1.25, ticklen=-0.02, thick=3, /noerase
  
  OPLOT, julian_time, x2, color=254, thick=3
  OPLOT, julian_time, y2, color=160, thick=3
  OPLOT, julian_time, z2, color=60, thick=3

  XYOUTS, 0.92, 0.15, 'X', /NORM, color=254 
  XYOUTS, 0.92, 0.2, 'Y', /NORM, color=160 
  XYOUTS, 0.92, 0.25, 'Z', /NORM, color=60 
  XYOUTS, 0.92, 0.3, 'R', /NORM
  
  XYOUTS, 0.92, 0.35, sysval[1], /NORM, color=30, ORIENTATION=45.0
    
  IF (N_ELEMENTS(ca) EQ 1) THEN BEGIN ;mark an event, e.g. CA of a flyby
  
    cspice_str2et, ca, etca
    cspice_et2utc, etca, 'J', 6, julian_ca_string; generate time in JD, useful for IDL plot labeling
    julian_time_ca=DOUBLE(STRSPLIT(julian_ca_string,'JD ',/EXTRACT))
    OPLOT, [julian_time_ca,julian_time_ca], [0,180], line=2
  
  ENDIF
  
  IF keyword_set(mark_ptr) THEN BEGIN
    
    FOR i=0l, N_ELEMENTS(input_key)-1l DO BEGIN
  
      tstart=ptr[input_key[i],0]
      cspice_str2et, tstart, etstart
      cspice_et2utc, etstart, 'J', 6, julian_tstart_string; generate time in JD, useful for IDL plot labeling
      julian_time_start=DOUBLE(STRSPLIT(julian_tstart_string,'JD ',/EXTRACT))
    
    
      tstop=ptr[input_key[i],1]
      cspice_str2et, tstop, etstop
      cspice_et2utc, etstop, 'J', 6, julian_tstop_string; generate time in JD, useful for IDL plot labeling
      julian_time_stop=DOUBLE(STRSPLIT(julian_tstop_string,'JD ',/EXTRACT))
    
      POLYFILL, [julian_time_start,julian_time_stop,julian_time_stop,julian_time_start], [MIN(r2), MIN(r2), MAX(r2), MAX(r2)], /line_fill, orient=45.0, color=180, spacing=0.05, noclip=0
      
    ENDFOR
    
  ENDIF
  
  IF keyword_set(mark_event) THEN BEGIN
    
    IF (ev[0] NE '') THEN BEGIN
  
      FOR i=0l, N_ELEMENTS(evstart)-1l DO BEGIN
    
        cspice_str2et, evstart[i], etstart
        cspice_et2utc, etstart, 'J', 6, julian_tstart_string; generate time in JD, useful for IDL plot labeling
        julian_time_start=DOUBLE(STRSPLIT(julian_tstart_string,'JD ',/EXTRACT))
    
        cspice_str2et, evstop[i], etstop
        cspice_et2utc, etstop, 'J', 6, julian_tstop_string; generate time in JD, useful for IDL plot labeling
        julian_time_stop=DOUBLE(STRSPLIT(julian_tstop_string,'JD ',/EXTRACT))
    
        IF (julian_time_stop GT MIN(julian_time) AND julian_time_start LT MAX(julian_time)) THEN plt=1 ELSE plt=0
    
        IF (plt EQ 1) THEN POLYFILL, [julian_time_start,julian_time_stop,julian_time_stop,julian_time_start], [MIN(r2), MIN(r2), MAX(r2), MAX(r2)], $
                            /line_fill, orient=45.0, color=180, spacing=0.05, noclip=0
        
      ENDFOR
    
    ENDIF   
  
  ENDIF
  
ENDIF

!P=p0; revert back to the original plot settings

cd, dirmk
cspice_kclear ;unload spice kernels

IF keyword_set(ps) THEN BEGIN
  
  Device, /Close_file  ; close postscript if my postscript plot calling method is used
  
  IF (strupcase(!version.os_family) EQ 'UNIX') THEN BEGIN ;If you want to convert to pdf and have ps2pdf installed (only works for unix)
    
    cd, out_dir
    spawn, 'ps2pdf '+output+'.ps'
    
  ENDIF
  
ENDIF

IF (N_ELEMENTS(export) NE 0) THEN BEGIN ;export in savefile
  
  IF NOT keyword_set(noplot)  THEN save, filename=out_dir+output+'.sav', minval, maxval, julian_time, et_time, pitch_angle, sensor, trange ; in this case, CA timing is also stored
  IF keyword_set(noplot) THEN save, filename=out_dir+output+'.sav', et_time, pitch_angle, sensor, trange
  
ENDIF

IF (strupcase(!version.os_family) EQ 'WINDOWS') THEN BEGIN

  Set_plot, 'Win'

ENDIF ELSE IF (strupcase(!version.os_family) EQ 'UNIX') THEN BEGIN

  Set_plot, 'X'

ENDIF

END