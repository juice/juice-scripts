FUNCTION juice_segment_read, list_segments=list_segments, dir=dir, type_segment=type_segment

  ;+
  ; NAME:
  ; juice_segment_read
  ;
  ; PURPOSE:
  ; Read segmentation files from JUICE available at: https://www.cosmos.esa.int/web/juice/shared-data
  ;
  ; CATEGORY:
  ; JUICE event/segment scripts
  ;
  ; CALLING SEQUENCE:
  ; out=juice_segment_read([type_segment=type_segment],[/list_segment],[dir=dir])
  ;
  ; INPUTS:
  ; type_segment: STRING, defining the segment(s) to be marked up. To see what events are available, open the segment file or run the script with /list_events. 
  ;             If not defined, all events are chosen. Input can be both upper or lowercase
  ; list_segment: If set, list of available events is printed on screen
  ; dir: STRING, directory where segment data are stored. A default value is set at the start of the script, you may modify it if you dont want to set the dir always.
  ; 
  ; OUTPUTS:
  ; STRING array of [No of segments, 4]
  ; Column 0: Event type 
  ; Column 1: Start time UTC
  ; Column 2: Stop Time UTC
  ; Column 3: Segment type
  ; 
  ; COMMON BLOCKS:
  ; None.
  ;
  ; SIDE EFFECTS:
  ; None.
  ;
  ; RESTRICTIONS:
  ; Requires JUICE SPICE kernels
  ;
  ; PROCEDURE:
  ; 1) Print list of segments (the user will be prompted to select an segments file)
  ; out=juice_segment_read(/list)
  ; 
  ; 2) Print times of segment 'DL_', 'DL_EXT' from segmentation_proposal_Jupiter_Phase_all_crema_5_0b23_1_MALARGUE.csv. The user will be promted to select that csv file
  ; out=juice_segment_read(type_event=['DL_','DL_EXT'])
  ; print, out
  ;
  ; MODIFICATION HISTORY:
  ; Written, ER, October, 2022.



IF (N_ELEMENTS(dir) EQ 0) THEN dir='/Users/roussos/owncloud/mylib/juice_scripts/Input_data/DATA/' ;SPICE kernels directory

evfile=file_search(dir, 'segmentation*.csv',count=mkcount)

PRINT, 'Segment Information Files' ;print metakernel names on screen
PRINT, '-----------------------'

FOR i=0l, mkcount-1l DO BEGIN

  PRINT, STRTRIM(STRING(i),2)+'. '+ strsplit(evfile[i], dir,/extract,/regex)

ENDFOR

READ, input_key, PROMPT='Select (0-'+STRTRIM(STRING(mkcount-1l),2)+'):' ;select 

ev=READ_CSV(evfile[input_key])

event=ev.field1
event_time_start=ev.field2
event_time_stop=ev.field3
event_type=ev.field4


event_sort=event(SORT(event))
event_unique=event_sort(UNIQ(event_sort))


IF keyword_set(list_segments) THEN BEGIN
  
  IF (N_ELEMENTS(event_unique) EQ 1) THEN PRINT, event_unique ELSE PRINT, TRANSPOSE(event_unique)
  RETURN, ''

ENDIF ELSE IF (N_ELEMENTS(type_segment) NE 0) THEN BEGIN
  
  FOR i=0l, N_ELEMENTS(type_segment)-1 DO BEGIN 

    tst=WHERE(STRUPCASE(event) EQ STRUPCASE(type_segment[i]))
    IF (i EQ 0 and tst[0] NE -1) THEN out=[[event[tst]], [event_time_start[tst]],[event_time_stop[tst]],[event_type[tst]]]
    IF (i NE 0 and tst[0] NE -1 and N_ELEMENTS(out) NE 0) THEN out=[out,[[event[tst]], [event_time_start[tst]],[event_time_stop[tst]],[event_type[tst]]]]
    IF (i NE 0 and tst[0] NE -1 and N_ELEMENTS(out) EQ 0) THEN out=[[event[tst]], [event_time_start[tst]],[event_time_stop[tst]],[event_type[tst]]]
    IF (i EQ N_ELEMENTS(type_segment)-1 and N_ELEMENTS(out) EQ 0) THEN out=''
    
  ENDFOR
  IF (out[0] NE '') THEN out=TRANSPOSE(out)
  RETURN, out

ENDIF ELSE BEGIN
  
  PRINT, 'No input given. All segments exported'
  out=TRANSPOSE([[event],[event_time_start],[event_time_stop],[event_type]])
  RETURN, out

  
ENDELSE

END