PRO radem_pitch_angle,$
    step=step,$ 
    trange=trange,$
    sensor=sensor,$
    ps=ps,$ 
    output=output,$
    ca=ca,$
    export=export,$
    shade=shade,$
    dir=dir,$
    out_dir=out_dir,$
    order=order,$
    mark_ptr=mark_ptr,$
    mark_event=mark_event
    
    
  ;+
  ; NAME:
  ; radem_pitch_angle
  ;
  ; PURPOSE:
  ; Calculate RADEM sensor pitch angle coverage
  ;
  ; CATEGORY:
  ; JUICE pointing scripts
  ;
  ; CALLING SEQUENCE:
  ; rade_pitch_angle, [step=step], [trange=trange], [sensor=sensor], [dir=dir], [mm=mm], [export=export],$
  ;                   [ps=ps], [output=output], [mark_ptr=mark_ptr], [mark_event=mark_event],[order=order]
  ;
  ; INPUTS:
  ;
  ; step: time step in seconds, default is 120 sec
  ; trange: time range (format example trange=['2032-01-11T05:44:04.000','2032-01-12T05:44:04.000'], time format accepted any UTC format that works for SPICE)
  ; sensor: which RADEM sensor (options: 'dd' for directionality, 'ud' for unidirectional detectors). Default is 'dd' 
  ; ca: Mark CA time, if a flyby is plotted e.g. ca='2032-07-02T16:22:11.000'. Default: CA undefined
  ; ps: export to postscript
  ; output: ps output filename
  ; mark_ptr: If a PTR file is available (e.g. from JUICE pointing tool, read and mark PTR events). Selecting this will prompt the user to select a ptr file.
  ; mark_event: Use SOC segmentation event files to mark events
  ; order: Jovian field expansion order, options: '', 'vit4', 'vip4_4', 'vipal_5', 'o6_3', 'isaac_10', 'jrm09_10', 'jrm33_13','jrm33_18'. Note that ''-->'jrm09_10' with older code. Default: jrm09_10
  ; dir: location of SPICE kernels directory. Default can be changed directly in the script
  ; out_dir: directory to store the output file. . Default can be changed directly in the script
  ; shade: fill the area of pitch angle coverage in the time plot
  ;
  ;
  ; OUTPUTS:
  ; Depending on the keywords a single panel plot is shown on the screen with the PAD coverage of the selected RADEM sensor.
  ; If ps is set, plot is saved in the location defined by out_dir, with filename defined through output keyword. ca, mark_ptr and mark_event will annotate the plot
  ; export will save some of the calculations in an IDL savefile (output folder, name defined by output keyword).
  ;
  ; COMMON BLOCKS:
  ; None.
  ;
  ; SIDE EFFECTS:
  ; None.
  ;
  ; RESTRICTIONS:
  ; Requires JUICE SPICE kernels, downloaded ptr, event files (if to be used).
  ;
  ; PROCEDURE:
  ; Adjust input folders in the beginning of the script: out_dir, dir
  ;
  ; Example: radem_pitch_angle, step=120d, sensor='dd', /ps,  out='RADEM_PAD', ca='2032-07-02T16:22:11.000' 
  ; 
  ; MODIFICATION HISTORY:
  ; Written, ER, October, 2022.
    
    
P0=!p ;Save default idl plot settings

LOADCT, 39
!p.thick = 2.5 ;make some modification of initial plot settings for this routine
!x.thick = 2.5
!y.thick = 2.5
!z.thick = 2.5

IF (N_ELEMENTS(dir) EQ 0) THEN dir='/Users/roussos/ownCloud/Work/JUICE/SPICE/kernels/' ;SPICE kernels directory
IF (N_ELEMENTS(out_dir) EQ 0) THEN out_dir='/Users/roussos/'

;some inputs
dir='/Users/roussos/ownCloud/Work/JUICE/SPICE/kernels/' ;SPICE kernels directory\
dirmk = dir + 'mk/' ;metakernels directory
sysval='IAU_JUPITER' ; use this coordinate system for follow-up SPICE calculations since magnetic field models return mag vector in that


IF keyword_set(ps) THEN BEGIN ;If postscript output is required, prepare the environment

  Set_Plot, 'ps'
  !p.font=0
  Device, Color=1, Bits_Per_pixel=8, /Landscape
  IF (N_ELEMENTS(output) EQ 0) THEN output='radem_pitch' ;default name for plot file, if not given

  Device, Filename=out_dir+output+'.ps' ;change here the folder path to save plots

ENDIF

;Define some default values
IF (N_ELEMENTS(step) EQ 0) THEN step=120d ;time step for ephemeris in seconds
IF (N_ELEMENTS(trange) EQ 0) THEN trange=['2032-07-02T04:22:11.000','2032-07-03T04:22:11.000'] ; Europa flyby 1, CREMA 5.0
IF (N_ELEMENTS(order) EQ 0) THEN order='jrm09_10'
order=STRLOWCASE(order); make sure input of order is in lowercase

rj=71492.0 ;jupiter radius in km

PRINT, 'Pitch Angles will be calculated for RADEM/'+STRTRIM(sensor,2)
PRINT,''

cd, dirmk ;change to metakernels directory
metakernel=file_search('*.tm', count=mkcount) ;find all metakernels

PRINT, 'SPICE metakernels' ;print metakernel names on screen
PRINT, '-----------------------'
FOR i=0l, mkcount-1l DO BEGIN

  PRINT, STRTRIM(STRING(i),2)+'. '+metakernel[i]

ENDFOR

READ, input_key, PROMPT='Select (0-'+STRTRIM(STRING(mkcount-1l),2)+'):' ;select metakernel
cspice_furnsh, dirmk + metakernel[input_key] ;load selected metakernel

;;;;;GET THE POSITION;;;;;;
;below get the JUICE position in the desired coordinate system (sysval) in unit=1.0 km, stored
;in the xyzt array
;xyzt[0:2]--> x, y, z position
;xyzt[3:5]--> x, y, z velocity
;xyzt[6]--> ET time (seconds)

xyzt=get_ephem_juice_generic(date=trange, $
  step=step, sys=sysval, center='JUPITER', target='JUICE', unit=rj) ;unit=Rj so output in Rj or Rj/sec

et_time=xyzt[*,6] ;assign a variable to the ET-time for simplicity

x=xyzt[*,0] ;x,y,z variables
y=xyzt[*,1]
z=xyzt[*,2]
r=SQRT(x*x + y*y + z*z) ;radial distance
colat=ACOS(z/r) ;this is in the 0-->pi range, as required by SPICE
s3lone=ATAN(y,x) ;s3 longitude, east
test=WHERE(s3lone LT 0)
IF (test[0] NE -1) THEN s3lone[test]=2*!PI +s3lone[test] ;convert to 0-->2pi

;Select instrument
;Get boresight and boundary corner vectors to get a good sample of pointings covered

IF (STRLOWCASE(sensor) EQ 'dd') THEN BEGIN
  
  dd_pixels=2892400l +LINDGEN(28)

  FOR i=0l, N_ELEMENTS(dd_pixels)-1 DO BEGIN ;get pointings 

    CSPICE_GETFOV, -dd_pixels[i], 6, shape, frame_in_which_fov_was_defined, bsight, bounds ;GET FoV params 
    IF (i EQ 0) THEN val=bsight ELSE val=[[val],[bsight]];

  ENDFOR

  nvec=SIZE(val)
  nvec=nvec[2] ;number of pointing vectors

ENDIF ELSE IF (STRLOWCASE(sensor) EQ 'ud') THEN BEGIN
  
  ud_pixels=28921l +LINDGEN(3)

  FOR i=0l, N_ELEMENTS(ud_pixels)-1 DO BEGIN ;get pointings 

    CSPICE_GETFOV, -ud_pixels[i], 6, shape, frame_in_which_fov_was_defined, bsight, bounds ;GET FoV params
    IF (i EQ 0) THEN val=bsight ELSE val=[[val],[bsight]];

  ENDFOR

  nvec=SIZE(val)
  nvec=nvec[2] ;number of pointing vectors

ENDIF

;Begin the procedure to calculate and store pitch angles
  
pitch_angle=FLTARR(N_ELEMENTS(et_time), nvec) ;Make arrays to store pitch angle

FOR k=0l, nvec-1l DO BEGIN ;loop over number of vectors
  
  FOR i=0l, N_ELEMENTS(et_time)-1l DO BEGIN ;start a loop for all times

    bsight=val[*,k] ;value of pointing vector k

    CSPICE_PXFORM, frame_in_which_fov_was_defined, sysval, et_time[i], rotate ;rotation matrix into the desired frame
    CSPICE_MXV, rotate, bsight, bsight_final ;make the rotation in the desired frame
    
    ;get an estimate for the magnetic field (JRM09 internal field model + CAN current sheet)
    ;output in IAU_JUPITER coordinates (cartesian)
    IF (order EQ '') THEN Bmag = jovian_jrm09_internal(r[i], colat[i], s3lone[i], /tocart)+con2020_model_xyz('analytic', x[i], y[i], z[i])
    IF (order EQ 'vit4') THEN Bmag = jovian_vit4_order04_internal_xyz(x[i], y[i], z[i])+con2020_model_xyz('analytic', x[i], y[i], z[i])
    IF (order EQ 'vip4_4') THEN Bmag = jovian_vip4_order04_internal_xyz(x[i], y[i], z[i])+con2020_model_xyz('analytic', x[i], y[i], z[i])
    IF (order EQ 'vipal_5') THEN Bmag = jovian_vipal_order05_internal_xyz(x[i], y[i], z[i])+con2020_model_xyz('analytic', x[i], y[i], z[i])
    IF (order EQ 'o6_3') THEN Bmag =jovian_o6_order03_internal_xyz(x[i], y[i], z[i])+con2020_model_xyz('analytic', x[i], y[i], z[i])
    IF (order EQ 'isaac_10') THEN Bmag =jovian_isaac_order10_internal_xyz(x[i], y[i], z[i])+con2020_model_xyz('analytic', x[i], y[i], z[i])
    IF (order EQ 'jrm09_10') THEN Bmag =jovian_jrm09_order10_internal_xyz(x[i], y[i], z[i])+con2020_model_xyz('analytic', x[i], y[i], z[i])
    IF (order EQ 'jrm33_13') THEN Bmag =jovian_jrm33_order13_internal_xyz(x[i], y[i], z[i])+con2020_model_xyz('analytic', x[i], y[i], z[i])
    IF (order EQ 'jrm33_18') THEN Bmag =jovian_jrm33_order18_internal_xyz(x[i], y[i], z[i])+con2020_model_xyz('analytic', x[i], y[i], z[i])

    u_mag = TRANSPOSE(bmag/SQRT(TOTAL(bmag^2.0))); get the magnetic field unit vector

    pitch_angle[i,k] = 180.0-CSPICE_VSEP(u_mag,bsight_final) * !RADEG ;get the pitch angle. The subtraction from 180 is for converting from instrument to particle pitch angle

  ENDFOR

ENDFOR
    
;Pitch angle time series for all defined pointing directions have been calculated in pitch_angle
;Below is a simple procedure to plot pitch angle coverage for the given period
  
minval=FLTARR(N_ELEMENTS(et_time));array to store value of minimum pitch angle covered
maxval=FLTARR(N_ELEMENTS(et_time));array to store value of maximum pitch angle covered

FOR i=0l, N_ELEMENTS(et_time)-1l DO BEGIN ;loop for all times to get min/max values above
  
  minval[i]=MIN(pitch_angle[i,*],/NaN)
  maxval[i]=MAX(pitch_angle[i,*],/NaN)

ENDFOR

cspice_et2utc, et_time, 'J', 6, julian_time_string; generate time in JD, useful for IDL plot labeling
julian_time=DBLARR(N_ELEMENTS(julian_time_string))
FOR i=0l,N_ELEMENTS(julian_time_string)-1l DO julian_time[i]=DOUBLE(STRSPLIT(julian_time_string[i],'JD ',/EXTRACT))  ;remove the "JD " part of the julian date string and convert to double

tlabels=LABEL_DATE(DATE_FORMAT=['%H:%I:%S','%Y-%M-%D']);format of plot labels for the time axis

PLOT, julian_time, minval, /xstyle, yrange=[0,180], /ystyle, XTICKUNITS = ['Time', 'Time'], position=[0.1,0.55,0.9,0.95],$
  XTICKFORMAT='LABEL_DATE', ytitle='Pitch Angle ('+STRUPCASE(sensor)+') [deg]', chars=1.25,/nodata, ticklen=-0.02
  
IF keyword_set(shade) THEN BEGIN 
  
  POLYFILL, [julian_time, REVERSE(julian_time)],[minval, REVERSE(maxval)], color=254
  
ENDIF ELSE BEGIN
  
  FOR i=0l, nvec-1 DO OPLOT, julian_time, pitch_angle[*,i], color=254, thick=3
 
ENDELSE

IF (N_ELEMENTS(ca) EQ 1) THEN BEGIN
  
  cspice_str2et, ca, etca
  cspice_et2utc, etca, 'J', 6, julian_ca_string; generate time in JD, useful for IDL plot labeling
  julian_time_ca=DOUBLE(STRSPLIT(julian_ca_string,'JD ',/EXTRACT))
  OPLOT, [julian_time_ca,julian_time_ca], [0,180], line=2
  
ENDIF

IF keyword_set(mark_ptr) THEN BEGIN

  ptr=read_juice_ptr()

  sz=size(ptr)
  ptrcount=sz[1]

  PRINT, 'PTR blocks' ;print metakernel names on screen
  PRINT, '-----------------------'
  FOR i=0l, ptrcount-1l DO BEGIN

    PRINT, STRTRIM(STRING(i),2)+'. '+ptr[i,0]+' '+ptr[i,1]+' '+ptr[i,2]

  ENDFOR
  input_key=''
  READ, input_key, PROMPT='Select (0-'+STRTRIM(STRING(ptrcount-1l),2)+'). You may select a single event (e.g. 0), or multiple (e.g. 1,4,5,7):' ;select block
  input_key=LONG(STRSPLIT(input_key,',', /extract))

  FOR i=0l, N_ELEMENTS(input_key)-1l DO BEGIN

    tstart=ptr[input_key[i],0]
    cspice_str2et, tstart, etstart
    cspice_et2utc, etstart, 'J', 6, julian_tstart_string; generate time in JD, useful for IDL plot labeling
    julian_time_start=DOUBLE(STRSPLIT(julian_tstart_string,'JD ',/EXTRACT))


    tstop=ptr[input_key[i],1]
    cspice_str2et, tstop, etstop
    cspice_et2utc, etstop, 'J', 6, julian_tstop_string; generate time in JD, useful for IDL plot labeling
    julian_time_stop=DOUBLE(STRSPLIT(julian_tstop_string,'JD ',/EXTRACT))

    POLYFILL, [julian_time_start,julian_time_stop,julian_time_stop,julian_time_start], [0, 0, 180,180], /line_fill, orient=45.0, color=180, spacing=0.05, noclip=0

    comm=STRMID(ptr[input_key[i],2],0,30)

    XYOUTS, 0.5*(julian_time_start+julian_time_stop), 182.0, comm, /DATA, chars=0.45, orient=45.0

  ENDFOR

ENDIF

IF keyword_set(mark_event) THEN BEGIN

  ev=juice_segment_read(type_segment=event)

  IF (ev[0] NE '') THEN BEGIN

    evdesc=ev[0,*]
    evstart=ev[1,*]
    evstop=ev[2,*]
    evtype=ev[3,*]

    FOR i=0l, N_ELEMENTS(evstart)-1l DO BEGIN

      cspice_str2et, evstart[i], etstart
      cspice_et2utc, etstart, 'J', 6, julian_tstart_string; generate time in JD, useful for IDL plot labeling
      julian_time_start=DOUBLE(STRSPLIT(julian_tstart_string,'JD ',/EXTRACT))

      cspice_str2et, evstop[i], etstop
      cspice_et2utc, etstop, 'J', 6, julian_tstop_string; generate time in JD, useful for IDL plot labeling
      julian_time_stop=DOUBLE(STRSPLIT(julian_tstop_string,'JD ',/EXTRACT))

      IF (julian_time_stop GT MIN(julian_time) AND julian_time_start LT MAX(julian_time)) THEN plt=1 ELSE plt=0

      IF (plt EQ 1) THEN BEGIN

        POLYFILL, [julian_time_start,julian_time_stop,julian_time_stop,julian_time_start], [0, 0, 180,180], /line_fill, orient=45.0, color=180, spacing=0.05, noclip=0

        comm=STRMID(evdesc[i],0,30)

        XYOUTS, 0.5*(julian_time_start+julian_time_stop), 182.0, comm, /DATA, chars=0.45, orient=45.0

      ENDIF

    ENDFOR

  ENDIF

ENDIF

!P=p0; revert back to the original plot settings

cd, dirmk
cspice_kclear ;unload spice kernels

IF keyword_set(ps) THEN BEGIN
  
  Device, /Close_file  ; close postscript if my postscript plot calling method is used
  IF (strupcase(!version.os_family) EQ 'UNIX') THEN BEGIN ;If you want to convert to pdf and have ps2pdf installed (only works for unix)
    
    cd, out_dir
    spawn, 'ps2pdf '+output+'.ps'
    
  ENDIF
  
ENDIF

IF (N_ELEMENTS(export) NE 0) THEN BEGIN
  
  IF (N_ELEMENTS(ca) EQ 1) THEN save, filename=out_dir+export+'.sav', julian_time_ca, minval, maxval, julian_time
  IF (N_ELEMENTS(ca) EQ 0) THEN save, filename=out_dir+'.sav', minval, maxval, julian_time
  
ENDIF

IF (strupcase(!version.os_family) EQ 'WINDOWS') THEN BEGIN

  Set_plot, 'Win'

ENDIF ELSE IF (strupcase(!version.os_family) EQ 'UNIX') THEN BEGIN

  Set_plot, 'X'

ENDIF

END