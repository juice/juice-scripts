FUNCTION juice_event_read, list_events=list_events, dir=dir, type_event=type_event

  ;+
  ; NAME:
  ; juice_event_read
  ;
  ; PURPOSE:
  ; Read event files from JUICE available at: https://www.cosmos.esa.int/web/juice/shared-data
  ;
  ; CATEGORY:
  ; JUICE event scripts
  ;
  ; CALLING SEQUENCE:
  ; out=juice_event_read([type_event=type_event],[/list_events],[dir=dir])
  ;
  ; INPUTS:
  ; type_event: STRING, defining the event(s) to be marked up. To see what events are available, open the event file or run the script with /list_events. 
  ;             If not defined, all events are chosen. Input can be both upper or lowercase
  ; list_events: If set, list of available events is printed on screen
  ; dir: STRING, directory where event data are stored. A default value is set at the start of the script, you may modify it if you dont want to set the dir always.
  ; 
  ; OUTPUTS:
  ; STRING array of [No of events, 2]
  ; Column 0: Event type 
  ; Column 1: Event time, UTC
  ; 
  ; COMMON BLOCKS:
  ; None.
  ;
  ; SIDE EFFECTS:
  ; None.
  ;
  ; RESTRICTIONS:
  ; Requires JUICE SPICE kernels
  ;
  ; PROCEDURE:
  ; 1) Print list of events (the user will be prompted to select an events file)
  ; out=juice_event_read(/list)
  ; 
  ; 2) Print times of event 'DOWNLINK_MALARGUE_START', from mission_timeline_event_file_5_0b23_1.csv. The user will be promted to select that csv file
  ; out=juice_event_read(type_event=['DOWNLINK_MALARGUE_START'])
  ; print, out
  ;
  ; MODIFICATION HISTORY:
  ; Written, ER, October, 2022.

IF (N_ELEMENTS(dir) EQ 0) THEN dir='/Users/roussos/owncloud/mylib/juice_scripts/Input_data/DATA/' ;SPICE kernels directory

evfile=file_search(dir, '*timeline_event*.csv',count=mkcount)

PRINT, 'Event Information Files' ;print metakernel names on screen
PRINT, '-----------------------'

FOR i=0l, mkcount-1l DO BEGIN

  PRINT, STRTRIM(STRING(i),2)+'. '+ strsplit(evfile[i], dir,/extract,/regex)

ENDFOR

READ, input_key, PROMPT='Select (0-'+STRTRIM(STRING(mkcount-1l),2)+'):' ;select metakernel

ev=READ_CSV(evfile[input_key])

event_type=ev.field1
event_time_start=ev.field2

event_sort=event_type(SORT(event_type))
event_unique=event_sort(UNIQ(event_sort))

IF keyword_set(list_events) THEN BEGIN
  
  IF (N_ELEMENTS(event_unique) EQ 1) THEN PRINT, event_unique ELSE PRINT, TRANSPOSE(event_unique)
  RETURN, ''

ENDIF ELSE IF (N_ELEMENTS(type_event) NE 0) THEN BEGIN
  
  FOR i=0l, N_ELEMENTS(type_event)-1 DO BEGIN 

    tst=WHERE(STRUPCASE(event_type) EQ STRUPCASE(type_event[i]))
    IF (i EQ 0 and tst[0] NE -1) THEN out=[[event_type[tst]], [event_time_start[tst]]]
    IF (i NE 0 and tst[0] NE -1 and N_ELEMENTS(out) NE 0) THEN out=[out,[[event_type[tst]], [event_time_start[tst]]]]
    IF (i NE 0 and tst[0] NE -1 and N_ELEMENTS(out) EQ 0) THEN out=[[event_type[tst]], [event_time_start[tst]]]
    IF (i EQ N_ELEMENTS(type_event)-1 and N_ELEMENTS(out) EQ 0) THEN out=''
    
  ENDFOR
  IF (out[0] NE '') THEN out=TRANSPOSE(out)
  RETURN, out 

ENDIF ELSE BEGIN
  
  PRINT, 'No input given. All events exported'
  out=TRANSPOSE([[event_type],[event_time_start]])
  RETURN, out

  
ENDELSE

END