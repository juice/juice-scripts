FUNCTION juice_orbit_read, orbit=orbit, dir=dir

  ;+
  ; NAME:
  ; juice_orbit_read
  ;
  ; PURPOSE:
  ; Read orbit files from JUICE SPICE kernels, available in the JUICE kernel distribution
  ; in misc/orbnum/ subfolded
  ;
  ; CATEGORY:
  ; JUICE SPICE scripts
  ;
  ; CALLING SEQUENCE: 
  ; out= juice_orbit_read([orbit=orbit], [dir=dir])
  ; 
  ;
  ; INPUTS:
  ; orbit: 'all' for all orbits, or long array with the desired orbit numbers. Default is all
  ; dir: SPICE kernels directory with orbit data. You may change its default value in the beginning of the code if you dont want to always input it.
  ;
  ; OUTPUTS:
  ; 
  ; STRING array of [No of orbits, 4]
  ; Column 0: Orbit no
  ; Column 1: Start time UTC
  ; Column 2: Stop Time UTC
  ; Column 3: Periapsis Time UTC
  ; 
  ; 
  ; COMMON BLOCKS:
  ; None.
  ;
  ; SIDE EFFECTS:
  ; None.
  ;
  ; RESTRICTIONS:
  ; Requires JUICE SPICE kernels
  ;
  ; PROCEDURE:
  ; 1) Get times for orbit 43, 50, if orbit files located in '/Users/roussos/ownCloud/Work/JUICE/SPICE/misc/orbnum/'
  ; orb = juice_orbit_read(orbit=[43, 50], dir='/Users/roussos/ownCloud/Work/JUICE/SPICE/misc/orbnum/')
  ; 
  ; 2) Get times for all orbits, if orbit files located in '/Users/roussos/ownCloud/Work/JUICE/SPICE/misc/orbnum/'
  ; orb = juice_orbit_read(orbit='all', dir='/Users/roussos/ownCloud/Work/JUICE/SPICE/misc/orbnum/')
  ; 
  ; MODIFICATION HISTORY:
  ; Written, ER, October, 2022.



IF (N_ELEMENTS(dir) EQ 0) THEN dir='/Users/roussos/ownCloud/Work/JUICE/SPICE/misc/orbnum/' ;SPICE kernels directory with orbit data
IF (N_ELEMENTS(orbit) EQ 0) THEN orbit='all' ;Default setting for orbit selection

cd, dir ;change to metakernels directory
orbfile=file_search('*.orb', count=mkcount) ;find all metakernels

PRINT, 'Orbit Information Files' ;print metakernel names on screen
PRINT, '-----------------------'
FOR i=0l, mkcount-1l DO BEGIN

  PRINT, STRTRIM(STRING(i),2)+'. '+orbfile[i]

ENDFOR

READ, input_key, PROMPT='Select (0-'+STRTRIM(STRING(mkcount-1l),2)+'):' ;select metakernel

GET_LUN, lun
OPENR, lun, orbfile[input_key]

line=''
READF, lun, line
temp = [line]

WHILE (NOT EOF(lun)) DO BEGIN
  READF, lun, line
  temp = [temp, line]
ENDWHILE                     ;number of lines read (including header)

tst=STRPOS(temp, 'N/A') ;some orbit files have this entry for timing of the 1st orbit, replace it with '2031 AUG 02 02:33:58' (kernel v5)
fnd=WHERE(tst NE -1)

IF (fnd[0] NE -1) THEN BEGIN

  var1=temp[fnd[0]]
  var2=tst[fnd[0]]
  STRPUT, var1, '2031 AUG 02 02:33:58', var2
  temp[fnd[0]]=var1
  
ENDIF

FREE_LUN,lun

temp=temp[2:N_ELEMENTS(temp)-1l] ;remove the header
nl=N_ELEMENTS(temp)

utc_peri=STRARR(nl) ;orbit periapsis UTC date
utc_start=STRARR(nl) ;orbit start UTC date
utc_stop=STRARR(nl) ;orbit end date UTC date
sclk_peri=STRARR(nl) ; orbit peripasis time in sclk format

FOR i=0l, nl-1l DO BEGIN ;format the output
  
  words=STRSPLIT(temp[i],/extract)
  utc_peri[i]=words[1]+' '+words[2]+' '+words[3]+' '+words[4]
  sclk_peri[i]=words[5]
  utc_start[i]=words[6]+' '+words[7]+' '+words[8]+' '+words[9]
  utc_stop[i]=words[10]+' '+words[11]+' '+words[12]+' '+words[13]  
  
ENDFOR

orbnum=LINDGEN(nl)+1 ; orbit numbers (they start from 1)
orbnum=STRTRIM(STRING(orbnum),2); convert to string

IF (STRLOWCASE(orbit[0]) EQ 'all') THEN out=[[orbnum],[utc_start],[utc_stop],[utc_peri]] ;define the output (if all orbits selected)

IF (STRLOWCASE(orbit[0]) NE 'all') THEN BEGIN ;define the output if selected orbit numbers requested
  
  oindex=orbit-1l ;orbit numbering -1 = orbit index in array
  out=[[orbnum[oindex]],[utc_start[oindex]],[utc_stop[oindex]],[utc_peri[oindex]]]
  
ENDIF

out=TRANSPOSE(out)

PRINT, 'Return values (STRING): Orbit Number, UTC Start, UTC Stop, UTC Periapis' 
RETURN, out

END