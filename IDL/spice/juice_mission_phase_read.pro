FUNCTION juice_mission_phase_read, list_events=list_events, dir=dir, type_event=type_event

  ;+
  ; NAME:
  ; juice_mission_phase_read
  ;
  ; PURPOSE:
  ; Read mission phase files from JUICE available at: https://www.cosmos.esa.int/web/juice/shared-data
  ;
  ; CATEGORY:
  ; JUICE event scripts
  ;
  ; CALLING SEQUENCE:
  ; out=juice_mission_phase_read([type_event=type_event], [dir=dir], [list_events=list_events] )
  ;
  ; INPUTS:
  ; 
  ; type_event: STRING, defining the phase to be queried. To see what phases are available, run the script with /list_events. 
  ;             If not defined, all phases are chosen and printed on screen. Input can be both upper or lowercase
  ; list_event: If set, list of available events is printed on screen
  ; dir: STRING, directory where phase data are stored. A default value is set at the start of the script, you may modify it if you dont want to set the dir always. 
  ; 
  ; OUTPUTS:
  ; 
  ; STRING array of [No of phases, 4]
  ; Column 0: Event name
  ; Column 1: Event description
  ; Column 2: Phase start UTC
  ; Column 3: Phase stop UTC
   
  ; 
  ; COMMON BLOCKS:
  ; None.
  ;
  ; SIDE EFFECTS:
  ; None.
  ;
  ; RESTRICTIONS:
  ; None.
  ;
  ; PROCEDURE:
  ; 1)List mission phases, search for phase file in '/Users/roussos/owncloud/mylib/juice_scripts/Input_data/DATA/':
  ; out=juice_mission_phase_read(/list_events, dir='/Users/roussos/owncloud/mylib/juice_scripts/Input_data/DATA/')
  ; 
  ; 2)Obtain properties of "Jupiter_Phase_1" & "Jupiter_Phase_3". Searches phase file in the default dir.
  ; out=juice_mission_phase_read(type_event=['Jupiter_phase_1', 'Jupiter_phase_3'] )
  ;
  ; MODIFICATION HISTORY:
  ; Written, ER, October, 2022.



IF (N_ELEMENTS(dir) EQ 0) THEN dir='/Users/roussos/owncloud/mylib/juice_scripts/Input_data/DATA/' ;SPICE kernels directory

evfile=file_search(dir, '*Phases.csv',count=mkcount)

PRINT, 'Phase Information Files' ;print metakernel names on screen
PRINT, '-----------------------'

FOR i=0l, mkcount-1l DO BEGIN

  PRINT, STRTRIM(STRING(i),2)+'. '+ strsplit(evfile[i], dir,/extract,/regex)

ENDFOR
READ, input_key, PROMPT='Select (0-'+STRTRIM(STRING(mkcount-1l),2)+'):' ;select metakernel

ev=READ_CSV(evfile[input_key])

event=ev.field1
event_description=ev.field2
event_time_start=ev.field3
event_time_stop=ev.field4

IF keyword_set(list_events) THEN BEGIN
  
  out=[[event], [event_description],[event_time_start],[event_time_stop]]
  out=TRANSPOSE(out)
  print, out 
  RETURN, out

ENDIF ELSE IF (N_ELEMENTS(type_event) NE 0) THEN BEGIN
  
  FOR i=0l, N_ELEMENTS(type_event)-1 DO BEGIN

    tst=WHERE(STRUPCASE(event) EQ STRUPCASE(type_event[i]))
    IF (i EQ 0 and tst[0] NE -1) THEN out=[[event[tst]], [event_description[tst]],[event_time_start[tst]],[event_time_stop[tst]]]
    IF (i NE 0 and tst[0] NE -1 and N_ELEMENTS(out) NE 0) THEN out=[out,[[event[tst]], [event_description[tst]],[event_time_start[tst]],[event_time_stop[tst]]]]
    IF (i NE 0 and tst[0] NE -1 and N_ELEMENTS(out) EQ 0) THEN out=[[event[tst]], [event_description[tst]],[event_time_start[tst]],[event_time_stop[tst]]]
    IF (i EQ N_ELEMENTS(type_event)-1 and N_ELEMENTS(out) EQ 0) THEN out=''

  ENDFOR
  
  IF (out[0] NE '') THEN out=TRANSPOSE(out)
  RETURN, out


ENDIF ELSE BEGIN
  
  PRINT, 'No input given. All phases exported'
  out=[[event], [event_description],[event_time_start],[event_time_stop]]
  out=TRANSPOSE(out)
  RETURN, out
  
ENDELSE

END
