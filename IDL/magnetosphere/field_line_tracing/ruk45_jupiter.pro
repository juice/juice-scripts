PRO ruk45_jupiter, ds, order, f, max_ds, errtol=errtol

  ;+
  ; NAME:
  ; Runge-Kutta-Fehlberg method (denoted RKF45)  
  ; err45 is the parameter checked for the rk=45 to be run
  ; and is the relative error between RK4 & RK5 in the RK45 method
  ; it is compared to the errtol[0] & errtol[1]
  ; if MIN(err45)<errtol[0] OR MAX(err45)>errtol[1] then the time step is adjusted accordingly
  ; if both conditions are violated, we set for this specific time step errtol[0]=MIN(err45) so that the calculations proceed
  ; it is assumed that errtol[0]<errtol[1], if not it is corrected at the start of the program in the main routine
  ;
  ; PURPOSE:
  ; 
  ;
  ; CATEGORY:
  ; Field line tracing
  ;
  ; CALLING SEQUENCE:
  ; 
  ;
  ; INPUTS:
  ; ds: ;integration step along field in Rj
  ; f: intial location in xyz cartesian [Rj, Rj, Rj] in IAU Jupiter
  ; order: Jovian field expansion order, options: '', 'vit4', 'vip4_4', 'vipal_5', 'o6_3', 'isaac_10', 'jrm09_10', 'jrm33_13','jrm33_18'. 
  ;        Default: jrm09_10
  ;
  ; OUTPUTS:
  ; Input position "f" is updated after integration
  ;
  ; COMMON BLOCKS:
  ; None.
  ;
  ; SIDE EFFECTS/ISSUES:
  ; ;The routine can enter in infinite cycles trying to find a ds step for which integration error is within errtol. For that reason there are certain limits forcing an exit:
  ; ;1) If during an integration the filed line distance exceeds 100 R_planet, tracing is stopped, routine returns a NaN
  ; ;2) If ds<0.025, tracing is kept with the previous ds>=0.025, since for such a small step the precision of the solution is sufficient
  ; ;3) If the call requires more than 10 iterations to adjust ds, then routine returns a NaN
  ;
  ; RESTRICTIONS:
  ; Requires Jupiter field models
  ;
  ; PROCEDURE:
  ; 
  ;
  ; MODIFICATION HISTORY:
  ; Written, ER, October, 2022.
  
CASE order OF
    'vit4': imodel = 'vit4_order04'
    'vip4_4': imodel  = 'vip4_order04'
    'vipal_5': imodel = 'vipal_order05'
    'o6_3': imodel = 'o6_order03'
    'isaac_10': imodel = 'isaac_order10'
    'jrm09_10': imodel = 'jrm09_order10'
    'jrm33_13': imodel = 'jrm33_order13'
    'jrm33_18': imodel = 'jrm33_order18' 
    ELSE: BEGIN
      order =  'jrm09_10'
      imodel = 'jrm09_order10'
    END
ENDCASE

err45=2d*errtol[1] ;set an artificial value of large error to start the loop below

rval=SQRT(TOTAL(f*f)) ;radial distance
IF (ABS(ds) LT 0.025) THEN ds=SIGN(ds)*0.025 ; this is a minimum integration step. Based on experience this is more than sufficient for tracing field lines with high precision, 
                               ;no need to go to lower values
                               
ds_change=0                               

WHILE (rval*0 EQ 0 AND (err45 GT errtol[1] or err45 LT errtol[0]) AND ABS(ds) LE max_ds AND ABS(ds) GE 0.025 and ds_change LE 10) DO BEGIN ;make sure to compare only not NaN values

  dummy= execute('vec=jovian_'+imodel+'_internal_xyz(f[0], f[1], f[2]) +con2020_model_xyz(''analytic'', f[0], f[1], f[2])')
  btot=sqrt(TOTAL(vec*vec))
  kr1=vec/btot
  df1= f + ds*kr1*0.25

  dummy= execute('vec=jovian_'+imodel+'_internal_xyz(df1[0], df1[1], df1[2]) +con2020_model_xyz(''analytic'', df1[0], df1[1], df1[2])')
  btot=sqrt(TOTAL(vec*vec))
  kr2=vec/btot
  df2 = f + ds*(3.0/32.0*kr1 + 9.0/32.0*kr2)

  dummy= execute('vec=jovian_'+imodel+'_internal_xyz(df2[0], df2[1], df2[2]) +con2020_model_xyz(''analytic'', df2[0], df2[1], df2[2])')
  btot=sqrt(TOTAL(vec*vec))
  kr3=vec/btot
  df3 = f + ds*(1932.0/2197.0*kr1 - 7200.0/2197.0*kr2 + 7296.0/2197.0*kr3)

  dummy= execute('vec=jovian_'+imodel+'_internal_xyz(df3[0], df3[1], df3[2]) +con2020_model_xyz(''analytic'', df3[0], df3[1], df3[2])')
  btot=sqrt(TOTAL(vec*vec))
  kr4=vec/btot  
  df4 = f + ds*(439.0/216.0*kr1 - 8.0*kr2 + 3680.0/513.0*kr3 - 845.0/4104.0*kr4)

  dummy= execute('vec=jovian_'+imodel+'_internal_xyz(df4[0], df4[1], df4[2]) +con2020_model_xyz(''analytic'', df4[0], df4[1], df4[2])')
  btot=sqrt(TOTAL(vec*vec))
  kr5=vec/btot  
  df5=f + ds*(-8.0/27.0*kr1 + 2.0*kr2 - 3544.0/2565.0*kr3 + 1859.0/4104.0*kr4 - 11.0/40.0*kr5)

  dummy= execute('vec=jovian_'+imodel+'_internal_xyz(df5[0], df5[1], df5[2]) +con2020_model_xyz(''analytic'', df5[0], df5[1], df5[2])')
  btot=sqrt(TOTAL(vec*vec))
  kr6=vec/btot

  ;get the two solutions f1--> RK5 , f2--> RK4
  f1 = f + ds*(16.0/135.0*kr1 + 6656.0/12825.0*kr3 + 28561.0/56430.0*kr4 - 9.0/50.0*kr5 + 2.0/55.0*kr6)
  f2 = f + ds*(25.0/216.0*kr1 + 1408.0/2565.0*kr3 + 2197.0/4101.0*kr4 - 1.0/5.0d*kr5)
  
  rval=SQRT(TOTAL(f1*f1)) ;test range of radial distance
  
  IF (rval GT 100.0) THEN BEGIN 
    
    f1=FLTARR(3)+!Values.F_NaN ;NAN values if the field line reaches to very large distances
    rval=!Values.F_NaN ;set to this value to exit the loop
    ;this will cause an exit from the while loop and assignement to NaN values for the result
    
  ENDIF ELSE BEGIN 

    temp=SQRT(TOTAL((f1-f2)*(f1-f2)))/sqrt(TOTAL(f1*f1))
  
    ;if conditions are violated, changes in time step below will be effected in the next calculation
  
    IF (temp GT errtol[1] AND ds*0.0 EQ 0.0) THEN BEGIN ;MAX condition violated
  
      ds = 0.75*ds
      ds_change=ds_change+1

    ENDIF ELSE IF (temp LT errtol[0] AND ds*0.0 EQ 0.0) THEN BEGIN ;only MIN condition violated
  
      ds = 1.25*ds
      ds_change=ds_change+1

    ENDIF ELSE IF (ds*0.0 NE 0.0) THEN BEGIN
      
      ds = max_ds/3.0
      ds_change=ds_change+1
      
    ENDIF
  
    ;update the MAX/MIN values of error so that the WHILE loop conditions can be evaluated for this time step
    err45=temp
   ;print, temp, errtol
    
  ENDELSE
  
ENDWHILE

;if the while loop exits, conditions have been satisfied, then set the solution to that of RK5 (f1) and advance the time step
;return also the error value as the maximum among all particles

IF (ds_change GT 10) THEN f1=FLTARR(3)+!Values.F_NaN 
f = f1

END
