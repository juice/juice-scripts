PRO ruk4_jupiter, ds, order, f

  ;+
  ; NAME:
  ; ruk4_jupiter
  ;
  ; PURPOSE:
  ; Runge Kutta 4th order, fixed step integrator for Jupiter's field line tracing in cartesian coords
  ;
  ; CATEGORY:
  ; Field line tracing
  ;
  ; CALLING SEQUENCE:
  ; ruk4_jupiter, ds, order, f
  ;
  ; INPUTS:
  ; ds: ;integration step along field in Rj
  ; f: intial location in xyz cartesian [Rj, Rj, Rj] in IAU Jupiter
  ; order: Jovian field expansion order, options: '', 'vit4', 'vip4_4', 'vipal_5', 'o6_3', 'isaac_10', 'jrm09_10', 'jrm33_13','jrm33_18'. Note that ''-->'jrm09_10'. 
  ;        Default: jrm09_10
  ;
  ; OUTPUTS:
  ; Input position "f" is updated after integration
  ;
  ; COMMON BLOCKS:
  ; None.
  ;
  ; SIDE EFFECTS/ISSUES:
  ;
  ; RESTRICTIONS:
  ; Requires Jupiter field models
  ;
  ; PROCEDURE:
  ; Example: Trace along the field, starting from IAU_JUPITER position [5d, 2d, 0d] Rj, step of 0.1 Rj, with Jupiter model jrm33_13
;   xyz=[5d, 2d, 0d]
;   ds=0.1d
;   ruk4_jupiter, ds, 'jrm33_13', xyz
;   print, xyz
  ;
  ; MODIFICATION HISTORY:
  ; Written, ER, October, 2022.
  
CASE order OF
    'vit4': imodel = 'vit4_order04'
    'vip4_4': imodel  = 'vip4_order04'
    'vipal_5': imodel = 'vipal_order05'
    'o6_3': imodel = 'o6_order03'
    'isaac_10': imodel = 'isaac_order10'
    'jrm09_10': imodel = 'jrm09_order10'
    'jrm33_13': imodel = 'jrm33_order13'
    'jrm33_18': imodel = 'jrm33_order18' 
    ELSE: BEGIN
      order =  'jrm09_10'
      imodel = 'jrm09_order10'
    END
ENDCASE

ds1=0.5*ds

rval=SQRT(TOTAL(f*f)) ;radial distance
IF (rval LE 100 and rval*0.0 EQ 0.0) THEN BEGIN
  
  dummy= execute('vec=jovian_'+imodel+'_internal_xyz(f[0], f[1], f[2]) +con2020_model_xyz(''analytic'', f[0], f[1], f[2])')
    
ENDIF ELSE BEGIN
  
  vec=FLTARR(3)+!Values.F_NaN
  
ENDELSE

btot=sqrt(TOTAL(vec*vec))
v1= vec/btot
f1= f +v1*ds1

rval=SQRT(TOTAL(f1*f1)) ;radial distance
IF (rval LE 100 and rval*0.0 EQ 0.0) THEN BEGIN
  
  dummy= execute('vec=jovian_'+imodel+'_internal_xyz(f1[0], f1[1], f1[2]) +con2020_model_xyz(''analytic'', f1[0], f1[1], f1[2])')
    
ENDIF ELSE BEGIN
  
  vec=FLTARR(3)+!Values.F_NaN
  
ENDELSE

btot=sqrt(TOTAL(vec*vec))
v2= vec/btot
f2= f +v2*ds1

rval=SQRT(TOTAL(f2*f2)) ;radial distance
IF (rval LE 100 and rval*0.0 EQ 0.0) THEN BEGIN
  
  dummy= execute('vec=jovian_'+imodel+'_internal_xyz(f2[0], f2[1], f2[2]) +con2020_model_xyz(''analytic'', f2[0], f2[1], f2[2])')
    
ENDIF ELSE BEGIN
  
  vec=FLTARR(3)+!Values.F_NaN
  
ENDELSE

btot=sqrt(TOTAL(vec*vec))
v3= vec/btot
f3= f +v3*ds

rval=SQRT(TOTAL(f3*f3)) ;radial distance
IF (rval LE 100 and rval*0.0 EQ 0.0) THEN BEGIN
  
  dummy= execute('vec=jovian_'+imodel+'_internal_xyz(f3[0], f3[1], f3[2]) +con2020_model_xyz(''analytic'', f3[0], f3[1], f3[2])')
  
    
ENDIF ELSE BEGIN
  
  vec=FLTARR(3)+!Values.F_NaN
  
ENDELSE

btot=sqrt(TOTAL(vec*vec))
v4= vec/btot

f=f+ ds*(v1/6.0 +v2/3.0 +v3/3.0 +v4/6.0)

END
