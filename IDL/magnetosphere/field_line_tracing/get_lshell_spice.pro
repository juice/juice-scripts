PRO get_lshell_spice, $
target=target,$
trange=trange,$
step=step,$
order=order,$
ds=ds,$
rlimit=rlimit,$
integrator=integrator,$
errtol=errtol,$
max_ds=max_ds,$
dir=dir,$
out_dir=out_dir 

;+
; NAME:
; get_lshell_spice
;
; PURPOSE:
; 
;
; CATEGORY:
; Field line tracing
;
; CALLING SEQUENCE:
; 
;
; INPUTS:
; ds: integration step along field in Rj
; target: target SPICE object to calculate field line traces for. Default is JUICE. It can also be a moon
; trange: time range from which to obtain trajectory information of the target. Default is ['2031-06-30T23:00:00.000','2035-09-30T22:00:00.000']
; step: time step of trajectory information. Default is 900
; rlimit: distance limit in Rj to which to stop tracing a field line, default=80 Rj
; dir: location of SPICE kernels directory. Default can be changed directly in the script
; out_dir: directory to store the output file. . Default can be changed directly in the script.
; order: Jovian field expansion order, options: '', 'vit4', 'vip4_4', 'vipal_5', 'o6_3', 'isaac_10', 'jrm09_10', 'jrm33_13','jrm33_18'. Note that ''-->'jrm09_10' with older code. 
;        Default: jrm09_10
; integrator: field line integration method, options 'ruk4' (runge kutta 4th order),  'euler' (euler), 'ruk45' (runge  kutta 5th order with adaptive step). Default is ruk4. 
;             ruk45 corresponds to the Runge-Kutta-Fehlberg method (sometimes denoted RKF45)
; errtol: Error tolerance if integrator='ruk45'. The integator calculates a tracing step with 4th and 5th order RK, compares results and if the error is within minmax(errtol)
;         then it continues integration with the same integration step ds. If err<min(errtol) the ds-->1.25*ds, if err>max(errtol) then ds-->0.75*ds. If increasing ds exceeds max_ds, the integrator
;         automatically reverts to 'ruk4' with the ds achieved at the last previous step.
;         Error calculation: If f1--> RK5 and f2--> RK4, the err = (distance difference between solutions 1 & 2)/distance_1
;         Default: errtol=[1e-6,7.5e-6]. 
; max_ds: maximum field line integration step. Default value is 0.5 Rs
;
; OUTPUTS:
; Creates a savefile with filename 'Tracing_'+target+'_'+metakernel[input_key]+'_'+order+'_'+trange[0]+'_'+trange[1]+'.sav' in the out_dir
; but with the time giving only the Year/Day info, not the hour/minute/second precision (this is stored in the file).
; 
; The savefile contains the variables ms, beq, bloc, et_time_ms, lon_north, lon_south, lat_north, lat_south, tstep, order, trange, ds, rlimit:
; ms-->M-shell array, magnetically mapped radial distance from Jupiter of magnetic equator (minimum B-magnitude along the field line), in Rj. 
;      Equivalent to L-shell for cylindrically symmetric fields
; beq: Equatorial magnetic field magnitude array in nT
; bloc: Local magnetic field magnitude array at the input locations of the time series, in nT
; lon_north: Mapped S3 East RH longitude array of the traced field lines, to the north hemisphere of Jupiter, in deg (0-->360)
; lon_south: Mapped S3 East RH longitude array of the traced field lines, to the south hemisphere of Jupiter, in deg (0-->360)
; lat_north: Mapped S3 latitude array of the traced field lines, to the north hemisphere of Jupiter, in deg
; lat_south: Mapped S3 latitude array of the traced field lines, to the north hemisphere of Jupiter, in deg
; tstep, order, trange, ds, rlimit: input parameters to indicate to the user the calculation assumptions.

; COMMON BLOCKS:
; None.
;
; SIDE EFFECTS/ISSUES:
;
;
; RESTRICTIONS:
; Requires JUICE SPICE kernels, trace_jupiter_l and dependencies

;
; PROCEDURE:
; Examples
; 1) Get JUICE tracing for period '2032-09-25T23:00:00.000' to '2032-09-30T22:00:00.000', limit tracings to R<70 Rj, use time step of 900 sec, use default field model jrm09_10
; get_lshell_spice, trange=['2032-09-25T23:00:00.000','2032-09-30T22:00:00.000'], step=900d, rlimit=70d 
; If you select metakernel juice_plan.tm at the prompt then
; results are saved in the out_dir as Tracing_JUICE_juice_plan.tm_v410_jrm09_10_2032-09-25T23:00:00.000_2032-09-30T22:00:00.000.sav (since v410 of the kernels was used)

; 2) Same as above but for target body: europa
; get_lshell_spice, trange=['2032-09-25T23:00:00.000','2032-09-30T22:00:00.000'], step=900d, rlimit=70d, target='Europa'
; If you select metakernel juice_plan.tm at the prompt then
; Results are saved in the out_dir as Tracing_EUROPA_juice_plan.tm_v410_jrm09_10_2032-09-25T23:00:00.000_2032-09-30T22:00:00.000.sav (since v410 of the kernels was used)

; MODIFICATION HISTORY:
; Written, ER, October, 2022.
; 

;Define some default values
IF (N_ELEMENTS(dir) EQ 0) THEN dir='/Users/roussos/ownCloud/Work/JUICE/SPICE/kernels/' ;SPICE kernels directory
IF (N_ELEMENTS(out_dir) EQ 0) THEN out_dir='/Users/roussos/'
IF (N_ELEMENTS(step) EQ 0) THEN step=900d ;time step for ephemeris in seconds
IF (N_ELEMENTS(trange) EQ 0) THEN trange=['2031-06-30T23:00:00.000','2035-09-30T22:00:00.000']
IF (N_ELEMENTS(order) EQ 0) THEN order='jrm09_10'
IF (N_ELEMENTS(rlimit) EQ 0) THEN rlimit=80.0
IF (N_ELEMENTS(ds) EQ 0) THEN ds=0.2 ;0default 0.2 Rj tracing step
ds_init=ds
IF (N_ELEMENTS(max_ds) EQ 0) THEN max_ds=0.5 ;0default 0.1 Rj tracing step
IF (N_ELEMENTS(target) EQ 0) THEN target='JUICE'
IF (N_ELEMENTS(errtol) EQ 0) THEN errtol=[1e-6,2.0e-5]

rj=71492d ;jupiter radius

dirmk = dir + 'mk/' ;metakernels directory
;dirmain=strsplit(dir, 'kernels/', /regex, /extract)
;GET_LUN, lun
;OPENR, lun, dirmain+'version'
;kernelversion=''
;READF, lun, kernelversion
;FREE_LUN,lun

cd, dirmk ;change to metakernels directory

metakernel=file_search('*.tm', count=mkcount) ;find all metakernels

PRINT, 'SPICE metakernels' ;print metakernel names on screen
PRINT, '-----------------------'

FOR i=0l, mkcount-1l DO PRINT, STRTRIM(STRING(i),2)+'. '+metakernel[i]

READ, input_key, PROMPT='Select (0-'+STRTRIM(STRING(mkcount-1l),2)+'):' ;select metakernel

cspice_furnsh, dirmk + metakernel[input_key] ;load selected metakernel

;below get the JUICE position in the desired coordinate system (sysval) in unit=1.0 km, stored
;in the xyzt array
;xyzt[0:2]--> x, y, z position
;xyzt[6]--> ET time (seconds)
sysval='IAU_JUPITER'
xyzt=get_ephem_juice_generic(date=trange, $
  step=step, sys=sysval, center='JUPITER', target=target, unit=rj)

et_time_ms=xyzt[*,6] ;assign a variable to the ET-time for simplicity

xmag=xyzt[*,0]
ymag=xyzt[*,1]
zmag=xyzt[*,2]
rval=SQRT(xmag*xmag + ymag*ymag + zmag*zmag)
colat=ACOS(zmag/rval) ;this is in the 0-->pi range, as required by SPICE
plat=ASIN(zmag/rval)
s3lone=ATAN(ymag,xmag)
test=WHERE(s3lone LT 0)
IF (test[0] NE -1) THEN s3lone[test]=2*!PI +s3lone[test] ;convert to 0-->2pi
nl=N_ELEMENTS(xmag)

FOR i=0l, nl-1l DO BEGIN

    ds=ds_init
    loc=[xmag[i], ymag[i], zmag[i]]
    tr=trace_jupiter_l(location=loc, ds=ds, rlimit=rlimit, order=order, integrator=integrator, errtol=errtol, max_ds=max_ds)
    
    IF (i EQ 0) THEN ms=tr[0] ELSE ms=[ms,tr[0]]
    IF (i EQ 0) THEN beq=tr[1] ELSE beq=[beq,tr[1]]
    IF (i EQ 0) THEN bloc=tr[2] ELSE bloc=[bloc,tr[2]]
    IF (i EQ 0) THEN lon_north=tr[3] ELSE lon_north=[lon_north,tr[3]]
    IF (i EQ 0) THEN lat_north=tr[5] ELSE lat_north=[lat_north,tr[5]]
    IF (i EQ 0) THEN lon_south=tr[4] ELSE lon_south=[lon_south,tr[4]]
    IF (i EQ 0) THEN lat_south=tr[6] ELSE lat_south=[lat_south,tr[6]]
    Print, format='(a,a,f6.2,a,$)','      ', string("15B), (i+1)/float(nl)*100, '% complete' 

ENDFOR

find_t=STRPOS(trange[0], 'T')

IF (find_t[0] NE -1) THEN BEGIN
  
  test=STRSPLIT(trange,'T')
  trange_lim=trange
  trange_lim[0]=STRMID(trange[0],0,test[0,1]-1l)
  trange_lim[1]=STRMID(trange[1],0,test[1,1]-1l) 
  
ENDIF ELSE BEGIN
  
  trange_lim=trange
  trange_lim[0]=STRMID(trange[0],0,10)
  trange_lim[1]=STRMID(trange[1],0,10)
  
ENDELSE


filename='Tracing_'+target+'_'+metakernel[input_key]+'_'+order+'_'+trange_lim[0]+'_'+trange_lim[1]+'.sav'
save, filename=out_dir+filename, ms, beq, bloc, et_time_ms, lon_north, lon_south, lat_north, lat_south, step, order, trange, ds_init, rlimit, integrator, errtol, max_ds


cspice_kclear ;unload spice kernels

END