# IDL Field Line Tracing scripts

**Script 1 line descriptions**

- **trace_lshell_jupiter.pro**: Trace a field line starting from a single location, retrieve L-shell, mapped footpoints on planet etc.
- **ruk4_jupiter.pro**: Runge-Kutta 4th order (constant step) for field line tracing, used in trace_lshell_jupiter.pro
- **ruk45_jupiter.pro**: Runge-Kutta-Fehlberg 5th order method (adaptive step) for field line tracing, used in trace_lshell_jupiter.pro
- **euler_jupiter.pro**:Euler integration (constant step) for field line tracing, used in trace_lshell_jupiter.pro. Fastest but less accurate
- **get_lshell_spice.pro**: Apply trace_lshell_jupiter.pro to multiple locations obtained from JUICE SPICE kernels. Can be applied to JUICE or the moons
