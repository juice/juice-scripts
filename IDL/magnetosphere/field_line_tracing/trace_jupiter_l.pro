FUNCTION trace_jupiter_l,$
ds=ds,$
location=location,$ 
rlimit=rlimit,$ 
order=order,$
spherical=spherical,$
integrator=integrator,$
errtol=errtol,$
max_ds=max_ds,$
full_output=full_output 

;+
; NAME:
; trace_jupiter_l
;
; PURPOSE:
; Trace a field line using a starting input position
;
; CATEGORY:
; Field line tracing
;
; CALLING SEQUENCE:
; out=trace_jupiter_l([ds=ds],location=location,[rlimit=rlimit],[integrator=integrator])
;
; INPUTS:
; ds: ;integration step along field in Rj
; location: intial location in xyz cartesian [Rj, Rj, Rj] or (if order='' OR /spherical is set) 
;           Spherical coordinates [radial distance, latitude, east longitude] in [Rj, deg, deg], in IAU Jupiter
; rlimit: distance limit in Rj to which to stop tracing a field line, default=70 Rj
; order: Jovian field expansion order, options: '', 'vit4', 'vip4_4', 'vipal_5', 'o6_3', 'isaac_10', 'jrm09_10', 'jrm33_13','jrm33_18'. Note that ''-->'jrm09_10'. Default: jrm09_10
; integrator: field line integration method, options 'ruk4' (runge kutta 4th order),  'euler' (euler), 'ruk45' (runge  kutta 5th order with adaptive step). Default is ruk4. 
;             ruk45 corresponds to the Runge-Kutta-Fehlberg method (sometimes denoted RKF45)
; errtol: Error tolerance if integrator='ruk45'. The integator calculates a tracing step with 4th and 5th order RK, compares results and if the error is within minmax(errtol)
;         then it continues integration with the same integration step ds. If err<min(errtol) the ds-->1.25*ds, if err>max(errtol) then ds-->0.75*ds. If increasing ds exceeds max_ds, the integrator
;         automatically reverts to 'ruk4' with the ds achieved at the last previous step.
;         Error calculation: If f1--> RK5 and f2--> RK4, the err = (distance difference between solutions 1 & 2)/distance_1
;         Default: errtol=[1e-6,7.5e-6].      
; max_ds: maximum field line integration step. Default value is 0.5 Rs
; full_output: if set, the routine returns full information on the field line tracing
; 
; OUTPUTS:
; 7-element array: 
; M-shell: magnetically mapped radial distance from Jupiter of magnetic equator (minimum B-magnitude along the field line or max field line distance), in Rj. 
;          Equivalent to L-shell for cylindrically symmetric fields
; Beq: Equatorial magnetic field magnitude in nT
; Bloc: Local magnetic field magnitude at the input location, in nT
; s3_north: Mapped S3 East RH longitude of the traced field line, to the north hemisphere of Jupiter, in deg (0-->360)
; s3_south: Mapped S3 East RH longitude of the traced field line, to the south hemisphere of Jupiter, in deg (0-->360)
; lat_north: Mapped S3 latitude of the traced field line, to the north hemisphere of Jupiter, in deg
; lat_south: Mapped S3 latitude of the traced field line, to the north hemisphere of Jupiter, in deg
;
; 
; If full_output keyword is set then the output is an IDL list containing:
; a) The 7-element array outlined above
; b) integrator used ("integrator" parameter)
; c) field model ("order" parameter)
; d) integration step array ("dsval" parameter, will contain different values for each step than the original set "ds" if ruk45 integrator is used)
; e) b magnitude along the field line positions ("bval" parameter)
; f) field line coordinates (3 dimensional field line position, ordered / "pos" parameter)
; g) location: original position for trace start
; h) spherical: definition if original position was given in spherical coords

; COMMON BLOCKS:
; None.
;
; SIDE EFFECTS/ISSUES:
; With the available field models, result not reliable for large distances (R>40-50 Rj at the equator).
; Field models dont include magnetopause/tail effects etc, just internal field + current sheet.
; L-shell estimate is more like a traced location of a magnetic field line at the equator, not the actual definition coming from the 3rd invariant. Use it carefully. 
;
; RESTRICTIONS:
; Requires Jupiter field models, ruk4_jupiter, ruk45_jupiter, euler_jupiter
;
; PROCEDURE:
; Results of examples (1), (2) below should be the same. 
; 
; 1) Trace field line with integration step of 0.1, starting from IAU_Jupiter x=5 Rj, y=2 Rj z=0 Rj, using the model jrm09_10
; out=trace_jupiter_l(ds=0.1, location=[5d, 2d, 0d], order='jrm09_10')
; 
; 2) Same as above but give the location input in spherical coordinates
; rdist=SQRT(total([5d, 2d, 0d]^2.0))
; s3long=ATAN(2d, 5d)*!radeg
; lat=ASIN(0d/rdist)
; out=trace_jupiter_l(ds=0.1, location=[rdist, lat, s3long], order='jrm09_10', /spherical)
; 
; 3) Same as (1), using the model jrm33_18, integrator--> ruk45
; out=trace_jupiter_l(ds=0.1, location=[5d, 2d, 0d], order='jrm33_18', integrator='ruk45')
;
; 4) Same as above, but export the full tracing output:
; out=trace_jupiter_l(ds=0.1, location=[5d, 2d, 0d], order='jrm33_18', integrator='ruk45', /full_output)
; Then the result will be:
; out[0]--> 7-element array of M-Shell, Beq ...
; out[1]--> 'ruk45'
; out[2]--> 'jrm33_18'
; out[3]--> array of integration step values used for estimating each field line position provided in out[5,*] 
; out[4]--> array of |B| at each field line position provided in out[5,*]
; out[5]--> field line cartesian coordinates, [3,n] array, where n is the number of integration steps
; out[6]--> initial tracing point
; out[7]--> spherical parameter, if one, initial position in out[6,*] was given in spherical coords.

; MODIFICATION HISTORY:
; Written, ER, October, 2022.

;some default values
rj=71492.0 ;jupiter radius
IF (N_ELEMENTS(ds) EQ 0) THEN ds=0.1 ;0default 0.1 Rj tracing step
ds_init=ds
IF (N_ELEMENTS(max_ds) EQ 0) THEN max_ds=0.5d ;0default 0.1 Rj tracing step
IF (N_ELEMENTS(rlimit) EQ 0) THEN rlimit=70.0 ;70 Rj is far enough for most juice applications
IF (N_ELEMENTS(integrator) EQ 0) THEN integrator='ruk4'
integrator=STRLOWCASE(integrator); make sure input is lowercase
IF (N_ELEMENTS(order) EQ 0) THEN order='jrm09_10' ;
IF (order EQ '') THEN order='jrm09_10' ;
order=STRLOWCASE(order); make sure input of order is in lowercase
IF (N_ELEMENTS(errtol) EQ 0) THEN errtol=[1e-6,2.0e-5]

;'vit4', 'vip4_4', 'vipal_5', 'o6_3', 'isaac_10', 'jrm09_10', 'jrm33_13','jrm33_18'
CASE order OF
    'vit4': imodel = 'vit4_order04'
    'vip4_4': imodel  = 'vip4_order04'
    'vipal_5': imodel = 'vipal_order05'
    'o6_3': imodel = 'o6_order03'
    'isaac_10': imodel = 'isaac_order10'
    'jrm09_10': imodel = 'jrm09_order10'
    'jrm33_13': imodel = 'jrm33_order13'
    'jrm33_18': imodel = 'jrm33_order18' 
    ELSE: BEGIN
      order =  'jrm09_10'
      imodel = 'jrm09_order10'
    END
ENDCASE

IF keyword_set(spherical) THEN BEGIN
  
  location_rtp_mag=[location[0],(90.0 - location[1])*!dtor, location[2]*!dtor]; input for certain magnetic field models [radial in Rj, colatitude in rad, east longitude in rad]
  location_rtp_cv=[location[2]*!dtor,location[1]*!dtor, location[0]]; below we transform this input to cartesian, cv_coord get input as [
  ;[east longitude in rad, latitude in rad, radial distance in Rj]
  location_xyz=cv_coord(from_sphere=location_rtp_cv, /to_rect) ;to cartesian
  r=location_rtp_mag[0] ;initial r value to track in the follow-up WHILE loop
  
ENDIF ELSE BEGIN
  
  location_xyz=location
  r=SQRT(TOTAL(location_xyz*location_xyz)) ;initial r value to track in the follow-up WHILE loop
  
ENDELSE

;trace_field_line ds>0, some initial conditions
i=0l ;counter for steps in the WHILE loop
rlim1=0.0 ;define some parameters that indicate the field line tracing is invalid
rlim2=0.0

IF (r GE rlimit OR r LT 1.0) THEN BEGIN ;condition: dont start integration if r<1 or r>rlimit

  rlim1=-1E38 ;in that case set these values
  rlim2=-1E38
  r=0.999 ; this value will stop immediatelly the follow-up while loop (r<1)

ENDIF ELSE BEGIN ;if initial values ok, get a first estimate of the b-vector and btotal
  
  dummy= execute('Bin=jovian_'+imodel+'_internal_xyz(location_xyz[0], location_xyz[1], location_xyz[2]) +con2020_model_xyz(''analytic'', location_xyz[0], location_xyz[1], location_xyz[2])')
 
  btot=SQRT(TOTAL(bin*bin))
  
ENDELSE

WHILE (r GE 1.0) DO BEGIN ;start the integration while loop (until r<1)
  
  IF (i EQ 0) THEN BEGIN
    
    position_temp=location_xyz  ;store position of field line in cartesian, for every integration step
    b_temp=btot
    
  ENDIF ELSE BEGIN
    
    position_temp=[[position_temp],[location_xyz]]
    b_temp=[b_temp,btot] ;store btotal for every integration step

  ENDELSE 

  IF (integrator EQ 'ruk45' AND ABS(ds) LE max_ds) THEN BEGIN
    
    ruk45_jupiter, ds, order, location_xyz, max_ds, errtol=errtol ; apply runge kutta tracing 4/5 with adaptive step, if step =< max_ds
    
  ENDIF ELSE IF (integrator EQ 'ruk4' OR ABS(ds) GT max_ds) THEN BEGIN
    
    ruk4_jupiter, ds, order, location_xyz ; apply runge kutta tracing with constant step if integrator set to 'ruk4' or step > max_ds
    
  ENDIF ELSE IF (integrator EQ 'euler' AND ABS(ds) LE max_ds) THEN BEGIN
    
    euler_jupiter, ds, order, location_xyz ; apply euler tracing with constant step if integrator set to 'ruk4' AND step =< max_ds
    
  ENDIF
  
  IF (i EQ 0) THEN ds_temp=ds ELSE ds_temp=[ds_temp,ds]
    
  r=SQRT(TOTAL(location_xyz*location_xyz)) ;get new radial position of field line
  
  IF (r*0.0 NE 0.0) THEN BEGIN ;test that r is a numeric value (not NaN or Inf), if not set appropriate values to terminate the loop and indicate invalid tracing
    
    r=0.999
    rlim1=-1E38
    
  ENDIF
  
  IF (r GE 1.0) THEN BEGIN ;make this step to get new bvector at the traced location. This is in a condition, because routine will crash if r<1 or lon/lat non-numeric. 
                           ;While condition tested at endwhile, not before
    
    dummy= execute('btot_vec=jovian_'+imodel+'_internal_xyz(location_xyz[0], location_xyz[1], location_xyz[2]) +con2020_model_xyz(''analytic'', location_xyz[0], location_xyz[1], location_xyz[2])')

    btot=SQRT(TOTAL(btot_vec*btot_vec))
    i=i+1l
    
  ENDIF

ENDWHILE

lat_ds1=ASIN(location_xyz[2]/r)*!radeg ;same as above, for latitude in IAU_JUPITER
s3_ds1=ATAN(location_xyz[1], location_xyz[0])*!radeg
test=WHERE(s3_ds1 LT 0)
IF (test[0] NE -1) THEN s3_ds1[test]=2.0*!PI +s3_ds1[test] ;convert to 0-->2pi
IF (s3_ds1 GE 2.0*!PI) THEN s3_ds1=0.0 ;sometimes due to decimal accuracy, s3_ds1=0.0 is estimated slightly above 2*pi, avoid this.

;now a field line has been traced from the starting location to the one side of the planet (if ds>0 it is traced to the south, if ds<0 to the north)
;trace now to the opposite hemisphere from the same initial starting point
;procedure above is repeated, but it will be for ds=-ds

IF keyword_set(spherical) THEN BEGIN

  location_rtp_mag=[location[0],(90.0 - location[1])*!dtor, location[2]*!dtor]; input for certain magnetic field models [radial in Rj, colatitude in rad, east longitude in rad]
  location_rtp_cv=[location[2]*!dtor,location[1]*!dtor, location[0]]; below we transform this input to cartesian, cv_coord get input as [
  ;[east longitude in rad, latitude in rad, radial distance in Rj]
  location_xyz=cv_coord(from_sphere=location_rtp_cv, /to_rect) ;to cartesian
  r=location_rtp_mag[0] ;initial r value to track in the follow-up WHILE loop
  
ENDIF ELSE BEGIN

  location_xyz=location
  r=SQRT(TOTAL(location_xyz*location_xyz)) ;initial r value to track in the follow-up WHILE loop

ENDELSE

IF (r GE rlimit OR r LE 1.0) THEN BEGIN

  rlim1=-1E38
  rlim2=-1e38
  r=0.999

ENDIF ELSE BEGIN

  dummy= execute('Bin=jovian_'+imodel+'_internal_xyz(location_xyz[0], location_xyz[1], location_xyz[2]) +con2020_model_xyz(''analytic'', location_xyz[0], location_xyz[1], location_xyz[2])')
  
  btot=SQRT(TOTAL(Bin*Bin))
  
ENDELSE

;trace_field_line ds=-ds
ds=ds_init
ds=-ds
i=0l

WHILE (r GE 1.0) DO BEGIN ;

  IF (i EQ 0) THEN BEGIN

    position_temp2=location_xyz  ;store position of field line in cartesian, for every integration step
    b_temp2=btot
    ds_temp2=ds

  ENDIF ELSE BEGIN

    position_temp2=[[position_temp2],[location_xyz]]
    b_temp2=[b_temp2,btot] ;store btotal for every integration step
    ds_temp2=[ds_temp2,ds]

  ENDELSE
  
  IF (integrator EQ 'ruk45' AND ABS(ds) LE max_ds) THEN BEGIN
    
    ruk45_jupiter, ds, order, location_xyz, max_ds, errtol=errtol; apply runge kutta tracing 4/5 with adaptive step, if step =< max_ds

  ENDIF ELSE IF (integrator EQ 'ruk4' OR ABS(ds) GT max_ds) THEN BEGIN
    
    ruk4_jupiter, ds, order, location_xyz ; apply runge kutta tracing with constant step if integrator set to 'ruk4' or step > max_ds
    
  ENDIF ELSE IF (integrator EQ 'euler' AND ABS(ds) LE max_ds) THEN BEGIN
    
    euler_jupiter, ds, order, location_xyz ; apply euler tracing with constant step if integrator set to 'ruk4' AND step =< max_ds
    
  ENDIF
  
  r=SQRT(TOTAL(location_xyz*location_xyz))
  
  IF (r*0.0 NE 0) THEN BEGIN
    
    r=0.999
    rlim2=-1E38
    
  ENDIF
  
  IF (r GE 1.0) THEN BEGIN
    
    dummy= execute('btot_vec=jovian_'+imodel+'_internal_xyz(location_xyz[0], location_xyz[1], location_xyz[2]) +con2020_model_xyz(''analytic'', location_xyz[0], location_xyz[1], location_xyz[2])')

    btot=SQRT(TOTAL(btot_vec*btot_vec))
    i=i+1l
    
  ENDIF

ENDWHILE

lat_ds2=ASIN(location_xyz[2]/r)*!radeg ;same as above, for latitude in IAU_JUPITER
s3_ds2=ATAN(location_xyz[1], location_xyz[0])*!radeg
test=WHERE(s3_ds2 LT 0)
IF (test[0] NE -1) THEN s3_ds2[test]=2.0*!PI +s3_ds2[test] ;convert to 0-->2pi
IF (s3_ds2 GE 2.0*!PI) THEN s3_ds2=0.0 ;sometimes due to decimal accuracy, s3_ds2=0.0 is estimated slightly above 2*pi, avoid this.

IF (rlim1 EQ -1E38 OR rlim2 EQ -1E38) THEN BEGIN ;test if tracing is valid
  
  out_temp=[!Values.F_NaN, !Values.F_NaN, !Values.F_NaN, !Values.F_NaN, !Values.F_NaN, !Values.F_NaN, !Values.F_NaN] ;output parameters are NaN if invalid
  pos=!Values.F_NaN
  bval=!Values.F_NaN
  dsval=!Values.F_NaN
  beq=!Values.F_NaN
  bloc=!Values.F_NaN
  
ENDIF ELSE BEGIN ;if valid
  
  nl=N_ELEMENTS(b_temp2)
  pos=[[REVERSE(position_temp,2)],[position_temp2[*,1l:nl-1l]]] ;full field line coordinates, combine ds and -ds tracings
  bval=[REVERSE(b_temp), b_temp2[1l:nl-1l]] ;full local B along a field, combine ds and -ds tracings
  btot=SQRT(TOTAL(bin*bin)) ;bin is the initial magnetic field value
  dsval=ABS([REVERSE(ds_temp), ds_temp2[1l:nl-1l]])
  
  ltemp=SQRT(TOTAL(pos*pos,1))
  
  l=MAX(ltemp,/NaN) ;find l-shell (mean value if multiple minima)
  testval=WHERE(ltemp EQ l)
  beq=MEAN(bval[testval],/NaN) ;find b equatorial at b=MINIMUM (mean value if multiple minima)
  bloc=btot ;local magnetic field at initial point
  ;one can use btot and beq to get an equatorial pitch angle

  ;find which tracing is north and which south

  IF (lat_ds1 GT lat_ds2) THEN BEGIN
    
    s3_north=s3_ds1
    lat_north=lat_ds1
    s3_south=s3_ds2
    lat_south=lat_ds2
    
  ENDIF ELSE BEGIN
    
    s3_north=s3_ds2
    lat_north=lat_ds2
    s3_south=s3_ds1
    lat_south=lat_ds1
    
  ENDELSE
  
  out_temp=[l,beq, bloc, s3_north, s3_south, lat_north, lat_south]

ENDELSE

IF keyword_set(full_output) THEN out=LIST(out_temp, integrator, order, dsval, bval, pos, location, spherical) ELSE out=out_temp

RETURN, out

END