; +
; NAME: BOUNCE_F
;
; PURPOSE: This function calculates the mirror latitude assuming motion in a
; dipole field, given the pitch angle at a certain magnetic latitude. The
; output value is in degrees
;
; CALLING SEQUENCE: 
;          mirror_latitude=bounce(pitch,[latt],[steps=steps])
;
; INPUTS:
;          [MANDATORY]
;          pitch: vector containing the particle's pitch angle
;
;          [OPTIONAL]
;          latt:  Vector with magnetic Latitude [deg] where the pitch angle is measured. If not given, latt=0.0*FLTARR(N_ELEMENTS(pitch)) 
;          steps: Step for lambda to solve numerical equation. If not given, steps=1000
;          
; MODIFICATION HISTORY:
;       Written, MPI: 28th August 2005.
;       Corrected for non-equatorial pitch angle calculations: 23 September, 2005
;       Vectorized & numerical solution method is more simple and faster (loops removed): March 2008
;	      Minor modifications to increase the speed slightly : February, 2011

FUNCTION bounce_f, pitch, latt, steps=steps

IF (n_elements(pitch) EQ 0) THEN pitch=90.0
IF (n_elements(latt) EQ 0) THEN latt=FLTARR(N_ELEMENTS(pitch))*0.0
IF (n_elements(steps) EQ 0) THEN steps=800.0

lambdastep=!PI/2.0/steps
pitchrad=pitch*!dtor  ;Convert pitch angle from degrees to rad

index=WHERE(pitchrad EQ 0.0 OR pitchrad EQ !PI)     ;Assign a small value to the pitch angle if it is !PI or zero, 
IF (index[0] NE -1) THEN pitchrad[index]=1e-7*!PI ;otherwise the numerical eq. does not give a correct solution
                                                  

temp=SQRT(SIN(pitchrad)^2.0*COS(latt*!dtor)^6.0/SQRT((4.0 -3.0*COS(latt*!dtor)^2.0))) ;If pitch angle not measured at 0.0 mag.lat
pitchrad=ASIN(temp) 	;convert to equatorial pitch angle

                                                                   
undefine, temp	;undefine variables to save memory (useful for large arrays)
		;Be careful not to undefine "pitch" or "latt", because after the routine is finished, t
		;the information of the input parameters (which you may need) would be erased

lrange=FINDGEN(ROUND(!PI/2.0/lambdastep)+1.0)*lambdastep ;Create value range for solution search: 
                                                       ;1000 values between 0 and !Pi/2 are created (default) 

lbounce=FLTARR(N_ELEMENTS(pitchrad))                    ;Form array to put the solutions
                                                        
tempall=((sin(pitchrad)^2.0)##(sqrt(4.0 -3.0*cos(lrange)^2.0)/cos(lrange)^6.0) -1.0)^2.0    ;The minimum of this function (for each column) is the solution

sol=min(tempall, k, dimension=1)                        ;Get the minimum for each column
ind = ARRAY_INDICES(tempall, k)                         ;Extract the correct indices
lbounce=lrange[ind[0,*]]                                ;Write the solutions

lbounce=lbounce*!radeg                                ;Convert solutions to degrees

RETURN, lbounce
END                



