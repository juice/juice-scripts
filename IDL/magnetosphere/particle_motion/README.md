**Script 1 line descriptions**

- **bounce_f.pro**: Estimate mirror latitutde of particles in a dipole field, based on their local or equatorial pitch angle
- **magnetodisk_drift.pro**: Estimate magnetic drift and bounce periods in Jupiter's & Saturn's magnetodisk (https://doi.org/10.1029/2020JA027827)