FUNCTION magnetodisk_drift, $
jupiter=jupiter,$
saturn=saturn,$
erange=erange,$
steps=steps,$
pitch=pitch,$
lshell=lshell,$
q=q,$
z=z

;+
; NAME:
; magnetodisk_drift
;
; PURPOSE:
; Estimate magnetic drift rate and bounce period in Jupiter's and Saturn's magnetodisk based on Guio et al. 2020 (https://doi.org/10.1029/2020JA027827)
; 
;
; CATEGORY:
; Magnetospheric particle motion
;
; CALLING SEQUENCE:
; out= magnetodisk_drift(jupiter=jupiter, saturn=saturn, erange=erange, pitch=pitch, lshell=lshell, [q=q], [z=z])
;
;
; INPUTS:
; jupiter: if set, calculations are done for jupiter diploe + magnetodisk model. Default among jupiter and saturn is jupiter. If both planets are set, jupiter is selected
; saturn: if set, calculations are done for jupiter diploe + magnetodisk model. 
; erange: energy range in MeV [emin, emax], divided in logarithmic steps, set by the steps keyword. If only one value is given for erange, then estimation is done for one energy only
; steps: number of log steps to divide energy range into. If steps=0 and N_elements(erange)>1, then erange is taken as an array with arbitrary user defined energy values
; pitch: equatorial pitch angle in deg for the considered particles. can be an array with "steps" elements, otherwise it should be single valued
; lshell: lshell of considered particles. can be an array with "steps" elements, otherwise it should be single valued
; z: ion atomic number. Relevant only if q>1
; q: charge state of particle. If negative it is set to -1 for electron (default), if >=1 its an ion. Obviously, charge state cannot be >z. q value is forced to be an integer. q cannot aceept vector input
;
;
; OUTPUTS:
; array of bounce period and drift period
;
; COMMON BLOCKS:
; None.
;
; SIDE EFFECTS:
; None.
;
; RESTRICTIONS:
; Only applies to the specific settings of the Achilleos et al. magnetodisk. See Guio et al. 2020 (https://doi.org/10.1029/2020JA027827)
;
; PROCEDURE/EXAMPLE:
; 1) out=magnetodisk_drift(/jupiter, lshell=[5,6,7,8]*1.0 , erange=[1.0,0.1,0.5,20.0] , pitch=90.0, q=-1.0) ;electrons
; 2) out=magnetodisk_drift(/jupiter, lshell=[5,6,7,8]*1.0 , erange=[1.0,0.1,0.5,20.0] , pitch=90.0, q=1.0, z=1.0) ;protons
; 3) out=magnetodisk_drift(/jupiter, lshell=[5,6,7,8]*1.0 , erange=[1.0,0.1,0.5,20.0] , pitch=[60.0,40.0,90.0,10.0], q=2.0, z=2.0) ;He++

; MODIFICATION HISTORY:
; Written, ER, October, 2022.

IF (NOT keyword_set(jupiter) AND NOT keyword_set(saturn)) THEN jupiter=1 ;if no planet selected, default to jupiter
IF (N_ELEMENTS(steps) EQ 0) THEN steps=10.0

IF keyword_set(saturn) THEN jupiter=0 ;;make sure if one planet is selected, the other is deselected. If both planets are selected, jupiter is the default
IF keyword_set(jupiter) THEN saturn=0

IF (N_ELEMENTS(q) EQ 0) THEN q=-1.0 ; default to electrons if no q value is given
IF (N_ELEMENTS(z) EQ 0) THEN z=1.0

IF (N_ELEMENTS(pitch) EQ 0) THEN pitch=90.0
pitch=pitch*!dtor ;convert to radians

IF (N_ELEMENTS(erange) EQ 2 and steps NE 0) THEN BEGIN
  
  steps=FLOOR(steps)
  energy=FINDGEN(steps)*(max(erange)-min(erange))/(steps-1.0) + min(erange)  
  
ENDIF ELSE BEGIN

  energy=erange

ENDELSE

IF (q LT 0) THEN BEGIN 
  
  q=-1.0
  eo=0.511
  
ENDIF ELSE BEGIN
  
  q=floor(q)
  eo=2.0*z*938.2721 ; 2*z gives the ion mass (assuming equal neutron/proton number) 
  
ENDELSE

IF keyword_set(jupiter) THEN BEGIN ;jupiter coefficients
  
  coeff1=0.954
  coeff2=1.15
  coeff3=-0.29
  coeff4=-0.04
  
  coeff5=1272.67
  coeff6=0.55
  coeff7=-0.55
  coeff8=0.1
  coeff9=-2.54e-3
 
  
ENDIF ELSE IF keyword_set(saturn) THEN BEGIN ;saturn coefficients

  coeff1=0.804
  coeff2=1.25
  coeff3=-0.49
  coeff4=-0.04

  coeff5=44.71
  coeff6=0.44
  coeff7=-0.25
  coeff8=0.13
  coeff9=-7.18e-3
  
ENDIF

;bounce period in seconds
tb=coeff1*(energy+eo)/SQRT(energy*(energy+2.0*eo))*lshell*(coeff2+coeff3*sin(pitch)+coeff4*lshell*sin(pitch))

;drift period in hours
td=q*coeff5*(energy+eo)/(energy*(energy+2.0*eo)*lshell*(coeff6+coeff7*sin(pitch)$
                        +coeff8*lshell*sin(pitch)+coeff9*lshell*lshell*sin(pitch)))
                        
out=[[tb],[td]] ; create array of bounce period and drift period
RETURN, out                        

END