# Magnetosphere IDL Scripts

**SUBFOLDERS (1 line description)**
- **field_line_tracing**: Folder containing IDL routines for field line tracing
- **particle_motion**: Folder containing IDL routines for estimating particle motion properties in Jupiter's magnetosphere
