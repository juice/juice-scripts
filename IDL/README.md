# IDL Scripts

**SUBFOLDERS (1 line description)**
- **misc**: Folder containing necessary IDL routines from external sources. No modifications or comments have been added to those routines
- **spice**: IDL scripts developed to handle SPICE & JUICE mission information
- **magnetosphere**: Folder containing IDL scripts for magnetospheric geometry/science calculations
- **examples**: Folder containing examples where combinations of IDL scripts are used
